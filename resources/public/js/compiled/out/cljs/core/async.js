// Compiled by ClojureScript 0.0-3211 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(f){
if(typeof cljs.core.async.t30572 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t30572 = (function (f,fn_handler,meta30573){
this.f = f;
this.fn_handler = fn_handler;
this.meta30573 = meta30573;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t30572.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t30572.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t30572.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t30572.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30574){
var self__ = this;
var _30574__$1 = this;
return self__.meta30573;
});

cljs.core.async.t30572.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30574,meta30573__$1){
var self__ = this;
var _30574__$1 = this;
return (new cljs.core.async.t30572(self__.f,self__.fn_handler,meta30573__$1));
});

cljs.core.async.t30572.cljs$lang$type = true;

cljs.core.async.t30572.cljs$lang$ctorStr = "cljs.core.async/t30572";

cljs.core.async.t30572.cljs$lang$ctorPrWriter = (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t30572");
});

cljs.core.async.__GT_t30572 = (function cljs$core$async$fn_handler_$___GT_t30572(f__$1,fn_handler__$1,meta30573){
return (new cljs.core.async.t30572(f__$1,fn_handler__$1,meta30573));
});

}

return (new cljs.core.async.t30572(f,cljs$core$async$fn_handler,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 * val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 * buffered, but oldest elements in buffer will be dropped (not
 * transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full.
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
var G__30576 = buff;
if(G__30576){
var bit__23449__auto__ = null;
if(cljs.core.truth_((function (){var or__22775__auto__ = bit__23449__auto__;
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return G__30576.cljs$core$async$impl$protocols$UnblockingBuffer$;
}
})())){
return true;
} else {
if((!G__30576.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__30576);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__30576);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 * (filter p) etc or a composition thereof), and an optional exception handler.
 * If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 * transducer is supplied a buffer must be specified. ex-handler must be a
 * fn of one argument - if an exception occurs during transformation it will be called
 * with the thrown value as an argument, and any non-nil return value will be placed
 * in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(){
var G__30578 = arguments.length;
switch (G__30578) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("buffer must be supplied when transducer is"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,new cljs.core.Symbol(null,"buf-or-n","buf-or-n",-1646815050,null)))].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;
/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 * return nil if closed. Will park if nothing is available.
 * Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(){
var G__30581 = arguments.length;
switch (G__30581) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_30583 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_30583);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_30583,ret){
return (function (){
return fn1.call(null,val_30583);
});})(val_30583,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;
cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 * inside a (go ...) block. Will park if no buffer space is available.
 * Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(){
var G__30585 = arguments.length;
switch (G__30585) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__4124__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__4124__auto__)){
var ret = temp__4124__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__4124__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__4124__auto__)){
var retb = temp__4124__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__4124__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__4124__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;
cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__23660__auto___30587 = n;
var x_30588 = (0);
while(true){
if((x_30588 < n__23660__auto___30587)){
(a[x_30588] = (0));

var G__30589 = (x_30588 + (1));
x_30588 = G__30589;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__30590 = (i + (1));
i = G__30590;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t30594 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t30594 = (function (flag,alt_flag,meta30595){
this.flag = flag;
this.alt_flag = alt_flag;
this.meta30595 = meta30595;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t30594.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t30594.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t30594.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t30594.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_30596){
var self__ = this;
var _30596__$1 = this;
return self__.meta30595;
});})(flag))
;

cljs.core.async.t30594.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_30596,meta30595__$1){
var self__ = this;
var _30596__$1 = this;
return (new cljs.core.async.t30594(self__.flag,self__.alt_flag,meta30595__$1));
});})(flag))
;

cljs.core.async.t30594.cljs$lang$type = true;

cljs.core.async.t30594.cljs$lang$ctorStr = "cljs.core.async/t30594";

cljs.core.async.t30594.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t30594");
});})(flag))
;

cljs.core.async.__GT_t30594 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t30594(flag__$1,alt_flag__$1,meta30595){
return (new cljs.core.async.t30594(flag__$1,alt_flag__$1,meta30595));
});})(flag))
;

}

return (new cljs.core.async.t30594(flag,cljs$core$async$alt_flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if(typeof cljs.core.async.t30600 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t30600 = (function (cb,flag,alt_handler,meta30601){
this.cb = cb;
this.flag = flag;
this.alt_handler = alt_handler;
this.meta30601 = meta30601;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t30600.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t30600.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t30600.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t30600.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30602){
var self__ = this;
var _30602__$1 = this;
return self__.meta30601;
});

cljs.core.async.t30600.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30602,meta30601__$1){
var self__ = this;
var _30602__$1 = this;
return (new cljs.core.async.t30600(self__.cb,self__.flag,self__.alt_handler,meta30601__$1));
});

cljs.core.async.t30600.cljs$lang$type = true;

cljs.core.async.t30600.cljs$lang$ctorStr = "cljs.core.async/t30600";

cljs.core.async.t30600.cljs$lang$ctorPrWriter = (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t30600");
});

cljs.core.async.__GT_t30600 = (function cljs$core$async$alt_handler_$___GT_t30600(cb__$1,flag__$1,alt_handler__$1,meta30601){
return (new cljs.core.async.t30600(cb__$1,flag__$1,alt_handler__$1,meta30601));
});

}

return (new cljs.core.async.t30600(cb,flag,cljs$core$async$alt_handler,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30603_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30603_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30604_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30604_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__22775__auto__ = wport;
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return port;
}
})()], null));
} else {
var G__30605 = (i + (1));
i = G__30605;
continue;
}
} else {
return null;
}
break;
}
})();
var or__22775__auto__ = ret;
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__4126__auto__ = (function (){var and__22763__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__22763__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__22763__auto__;
}
})();
if(cljs.core.truth_(temp__4126__auto__)){
var got = temp__4126__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 * [channel-to-put-to val-to-put], in any combination. Takes will be
 * made as if by <!, and puts will be made as if by >!. Unless
 * the :priority option is true, if more than one port operation is
 * ready a non-deterministic choice will be made. If no operation is
 * ready and a :default value is supplied, [default-val :default] will
 * be returned, otherwise alts! will park until the first operation to
 * become ready completes. Returns [val port] of the completed
 * operation, where val is the value taken for takes, and a
 * boolean (true unless already closed, as per put!) for puts.
 * 
 * opts are passed as :key val ... Supported options:
 * 
 * :default val - the value to use if none of the operations are immediately ready
 * :priority true - (default nil) when true, the operations will be tried in order.
 * 
 * Note: there is no guarantee that the port exps or val exprs will be
 * used, nor in what order should they be, so they should not be
 * depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(){
var argseq__23815__auto__ = ((((1) < arguments.length))?(new cljs.core.IndexedSeq(Array.prototype.slice.call(arguments,(1)),(0))):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__23815__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__30608){
var map__30609 = p__30608;
var map__30609__$1 = ((cljs.core.seq_QMARK_.call(null,map__30609))?cljs.core.apply.call(null,cljs.core.hash_map,map__30609):map__30609);
var opts = map__30609__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq30606){
var G__30607 = cljs.core.first.call(null,seq30606);
var seq30606__$1 = cljs.core.next.call(null,seq30606);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__30607,seq30606__$1);
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(){
var G__30611 = arguments.length;
switch (G__30611) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__25999__auto___30660 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___30660){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___30660){
return (function (state_30635){
var state_val_30636 = (state_30635[(1)]);
if((state_val_30636 === (7))){
var inst_30631 = (state_30635[(2)]);
var state_30635__$1 = state_30635;
var statearr_30637_30661 = state_30635__$1;
(statearr_30637_30661[(2)] = inst_30631);

(statearr_30637_30661[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (1))){
var state_30635__$1 = state_30635;
var statearr_30638_30662 = state_30635__$1;
(statearr_30638_30662[(2)] = null);

(statearr_30638_30662[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (4))){
var inst_30614 = (state_30635[(7)]);
var inst_30614__$1 = (state_30635[(2)]);
var inst_30615 = (inst_30614__$1 == null);
var state_30635__$1 = (function (){var statearr_30639 = state_30635;
(statearr_30639[(7)] = inst_30614__$1);

return statearr_30639;
})();
if(cljs.core.truth_(inst_30615)){
var statearr_30640_30663 = state_30635__$1;
(statearr_30640_30663[(1)] = (5));

} else {
var statearr_30641_30664 = state_30635__$1;
(statearr_30641_30664[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (13))){
var state_30635__$1 = state_30635;
var statearr_30642_30665 = state_30635__$1;
(statearr_30642_30665[(2)] = null);

(statearr_30642_30665[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (6))){
var inst_30614 = (state_30635[(7)]);
var state_30635__$1 = state_30635;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30635__$1,(11),to,inst_30614);
} else {
if((state_val_30636 === (3))){
var inst_30633 = (state_30635[(2)]);
var state_30635__$1 = state_30635;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30635__$1,inst_30633);
} else {
if((state_val_30636 === (12))){
var state_30635__$1 = state_30635;
var statearr_30643_30666 = state_30635__$1;
(statearr_30643_30666[(2)] = null);

(statearr_30643_30666[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (2))){
var state_30635__$1 = state_30635;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30635__$1,(4),from);
} else {
if((state_val_30636 === (11))){
var inst_30624 = (state_30635[(2)]);
var state_30635__$1 = state_30635;
if(cljs.core.truth_(inst_30624)){
var statearr_30644_30667 = state_30635__$1;
(statearr_30644_30667[(1)] = (12));

} else {
var statearr_30645_30668 = state_30635__$1;
(statearr_30645_30668[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (9))){
var state_30635__$1 = state_30635;
var statearr_30646_30669 = state_30635__$1;
(statearr_30646_30669[(2)] = null);

(statearr_30646_30669[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (5))){
var state_30635__$1 = state_30635;
if(cljs.core.truth_(close_QMARK_)){
var statearr_30647_30670 = state_30635__$1;
(statearr_30647_30670[(1)] = (8));

} else {
var statearr_30648_30671 = state_30635__$1;
(statearr_30648_30671[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (14))){
var inst_30629 = (state_30635[(2)]);
var state_30635__$1 = state_30635;
var statearr_30649_30672 = state_30635__$1;
(statearr_30649_30672[(2)] = inst_30629);

(statearr_30649_30672[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (10))){
var inst_30621 = (state_30635[(2)]);
var state_30635__$1 = state_30635;
var statearr_30650_30673 = state_30635__$1;
(statearr_30650_30673[(2)] = inst_30621);

(statearr_30650_30673[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30636 === (8))){
var inst_30618 = cljs.core.async.close_BANG_.call(null,to);
var state_30635__$1 = state_30635;
var statearr_30651_30674 = state_30635__$1;
(statearr_30651_30674[(2)] = inst_30618);

(statearr_30651_30674[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___30660))
;
return ((function (switch__25937__auto__,c__25999__auto___30660){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_30655 = [null,null,null,null,null,null,null,null];
(statearr_30655[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_30655[(1)] = (1));

return statearr_30655;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_30635){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_30635);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e30656){if((e30656 instanceof Object)){
var ex__25941__auto__ = e30656;
var statearr_30657_30675 = state_30635;
(statearr_30657_30675[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30635);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30656;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30676 = state_30635;
state_30635 = G__30676;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_30635){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_30635);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___30660))
})();
var state__26001__auto__ = (function (){var statearr_30658 = f__26000__auto__.call(null);
(statearr_30658[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___30660);

return statearr_30658;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___30660))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;
cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"pos?","pos?",-244377722,null),new cljs.core.Symbol(null,"n","n",-2092305744,null))))].join('')));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process = ((function (jobs,results){
return (function (p__30860){
var vec__30861 = p__30860;
var v = cljs.core.nth.call(null,vec__30861,(0),null);
var p = cljs.core.nth.call(null,vec__30861,(1),null);
var job = vec__30861;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__25999__auto___31043 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___31043,res,vec__30861,v,p,job,jobs,results){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___31043,res,vec__30861,v,p,job,jobs,results){
return (function (state_30866){
var state_val_30867 = (state_30866[(1)]);
if((state_val_30867 === (2))){
var inst_30863 = (state_30866[(2)]);
var inst_30864 = cljs.core.async.close_BANG_.call(null,res);
var state_30866__$1 = (function (){var statearr_30868 = state_30866;
(statearr_30868[(7)] = inst_30863);

return statearr_30868;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30866__$1,inst_30864);
} else {
if((state_val_30867 === (1))){
var state_30866__$1 = state_30866;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30866__$1,(2),res,v);
} else {
return null;
}
}
});})(c__25999__auto___31043,res,vec__30861,v,p,job,jobs,results))
;
return ((function (switch__25937__auto__,c__25999__auto___31043,res,vec__30861,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0 = (function (){
var statearr_30872 = [null,null,null,null,null,null,null,null];
(statearr_30872[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__);

(statearr_30872[(1)] = (1));

return statearr_30872;
});
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1 = (function (state_30866){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_30866);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e30873){if((e30873 instanceof Object)){
var ex__25941__auto__ = e30873;
var statearr_30874_31044 = state_30866;
(statearr_30874_31044[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30866);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30873;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31045 = state_30866;
state_30866 = G__31045;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = function(state_30866){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1.call(this,state_30866);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___31043,res,vec__30861,v,p,job,jobs,results))
})();
var state__26001__auto__ = (function (){var statearr_30875 = f__26000__auto__.call(null);
(statearr_30875[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___31043);

return statearr_30875;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___31043,res,vec__30861,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__30876){
var vec__30877 = p__30876;
var v = cljs.core.nth.call(null,vec__30877,(0),null);
var p = cljs.core.nth.call(null,vec__30877,(1),null);
var job = vec__30877;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process))
;
var n__23660__auto___31046 = n;
var __31047 = (0);
while(true){
if((__31047 < n__23660__auto___31046)){
var G__30878_31048 = (((type instanceof cljs.core.Keyword))?type.fqn:null);
switch (G__30878_31048) {
case "async":
var c__25999__auto___31050 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__31047,c__25999__auto___31050,G__30878_31048,n__23660__auto___31046,jobs,results,process,async){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (__31047,c__25999__auto___31050,G__30878_31048,n__23660__auto___31046,jobs,results,process,async){
return (function (state_30891){
var state_val_30892 = (state_30891[(1)]);
if((state_val_30892 === (7))){
var inst_30887 = (state_30891[(2)]);
var state_30891__$1 = state_30891;
var statearr_30893_31051 = state_30891__$1;
(statearr_30893_31051[(2)] = inst_30887);

(statearr_30893_31051[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30892 === (6))){
var state_30891__$1 = state_30891;
var statearr_30894_31052 = state_30891__$1;
(statearr_30894_31052[(2)] = null);

(statearr_30894_31052[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30892 === (5))){
var state_30891__$1 = state_30891;
var statearr_30895_31053 = state_30891__$1;
(statearr_30895_31053[(2)] = null);

(statearr_30895_31053[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30892 === (4))){
var inst_30881 = (state_30891[(2)]);
var inst_30882 = async.call(null,inst_30881);
var state_30891__$1 = state_30891;
if(cljs.core.truth_(inst_30882)){
var statearr_30896_31054 = state_30891__$1;
(statearr_30896_31054[(1)] = (5));

} else {
var statearr_30897_31055 = state_30891__$1;
(statearr_30897_31055[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30892 === (3))){
var inst_30889 = (state_30891[(2)]);
var state_30891__$1 = state_30891;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30891__$1,inst_30889);
} else {
if((state_val_30892 === (2))){
var state_30891__$1 = state_30891;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30891__$1,(4),jobs);
} else {
if((state_val_30892 === (1))){
var state_30891__$1 = state_30891;
var statearr_30898_31056 = state_30891__$1;
(statearr_30898_31056[(2)] = null);

(statearr_30898_31056[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__31047,c__25999__auto___31050,G__30878_31048,n__23660__auto___31046,jobs,results,process,async))
;
return ((function (__31047,switch__25937__auto__,c__25999__auto___31050,G__30878_31048,n__23660__auto___31046,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0 = (function (){
var statearr_30902 = [null,null,null,null,null,null,null];
(statearr_30902[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__);

(statearr_30902[(1)] = (1));

return statearr_30902;
});
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1 = (function (state_30891){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_30891);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e30903){if((e30903 instanceof Object)){
var ex__25941__auto__ = e30903;
var statearr_30904_31057 = state_30891;
(statearr_30904_31057[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30891);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30903;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31058 = state_30891;
state_30891 = G__31058;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = function(state_30891){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1.call(this,state_30891);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__;
})()
;})(__31047,switch__25937__auto__,c__25999__auto___31050,G__30878_31048,n__23660__auto___31046,jobs,results,process,async))
})();
var state__26001__auto__ = (function (){var statearr_30905 = f__26000__auto__.call(null);
(statearr_30905[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___31050);

return statearr_30905;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(__31047,c__25999__auto___31050,G__30878_31048,n__23660__auto___31046,jobs,results,process,async))
);


break;
case "compute":
var c__25999__auto___31059 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__31047,c__25999__auto___31059,G__30878_31048,n__23660__auto___31046,jobs,results,process,async){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (__31047,c__25999__auto___31059,G__30878_31048,n__23660__auto___31046,jobs,results,process,async){
return (function (state_30918){
var state_val_30919 = (state_30918[(1)]);
if((state_val_30919 === (7))){
var inst_30914 = (state_30918[(2)]);
var state_30918__$1 = state_30918;
var statearr_30920_31060 = state_30918__$1;
(statearr_30920_31060[(2)] = inst_30914);

(statearr_30920_31060[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30919 === (6))){
var state_30918__$1 = state_30918;
var statearr_30921_31061 = state_30918__$1;
(statearr_30921_31061[(2)] = null);

(statearr_30921_31061[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30919 === (5))){
var state_30918__$1 = state_30918;
var statearr_30922_31062 = state_30918__$1;
(statearr_30922_31062[(2)] = null);

(statearr_30922_31062[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30919 === (4))){
var inst_30908 = (state_30918[(2)]);
var inst_30909 = process.call(null,inst_30908);
var state_30918__$1 = state_30918;
if(cljs.core.truth_(inst_30909)){
var statearr_30923_31063 = state_30918__$1;
(statearr_30923_31063[(1)] = (5));

} else {
var statearr_30924_31064 = state_30918__$1;
(statearr_30924_31064[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30919 === (3))){
var inst_30916 = (state_30918[(2)]);
var state_30918__$1 = state_30918;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30918__$1,inst_30916);
} else {
if((state_val_30919 === (2))){
var state_30918__$1 = state_30918;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30918__$1,(4),jobs);
} else {
if((state_val_30919 === (1))){
var state_30918__$1 = state_30918;
var statearr_30925_31065 = state_30918__$1;
(statearr_30925_31065[(2)] = null);

(statearr_30925_31065[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__31047,c__25999__auto___31059,G__30878_31048,n__23660__auto___31046,jobs,results,process,async))
;
return ((function (__31047,switch__25937__auto__,c__25999__auto___31059,G__30878_31048,n__23660__auto___31046,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0 = (function (){
var statearr_30929 = [null,null,null,null,null,null,null];
(statearr_30929[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__);

(statearr_30929[(1)] = (1));

return statearr_30929;
});
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1 = (function (state_30918){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_30918);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e30930){if((e30930 instanceof Object)){
var ex__25941__auto__ = e30930;
var statearr_30931_31066 = state_30918;
(statearr_30931_31066[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30918);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30930;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31067 = state_30918;
state_30918 = G__31067;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = function(state_30918){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1.call(this,state_30918);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__;
})()
;})(__31047,switch__25937__auto__,c__25999__auto___31059,G__30878_31048,n__23660__auto___31046,jobs,results,process,async))
})();
var state__26001__auto__ = (function (){var statearr_30932 = f__26000__auto__.call(null);
(statearr_30932[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___31059);

return statearr_30932;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(__31047,c__25999__auto___31059,G__30878_31048,n__23660__auto___31046,jobs,results,process,async))
);


break;
default:
throw (new Error([cljs.core.str("No matching clause: "),cljs.core.str(type)].join('')));

}

var G__31068 = (__31047 + (1));
__31047 = G__31068;
continue;
} else {
}
break;
}

var c__25999__auto___31069 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___31069,jobs,results,process,async){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___31069,jobs,results,process,async){
return (function (state_30954){
var state_val_30955 = (state_30954[(1)]);
if((state_val_30955 === (9))){
var inst_30947 = (state_30954[(2)]);
var state_30954__$1 = (function (){var statearr_30956 = state_30954;
(statearr_30956[(7)] = inst_30947);

return statearr_30956;
})();
var statearr_30957_31070 = state_30954__$1;
(statearr_30957_31070[(2)] = null);

(statearr_30957_31070[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30955 === (8))){
var inst_30940 = (state_30954[(8)]);
var inst_30945 = (state_30954[(2)]);
var state_30954__$1 = (function (){var statearr_30958 = state_30954;
(statearr_30958[(9)] = inst_30945);

return statearr_30958;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30954__$1,(9),results,inst_30940);
} else {
if((state_val_30955 === (7))){
var inst_30950 = (state_30954[(2)]);
var state_30954__$1 = state_30954;
var statearr_30959_31071 = state_30954__$1;
(statearr_30959_31071[(2)] = inst_30950);

(statearr_30959_31071[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30955 === (6))){
var inst_30940 = (state_30954[(8)]);
var inst_30935 = (state_30954[(10)]);
var inst_30940__$1 = cljs.core.async.chan.call(null,(1));
var inst_30941 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_30942 = [inst_30935,inst_30940__$1];
var inst_30943 = (new cljs.core.PersistentVector(null,2,(5),inst_30941,inst_30942,null));
var state_30954__$1 = (function (){var statearr_30960 = state_30954;
(statearr_30960[(8)] = inst_30940__$1);

return statearr_30960;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30954__$1,(8),jobs,inst_30943);
} else {
if((state_val_30955 === (5))){
var inst_30938 = cljs.core.async.close_BANG_.call(null,jobs);
var state_30954__$1 = state_30954;
var statearr_30961_31072 = state_30954__$1;
(statearr_30961_31072[(2)] = inst_30938);

(statearr_30961_31072[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30955 === (4))){
var inst_30935 = (state_30954[(10)]);
var inst_30935__$1 = (state_30954[(2)]);
var inst_30936 = (inst_30935__$1 == null);
var state_30954__$1 = (function (){var statearr_30962 = state_30954;
(statearr_30962[(10)] = inst_30935__$1);

return statearr_30962;
})();
if(cljs.core.truth_(inst_30936)){
var statearr_30963_31073 = state_30954__$1;
(statearr_30963_31073[(1)] = (5));

} else {
var statearr_30964_31074 = state_30954__$1;
(statearr_30964_31074[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30955 === (3))){
var inst_30952 = (state_30954[(2)]);
var state_30954__$1 = state_30954;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30954__$1,inst_30952);
} else {
if((state_val_30955 === (2))){
var state_30954__$1 = state_30954;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30954__$1,(4),from);
} else {
if((state_val_30955 === (1))){
var state_30954__$1 = state_30954;
var statearr_30965_31075 = state_30954__$1;
(statearr_30965_31075[(2)] = null);

(statearr_30965_31075[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___31069,jobs,results,process,async))
;
return ((function (switch__25937__auto__,c__25999__auto___31069,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0 = (function (){
var statearr_30969 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_30969[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__);

(statearr_30969[(1)] = (1));

return statearr_30969;
});
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1 = (function (state_30954){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_30954);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e30970){if((e30970 instanceof Object)){
var ex__25941__auto__ = e30970;
var statearr_30971_31076 = state_30954;
(statearr_30971_31076[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30954);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30970;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31077 = state_30954;
state_30954 = G__31077;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = function(state_30954){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1.call(this,state_30954);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___31069,jobs,results,process,async))
})();
var state__26001__auto__ = (function (){var statearr_30972 = f__26000__auto__.call(null);
(statearr_30972[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___31069);

return statearr_30972;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___31069,jobs,results,process,async))
);


var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__,jobs,results,process,async){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__,jobs,results,process,async){
return (function (state_31010){
var state_val_31011 = (state_31010[(1)]);
if((state_val_31011 === (7))){
var inst_31006 = (state_31010[(2)]);
var state_31010__$1 = state_31010;
var statearr_31012_31078 = state_31010__$1;
(statearr_31012_31078[(2)] = inst_31006);

(statearr_31012_31078[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (20))){
var state_31010__$1 = state_31010;
var statearr_31013_31079 = state_31010__$1;
(statearr_31013_31079[(2)] = null);

(statearr_31013_31079[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (1))){
var state_31010__$1 = state_31010;
var statearr_31014_31080 = state_31010__$1;
(statearr_31014_31080[(2)] = null);

(statearr_31014_31080[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (4))){
var inst_30975 = (state_31010[(7)]);
var inst_30975__$1 = (state_31010[(2)]);
var inst_30976 = (inst_30975__$1 == null);
var state_31010__$1 = (function (){var statearr_31015 = state_31010;
(statearr_31015[(7)] = inst_30975__$1);

return statearr_31015;
})();
if(cljs.core.truth_(inst_30976)){
var statearr_31016_31081 = state_31010__$1;
(statearr_31016_31081[(1)] = (5));

} else {
var statearr_31017_31082 = state_31010__$1;
(statearr_31017_31082[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (15))){
var inst_30988 = (state_31010[(8)]);
var state_31010__$1 = state_31010;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31010__$1,(18),to,inst_30988);
} else {
if((state_val_31011 === (21))){
var inst_31001 = (state_31010[(2)]);
var state_31010__$1 = state_31010;
var statearr_31018_31083 = state_31010__$1;
(statearr_31018_31083[(2)] = inst_31001);

(statearr_31018_31083[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (13))){
var inst_31003 = (state_31010[(2)]);
var state_31010__$1 = (function (){var statearr_31019 = state_31010;
(statearr_31019[(9)] = inst_31003);

return statearr_31019;
})();
var statearr_31020_31084 = state_31010__$1;
(statearr_31020_31084[(2)] = null);

(statearr_31020_31084[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (6))){
var inst_30975 = (state_31010[(7)]);
var state_31010__$1 = state_31010;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31010__$1,(11),inst_30975);
} else {
if((state_val_31011 === (17))){
var inst_30996 = (state_31010[(2)]);
var state_31010__$1 = state_31010;
if(cljs.core.truth_(inst_30996)){
var statearr_31021_31085 = state_31010__$1;
(statearr_31021_31085[(1)] = (19));

} else {
var statearr_31022_31086 = state_31010__$1;
(statearr_31022_31086[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (3))){
var inst_31008 = (state_31010[(2)]);
var state_31010__$1 = state_31010;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31010__$1,inst_31008);
} else {
if((state_val_31011 === (12))){
var inst_30985 = (state_31010[(10)]);
var state_31010__$1 = state_31010;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31010__$1,(14),inst_30985);
} else {
if((state_val_31011 === (2))){
var state_31010__$1 = state_31010;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31010__$1,(4),results);
} else {
if((state_val_31011 === (19))){
var state_31010__$1 = state_31010;
var statearr_31023_31087 = state_31010__$1;
(statearr_31023_31087[(2)] = null);

(statearr_31023_31087[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (11))){
var inst_30985 = (state_31010[(2)]);
var state_31010__$1 = (function (){var statearr_31024 = state_31010;
(statearr_31024[(10)] = inst_30985);

return statearr_31024;
})();
var statearr_31025_31088 = state_31010__$1;
(statearr_31025_31088[(2)] = null);

(statearr_31025_31088[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (9))){
var state_31010__$1 = state_31010;
var statearr_31026_31089 = state_31010__$1;
(statearr_31026_31089[(2)] = null);

(statearr_31026_31089[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (5))){
var state_31010__$1 = state_31010;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31027_31090 = state_31010__$1;
(statearr_31027_31090[(1)] = (8));

} else {
var statearr_31028_31091 = state_31010__$1;
(statearr_31028_31091[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (14))){
var inst_30990 = (state_31010[(11)]);
var inst_30988 = (state_31010[(8)]);
var inst_30988__$1 = (state_31010[(2)]);
var inst_30989 = (inst_30988__$1 == null);
var inst_30990__$1 = cljs.core.not.call(null,inst_30989);
var state_31010__$1 = (function (){var statearr_31029 = state_31010;
(statearr_31029[(11)] = inst_30990__$1);

(statearr_31029[(8)] = inst_30988__$1);

return statearr_31029;
})();
if(inst_30990__$1){
var statearr_31030_31092 = state_31010__$1;
(statearr_31030_31092[(1)] = (15));

} else {
var statearr_31031_31093 = state_31010__$1;
(statearr_31031_31093[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (16))){
var inst_30990 = (state_31010[(11)]);
var state_31010__$1 = state_31010;
var statearr_31032_31094 = state_31010__$1;
(statearr_31032_31094[(2)] = inst_30990);

(statearr_31032_31094[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (10))){
var inst_30982 = (state_31010[(2)]);
var state_31010__$1 = state_31010;
var statearr_31033_31095 = state_31010__$1;
(statearr_31033_31095[(2)] = inst_30982);

(statearr_31033_31095[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (18))){
var inst_30993 = (state_31010[(2)]);
var state_31010__$1 = state_31010;
var statearr_31034_31096 = state_31010__$1;
(statearr_31034_31096[(2)] = inst_30993);

(statearr_31034_31096[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31011 === (8))){
var inst_30979 = cljs.core.async.close_BANG_.call(null,to);
var state_31010__$1 = state_31010;
var statearr_31035_31097 = state_31010__$1;
(statearr_31035_31097[(2)] = inst_30979);

(statearr_31035_31097[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto__,jobs,results,process,async))
;
return ((function (switch__25937__auto__,c__25999__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0 = (function (){
var statearr_31039 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31039[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__);

(statearr_31039[(1)] = (1));

return statearr_31039;
});
var cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1 = (function (state_31010){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_31010);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e31040){if((e31040 instanceof Object)){
var ex__25941__auto__ = e31040;
var statearr_31041_31098 = state_31010;
(statearr_31041_31098[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31010);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31040;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31099 = state_31010;
state_31010 = G__31099;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__ = function(state_31010){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1.call(this,state_31010);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__25938__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__,jobs,results,process,async))
})();
var state__26001__auto__ = (function (){var statearr_31042 = f__26000__auto__.call(null);
(statearr_31042[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_31042;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__,jobs,results,process,async))
);

return c__25999__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel, subject to the async function af, with parallelism n. af
 * must be a function of two arguments, the first an input value and
 * the second a channel on which to place the result(s). af must close!
 * the channel before returning.  The presumption is that af will
 * return immediately, having launched some asynchronous operation
 * whose completion/callback will manipulate the result channel. Outputs
 * will be returned in order relative to  the inputs. By default, the to
 * channel will be closed when the from channel closes, but can be
 * determined by the close?  parameter. Will stop consuming the from
 * channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(){
var G__31101 = arguments.length;
switch (G__31101) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;
/**
 * Takes elements from the from channel and supplies them to the to
 * channel, subject to the transducer xf, with parallelism n. Because
 * it is parallel, the transducer will be applied independently to each
 * element, not across elements, and may produce zero or more outputs
 * per input.  Outputs will be returned in order relative to the
 * inputs. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes.
 * 
 * Note this is supplied for API compatibility with the Clojure version.
 * Values of N > 1 will not result in actual concurrency in a
 * single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(){
var G__31104 = arguments.length;
switch (G__31104) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;
/**
 * Takes a predicate and a source channel and returns a vector of two
 * channels, the first of which will contain the values for which the
 * predicate returned true, the second those for which it returned
 * false.
 * 
 * The out channels will be unbuffered by default, or two buf-or-ns can
 * be supplied. The channels will close after the source channel has
 * closed.
 */
cljs.core.async.split = (function cljs$core$async$split(){
var G__31107 = arguments.length;
switch (G__31107) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__25999__auto___31159 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___31159,tc,fc){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___31159,tc,fc){
return (function (state_31133){
var state_val_31134 = (state_31133[(1)]);
if((state_val_31134 === (7))){
var inst_31129 = (state_31133[(2)]);
var state_31133__$1 = state_31133;
var statearr_31135_31160 = state_31133__$1;
(statearr_31135_31160[(2)] = inst_31129);

(statearr_31135_31160[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (1))){
var state_31133__$1 = state_31133;
var statearr_31136_31161 = state_31133__$1;
(statearr_31136_31161[(2)] = null);

(statearr_31136_31161[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (4))){
var inst_31110 = (state_31133[(7)]);
var inst_31110__$1 = (state_31133[(2)]);
var inst_31111 = (inst_31110__$1 == null);
var state_31133__$1 = (function (){var statearr_31137 = state_31133;
(statearr_31137[(7)] = inst_31110__$1);

return statearr_31137;
})();
if(cljs.core.truth_(inst_31111)){
var statearr_31138_31162 = state_31133__$1;
(statearr_31138_31162[(1)] = (5));

} else {
var statearr_31139_31163 = state_31133__$1;
(statearr_31139_31163[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (13))){
var state_31133__$1 = state_31133;
var statearr_31140_31164 = state_31133__$1;
(statearr_31140_31164[(2)] = null);

(statearr_31140_31164[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (6))){
var inst_31110 = (state_31133[(7)]);
var inst_31116 = p.call(null,inst_31110);
var state_31133__$1 = state_31133;
if(cljs.core.truth_(inst_31116)){
var statearr_31141_31165 = state_31133__$1;
(statearr_31141_31165[(1)] = (9));

} else {
var statearr_31142_31166 = state_31133__$1;
(statearr_31142_31166[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (3))){
var inst_31131 = (state_31133[(2)]);
var state_31133__$1 = state_31133;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31133__$1,inst_31131);
} else {
if((state_val_31134 === (12))){
var state_31133__$1 = state_31133;
var statearr_31143_31167 = state_31133__$1;
(statearr_31143_31167[(2)] = null);

(statearr_31143_31167[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (2))){
var state_31133__$1 = state_31133;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31133__$1,(4),ch);
} else {
if((state_val_31134 === (11))){
var inst_31110 = (state_31133[(7)]);
var inst_31120 = (state_31133[(2)]);
var state_31133__$1 = state_31133;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31133__$1,(8),inst_31120,inst_31110);
} else {
if((state_val_31134 === (9))){
var state_31133__$1 = state_31133;
var statearr_31144_31168 = state_31133__$1;
(statearr_31144_31168[(2)] = tc);

(statearr_31144_31168[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (5))){
var inst_31113 = cljs.core.async.close_BANG_.call(null,tc);
var inst_31114 = cljs.core.async.close_BANG_.call(null,fc);
var state_31133__$1 = (function (){var statearr_31145 = state_31133;
(statearr_31145[(8)] = inst_31113);

return statearr_31145;
})();
var statearr_31146_31169 = state_31133__$1;
(statearr_31146_31169[(2)] = inst_31114);

(statearr_31146_31169[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (14))){
var inst_31127 = (state_31133[(2)]);
var state_31133__$1 = state_31133;
var statearr_31147_31170 = state_31133__$1;
(statearr_31147_31170[(2)] = inst_31127);

(statearr_31147_31170[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (10))){
var state_31133__$1 = state_31133;
var statearr_31148_31171 = state_31133__$1;
(statearr_31148_31171[(2)] = fc);

(statearr_31148_31171[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31134 === (8))){
var inst_31122 = (state_31133[(2)]);
var state_31133__$1 = state_31133;
if(cljs.core.truth_(inst_31122)){
var statearr_31149_31172 = state_31133__$1;
(statearr_31149_31172[(1)] = (12));

} else {
var statearr_31150_31173 = state_31133__$1;
(statearr_31150_31173[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___31159,tc,fc))
;
return ((function (switch__25937__auto__,c__25999__auto___31159,tc,fc){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_31154 = [null,null,null,null,null,null,null,null,null];
(statearr_31154[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_31154[(1)] = (1));

return statearr_31154;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_31133){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_31133);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e31155){if((e31155 instanceof Object)){
var ex__25941__auto__ = e31155;
var statearr_31156_31174 = state_31133;
(statearr_31156_31174[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31133);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31155;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31175 = state_31133;
state_31133 = G__31175;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_31133){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_31133);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___31159,tc,fc))
})();
var state__26001__auto__ = (function (){var statearr_31157 = f__26000__auto__.call(null);
(statearr_31157[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___31159);

return statearr_31157;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___31159,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;
/**
 * f should be a function of 2 arguments. Returns a channel containing
 * the single result of applying f to init and the first item from the
 * channel, then applying f to that result and the 2nd item, etc. If
 * the channel closes without yielding items, returns init and f is not
 * called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__){
return (function (state_31222){
var state_val_31223 = (state_31222[(1)]);
if((state_val_31223 === (7))){
var inst_31218 = (state_31222[(2)]);
var state_31222__$1 = state_31222;
var statearr_31224_31240 = state_31222__$1;
(statearr_31224_31240[(2)] = inst_31218);

(statearr_31224_31240[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31223 === (6))){
var inst_31208 = (state_31222[(7)]);
var inst_31211 = (state_31222[(8)]);
var inst_31215 = f.call(null,inst_31208,inst_31211);
var inst_31208__$1 = inst_31215;
var state_31222__$1 = (function (){var statearr_31225 = state_31222;
(statearr_31225[(7)] = inst_31208__$1);

return statearr_31225;
})();
var statearr_31226_31241 = state_31222__$1;
(statearr_31226_31241[(2)] = null);

(statearr_31226_31241[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31223 === (5))){
var inst_31208 = (state_31222[(7)]);
var state_31222__$1 = state_31222;
var statearr_31227_31242 = state_31222__$1;
(statearr_31227_31242[(2)] = inst_31208);

(statearr_31227_31242[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31223 === (4))){
var inst_31211 = (state_31222[(8)]);
var inst_31211__$1 = (state_31222[(2)]);
var inst_31212 = (inst_31211__$1 == null);
var state_31222__$1 = (function (){var statearr_31228 = state_31222;
(statearr_31228[(8)] = inst_31211__$1);

return statearr_31228;
})();
if(cljs.core.truth_(inst_31212)){
var statearr_31229_31243 = state_31222__$1;
(statearr_31229_31243[(1)] = (5));

} else {
var statearr_31230_31244 = state_31222__$1;
(statearr_31230_31244[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31223 === (3))){
var inst_31220 = (state_31222[(2)]);
var state_31222__$1 = state_31222;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31222__$1,inst_31220);
} else {
if((state_val_31223 === (2))){
var state_31222__$1 = state_31222;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31222__$1,(4),ch);
} else {
if((state_val_31223 === (1))){
var inst_31208 = init;
var state_31222__$1 = (function (){var statearr_31231 = state_31222;
(statearr_31231[(7)] = inst_31208);

return statearr_31231;
})();
var statearr_31232_31245 = state_31222__$1;
(statearr_31232_31245[(2)] = null);

(statearr_31232_31245[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__25999__auto__))
;
return ((function (switch__25937__auto__,c__25999__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__25938__auto__ = null;
var cljs$core$async$reduce_$_state_machine__25938__auto____0 = (function (){
var statearr_31236 = [null,null,null,null,null,null,null,null,null];
(statearr_31236[(0)] = cljs$core$async$reduce_$_state_machine__25938__auto__);

(statearr_31236[(1)] = (1));

return statearr_31236;
});
var cljs$core$async$reduce_$_state_machine__25938__auto____1 = (function (state_31222){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_31222);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e31237){if((e31237 instanceof Object)){
var ex__25941__auto__ = e31237;
var statearr_31238_31246 = state_31222;
(statearr_31238_31246[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31222);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31237;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31247 = state_31222;
state_31222 = G__31247;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__25938__auto__ = function(state_31222){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__25938__auto____1.call(this,state_31222);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__25938__auto____0;
cljs$core$async$reduce_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__25938__auto____1;
return cljs$core$async$reduce_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__))
})();
var state__26001__auto__ = (function (){var statearr_31239 = f__26000__auto__.call(null);
(statearr_31239[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_31239;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__))
);

return c__25999__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 * By default the channel will be closed after the items are copied,
 * but can be determined by the close? parameter.
 * 
 * Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(){
var G__31249 = arguments.length;
switch (G__31249) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__){
return (function (state_31274){
var state_val_31275 = (state_31274[(1)]);
if((state_val_31275 === (7))){
var inst_31256 = (state_31274[(2)]);
var state_31274__$1 = state_31274;
var statearr_31276_31300 = state_31274__$1;
(statearr_31276_31300[(2)] = inst_31256);

(statearr_31276_31300[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (1))){
var inst_31250 = cljs.core.seq.call(null,coll);
var inst_31251 = inst_31250;
var state_31274__$1 = (function (){var statearr_31277 = state_31274;
(statearr_31277[(7)] = inst_31251);

return statearr_31277;
})();
var statearr_31278_31301 = state_31274__$1;
(statearr_31278_31301[(2)] = null);

(statearr_31278_31301[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (4))){
var inst_31251 = (state_31274[(7)]);
var inst_31254 = cljs.core.first.call(null,inst_31251);
var state_31274__$1 = state_31274;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_31274__$1,(7),ch,inst_31254);
} else {
if((state_val_31275 === (13))){
var inst_31268 = (state_31274[(2)]);
var state_31274__$1 = state_31274;
var statearr_31279_31302 = state_31274__$1;
(statearr_31279_31302[(2)] = inst_31268);

(statearr_31279_31302[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (6))){
var inst_31259 = (state_31274[(2)]);
var state_31274__$1 = state_31274;
if(cljs.core.truth_(inst_31259)){
var statearr_31280_31303 = state_31274__$1;
(statearr_31280_31303[(1)] = (8));

} else {
var statearr_31281_31304 = state_31274__$1;
(statearr_31281_31304[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (3))){
var inst_31272 = (state_31274[(2)]);
var state_31274__$1 = state_31274;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31274__$1,inst_31272);
} else {
if((state_val_31275 === (12))){
var state_31274__$1 = state_31274;
var statearr_31282_31305 = state_31274__$1;
(statearr_31282_31305[(2)] = null);

(statearr_31282_31305[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (2))){
var inst_31251 = (state_31274[(7)]);
var state_31274__$1 = state_31274;
if(cljs.core.truth_(inst_31251)){
var statearr_31283_31306 = state_31274__$1;
(statearr_31283_31306[(1)] = (4));

} else {
var statearr_31284_31307 = state_31274__$1;
(statearr_31284_31307[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (11))){
var inst_31265 = cljs.core.async.close_BANG_.call(null,ch);
var state_31274__$1 = state_31274;
var statearr_31285_31308 = state_31274__$1;
(statearr_31285_31308[(2)] = inst_31265);

(statearr_31285_31308[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (9))){
var state_31274__$1 = state_31274;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31286_31309 = state_31274__$1;
(statearr_31286_31309[(1)] = (11));

} else {
var statearr_31287_31310 = state_31274__$1;
(statearr_31287_31310[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (5))){
var inst_31251 = (state_31274[(7)]);
var state_31274__$1 = state_31274;
var statearr_31288_31311 = state_31274__$1;
(statearr_31288_31311[(2)] = inst_31251);

(statearr_31288_31311[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (10))){
var inst_31270 = (state_31274[(2)]);
var state_31274__$1 = state_31274;
var statearr_31289_31312 = state_31274__$1;
(statearr_31289_31312[(2)] = inst_31270);

(statearr_31289_31312[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31275 === (8))){
var inst_31251 = (state_31274[(7)]);
var inst_31261 = cljs.core.next.call(null,inst_31251);
var inst_31251__$1 = inst_31261;
var state_31274__$1 = (function (){var statearr_31290 = state_31274;
(statearr_31290[(7)] = inst_31251__$1);

return statearr_31290;
})();
var statearr_31291_31313 = state_31274__$1;
(statearr_31291_31313[(2)] = null);

(statearr_31291_31313[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto__))
;
return ((function (switch__25937__auto__,c__25999__auto__){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_31295 = [null,null,null,null,null,null,null,null];
(statearr_31295[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_31295[(1)] = (1));

return statearr_31295;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_31274){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_31274);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e31296){if((e31296 instanceof Object)){
var ex__25941__auto__ = e31296;
var statearr_31297_31314 = state_31274;
(statearr_31297_31314[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31274);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31296;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31315 = state_31274;
state_31274 = G__31315;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_31274){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_31274);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__))
})();
var state__26001__auto__ = (function (){var statearr_31298 = f__26000__auto__.call(null);
(statearr_31298[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_31298;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__))
);

return c__25999__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;
/**
 * Creates and returns a channel which contains the contents of coll,
 * closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

cljs.core.async.Mux = (function (){var obj31317 = {};
return obj31317;
})();

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((function (){var and__22763__auto__ = _;
if(and__22763__auto__){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1;
} else {
return and__22763__auto__;
}
})()){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__23411__auto__ = (((_ == null))?null:_);
return (function (){var or__22775__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
})().call(null,_);
}
});


cljs.core.async.Mult = (function (){var obj31319 = {};
return obj31319;
})();

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((function (){var and__22763__auto__ = m;
if(and__22763__auto__){
return m.cljs$core$async$Mult$tap_STAR_$arity$3;
} else {
return and__22763__auto__;
}
})()){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__23411__auto__ = (((m == null))?null:m);
return (function (){var or__22775__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
})().call(null,m,ch,close_QMARK_);
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((function (){var and__22763__auto__ = m;
if(and__22763__auto__){
return m.cljs$core$async$Mult$untap_STAR_$arity$2;
} else {
return and__22763__auto__;
}
})()){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__23411__auto__ = (((m == null))?null:m);
return (function (){var or__22775__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
})().call(null,m,ch);
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((function (){var and__22763__auto__ = m;
if(and__22763__auto__){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1;
} else {
return and__22763__auto__;
}
})()){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__23411__auto__ = (((m == null))?null:m);
return (function (){var or__22775__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
})().call(null,m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 * containing copies of the channel can be created with 'tap', and
 * detached with 'untap'.
 * 
 * Each item is distributed to all taps in parallel and synchronously,
 * i.e. each tap must accept before the next item is distributed. Use
 * buffering/windowing to prevent slow taps from holding up the mult.
 * 
 * Items received when there are no taps get dropped.
 * 
 * If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t31541 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t31541 = (function (cs,ch,mult,meta31542){
this.cs = cs;
this.ch = ch;
this.mult = mult;
this.meta31542 = meta31542;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t31541.prototype.cljs$core$async$Mult$ = true;

cljs.core.async.t31541.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t31541.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t31541.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t31541.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t31541.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t31541.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_31543){
var self__ = this;
var _31543__$1 = this;
return self__.meta31542;
});})(cs))
;

cljs.core.async.t31541.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_31543,meta31542__$1){
var self__ = this;
var _31543__$1 = this;
return (new cljs.core.async.t31541(self__.cs,self__.ch,self__.mult,meta31542__$1));
});})(cs))
;

cljs.core.async.t31541.cljs$lang$type = true;

cljs.core.async.t31541.cljs$lang$ctorStr = "cljs.core.async/t31541";

cljs.core.async.t31541.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t31541");
});})(cs))
;

cljs.core.async.__GT_t31541 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t31541(cs__$1,ch__$1,mult__$1,meta31542){
return (new cljs.core.async.t31541(cs__$1,ch__$1,mult__$1,meta31542));
});})(cs))
;

}

return (new cljs.core.async.t31541(cs,ch,cljs$core$async$mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__25999__auto___31762 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___31762,cs,m,dchan,dctr,done){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___31762,cs,m,dchan,dctr,done){
return (function (state_31674){
var state_val_31675 = (state_31674[(1)]);
if((state_val_31675 === (7))){
var inst_31670 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31676_31763 = state_31674__$1;
(statearr_31676_31763[(2)] = inst_31670);

(statearr_31676_31763[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (20))){
var inst_31575 = (state_31674[(7)]);
var inst_31585 = cljs.core.first.call(null,inst_31575);
var inst_31586 = cljs.core.nth.call(null,inst_31585,(0),null);
var inst_31587 = cljs.core.nth.call(null,inst_31585,(1),null);
var state_31674__$1 = (function (){var statearr_31677 = state_31674;
(statearr_31677[(8)] = inst_31586);

return statearr_31677;
})();
if(cljs.core.truth_(inst_31587)){
var statearr_31678_31764 = state_31674__$1;
(statearr_31678_31764[(1)] = (22));

} else {
var statearr_31679_31765 = state_31674__$1;
(statearr_31679_31765[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (27))){
var inst_31622 = (state_31674[(9)]);
var inst_31546 = (state_31674[(10)]);
var inst_31615 = (state_31674[(11)]);
var inst_31617 = (state_31674[(12)]);
var inst_31622__$1 = cljs.core._nth.call(null,inst_31615,inst_31617);
var inst_31623 = cljs.core.async.put_BANG_.call(null,inst_31622__$1,inst_31546,done);
var state_31674__$1 = (function (){var statearr_31680 = state_31674;
(statearr_31680[(9)] = inst_31622__$1);

return statearr_31680;
})();
if(cljs.core.truth_(inst_31623)){
var statearr_31681_31766 = state_31674__$1;
(statearr_31681_31766[(1)] = (30));

} else {
var statearr_31682_31767 = state_31674__$1;
(statearr_31682_31767[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (1))){
var state_31674__$1 = state_31674;
var statearr_31683_31768 = state_31674__$1;
(statearr_31683_31768[(2)] = null);

(statearr_31683_31768[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (24))){
var inst_31575 = (state_31674[(7)]);
var inst_31592 = (state_31674[(2)]);
var inst_31593 = cljs.core.next.call(null,inst_31575);
var inst_31555 = inst_31593;
var inst_31556 = null;
var inst_31557 = (0);
var inst_31558 = (0);
var state_31674__$1 = (function (){var statearr_31684 = state_31674;
(statearr_31684[(13)] = inst_31557);

(statearr_31684[(14)] = inst_31555);

(statearr_31684[(15)] = inst_31558);

(statearr_31684[(16)] = inst_31592);

(statearr_31684[(17)] = inst_31556);

return statearr_31684;
})();
var statearr_31685_31769 = state_31674__$1;
(statearr_31685_31769[(2)] = null);

(statearr_31685_31769[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (39))){
var state_31674__$1 = state_31674;
var statearr_31689_31770 = state_31674__$1;
(statearr_31689_31770[(2)] = null);

(statearr_31689_31770[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (4))){
var inst_31546 = (state_31674[(10)]);
var inst_31546__$1 = (state_31674[(2)]);
var inst_31547 = (inst_31546__$1 == null);
var state_31674__$1 = (function (){var statearr_31690 = state_31674;
(statearr_31690[(10)] = inst_31546__$1);

return statearr_31690;
})();
if(cljs.core.truth_(inst_31547)){
var statearr_31691_31771 = state_31674__$1;
(statearr_31691_31771[(1)] = (5));

} else {
var statearr_31692_31772 = state_31674__$1;
(statearr_31692_31772[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (15))){
var inst_31557 = (state_31674[(13)]);
var inst_31555 = (state_31674[(14)]);
var inst_31558 = (state_31674[(15)]);
var inst_31556 = (state_31674[(17)]);
var inst_31571 = (state_31674[(2)]);
var inst_31572 = (inst_31558 + (1));
var tmp31686 = inst_31557;
var tmp31687 = inst_31555;
var tmp31688 = inst_31556;
var inst_31555__$1 = tmp31687;
var inst_31556__$1 = tmp31688;
var inst_31557__$1 = tmp31686;
var inst_31558__$1 = inst_31572;
var state_31674__$1 = (function (){var statearr_31693 = state_31674;
(statearr_31693[(13)] = inst_31557__$1);

(statearr_31693[(14)] = inst_31555__$1);

(statearr_31693[(15)] = inst_31558__$1);

(statearr_31693[(18)] = inst_31571);

(statearr_31693[(17)] = inst_31556__$1);

return statearr_31693;
})();
var statearr_31694_31773 = state_31674__$1;
(statearr_31694_31773[(2)] = null);

(statearr_31694_31773[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (21))){
var inst_31596 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31698_31774 = state_31674__$1;
(statearr_31698_31774[(2)] = inst_31596);

(statearr_31698_31774[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (31))){
var inst_31622 = (state_31674[(9)]);
var inst_31626 = done.call(null,null);
var inst_31627 = cljs.core.async.untap_STAR_.call(null,m,inst_31622);
var state_31674__$1 = (function (){var statearr_31699 = state_31674;
(statearr_31699[(19)] = inst_31626);

return statearr_31699;
})();
var statearr_31700_31775 = state_31674__$1;
(statearr_31700_31775[(2)] = inst_31627);

(statearr_31700_31775[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (32))){
var inst_31614 = (state_31674[(20)]);
var inst_31615 = (state_31674[(11)]);
var inst_31616 = (state_31674[(21)]);
var inst_31617 = (state_31674[(12)]);
var inst_31629 = (state_31674[(2)]);
var inst_31630 = (inst_31617 + (1));
var tmp31695 = inst_31614;
var tmp31696 = inst_31615;
var tmp31697 = inst_31616;
var inst_31614__$1 = tmp31695;
var inst_31615__$1 = tmp31696;
var inst_31616__$1 = tmp31697;
var inst_31617__$1 = inst_31630;
var state_31674__$1 = (function (){var statearr_31701 = state_31674;
(statearr_31701[(20)] = inst_31614__$1);

(statearr_31701[(11)] = inst_31615__$1);

(statearr_31701[(21)] = inst_31616__$1);

(statearr_31701[(12)] = inst_31617__$1);

(statearr_31701[(22)] = inst_31629);

return statearr_31701;
})();
var statearr_31702_31776 = state_31674__$1;
(statearr_31702_31776[(2)] = null);

(statearr_31702_31776[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (40))){
var inst_31642 = (state_31674[(23)]);
var inst_31646 = done.call(null,null);
var inst_31647 = cljs.core.async.untap_STAR_.call(null,m,inst_31642);
var state_31674__$1 = (function (){var statearr_31703 = state_31674;
(statearr_31703[(24)] = inst_31646);

return statearr_31703;
})();
var statearr_31704_31777 = state_31674__$1;
(statearr_31704_31777[(2)] = inst_31647);

(statearr_31704_31777[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (33))){
var inst_31633 = (state_31674[(25)]);
var inst_31635 = cljs.core.chunked_seq_QMARK_.call(null,inst_31633);
var state_31674__$1 = state_31674;
if(inst_31635){
var statearr_31705_31778 = state_31674__$1;
(statearr_31705_31778[(1)] = (36));

} else {
var statearr_31706_31779 = state_31674__$1;
(statearr_31706_31779[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (13))){
var inst_31565 = (state_31674[(26)]);
var inst_31568 = cljs.core.async.close_BANG_.call(null,inst_31565);
var state_31674__$1 = state_31674;
var statearr_31707_31780 = state_31674__$1;
(statearr_31707_31780[(2)] = inst_31568);

(statearr_31707_31780[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (22))){
var inst_31586 = (state_31674[(8)]);
var inst_31589 = cljs.core.async.close_BANG_.call(null,inst_31586);
var state_31674__$1 = state_31674;
var statearr_31708_31781 = state_31674__$1;
(statearr_31708_31781[(2)] = inst_31589);

(statearr_31708_31781[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (36))){
var inst_31633 = (state_31674[(25)]);
var inst_31637 = cljs.core.chunk_first.call(null,inst_31633);
var inst_31638 = cljs.core.chunk_rest.call(null,inst_31633);
var inst_31639 = cljs.core.count.call(null,inst_31637);
var inst_31614 = inst_31638;
var inst_31615 = inst_31637;
var inst_31616 = inst_31639;
var inst_31617 = (0);
var state_31674__$1 = (function (){var statearr_31709 = state_31674;
(statearr_31709[(20)] = inst_31614);

(statearr_31709[(11)] = inst_31615);

(statearr_31709[(21)] = inst_31616);

(statearr_31709[(12)] = inst_31617);

return statearr_31709;
})();
var statearr_31710_31782 = state_31674__$1;
(statearr_31710_31782[(2)] = null);

(statearr_31710_31782[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (41))){
var inst_31633 = (state_31674[(25)]);
var inst_31649 = (state_31674[(2)]);
var inst_31650 = cljs.core.next.call(null,inst_31633);
var inst_31614 = inst_31650;
var inst_31615 = null;
var inst_31616 = (0);
var inst_31617 = (0);
var state_31674__$1 = (function (){var statearr_31711 = state_31674;
(statearr_31711[(27)] = inst_31649);

(statearr_31711[(20)] = inst_31614);

(statearr_31711[(11)] = inst_31615);

(statearr_31711[(21)] = inst_31616);

(statearr_31711[(12)] = inst_31617);

return statearr_31711;
})();
var statearr_31712_31783 = state_31674__$1;
(statearr_31712_31783[(2)] = null);

(statearr_31712_31783[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (43))){
var state_31674__$1 = state_31674;
var statearr_31713_31784 = state_31674__$1;
(statearr_31713_31784[(2)] = null);

(statearr_31713_31784[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (29))){
var inst_31658 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31714_31785 = state_31674__$1;
(statearr_31714_31785[(2)] = inst_31658);

(statearr_31714_31785[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (44))){
var inst_31667 = (state_31674[(2)]);
var state_31674__$1 = (function (){var statearr_31715 = state_31674;
(statearr_31715[(28)] = inst_31667);

return statearr_31715;
})();
var statearr_31716_31786 = state_31674__$1;
(statearr_31716_31786[(2)] = null);

(statearr_31716_31786[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (6))){
var inst_31606 = (state_31674[(29)]);
var inst_31605 = cljs.core.deref.call(null,cs);
var inst_31606__$1 = cljs.core.keys.call(null,inst_31605);
var inst_31607 = cljs.core.count.call(null,inst_31606__$1);
var inst_31608 = cljs.core.reset_BANG_.call(null,dctr,inst_31607);
var inst_31613 = cljs.core.seq.call(null,inst_31606__$1);
var inst_31614 = inst_31613;
var inst_31615 = null;
var inst_31616 = (0);
var inst_31617 = (0);
var state_31674__$1 = (function (){var statearr_31717 = state_31674;
(statearr_31717[(29)] = inst_31606__$1);

(statearr_31717[(20)] = inst_31614);

(statearr_31717[(11)] = inst_31615);

(statearr_31717[(21)] = inst_31616);

(statearr_31717[(12)] = inst_31617);

(statearr_31717[(30)] = inst_31608);

return statearr_31717;
})();
var statearr_31718_31787 = state_31674__$1;
(statearr_31718_31787[(2)] = null);

(statearr_31718_31787[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (28))){
var inst_31614 = (state_31674[(20)]);
var inst_31633 = (state_31674[(25)]);
var inst_31633__$1 = cljs.core.seq.call(null,inst_31614);
var state_31674__$1 = (function (){var statearr_31719 = state_31674;
(statearr_31719[(25)] = inst_31633__$1);

return statearr_31719;
})();
if(inst_31633__$1){
var statearr_31720_31788 = state_31674__$1;
(statearr_31720_31788[(1)] = (33));

} else {
var statearr_31721_31789 = state_31674__$1;
(statearr_31721_31789[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (25))){
var inst_31616 = (state_31674[(21)]);
var inst_31617 = (state_31674[(12)]);
var inst_31619 = (inst_31617 < inst_31616);
var inst_31620 = inst_31619;
var state_31674__$1 = state_31674;
if(cljs.core.truth_(inst_31620)){
var statearr_31722_31790 = state_31674__$1;
(statearr_31722_31790[(1)] = (27));

} else {
var statearr_31723_31791 = state_31674__$1;
(statearr_31723_31791[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (34))){
var state_31674__$1 = state_31674;
var statearr_31724_31792 = state_31674__$1;
(statearr_31724_31792[(2)] = null);

(statearr_31724_31792[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (17))){
var state_31674__$1 = state_31674;
var statearr_31725_31793 = state_31674__$1;
(statearr_31725_31793[(2)] = null);

(statearr_31725_31793[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (3))){
var inst_31672 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_31674__$1,inst_31672);
} else {
if((state_val_31675 === (12))){
var inst_31601 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31726_31794 = state_31674__$1;
(statearr_31726_31794[(2)] = inst_31601);

(statearr_31726_31794[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (2))){
var state_31674__$1 = state_31674;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31674__$1,(4),ch);
} else {
if((state_val_31675 === (23))){
var state_31674__$1 = state_31674;
var statearr_31727_31795 = state_31674__$1;
(statearr_31727_31795[(2)] = null);

(statearr_31727_31795[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (35))){
var inst_31656 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31728_31796 = state_31674__$1;
(statearr_31728_31796[(2)] = inst_31656);

(statearr_31728_31796[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (19))){
var inst_31575 = (state_31674[(7)]);
var inst_31579 = cljs.core.chunk_first.call(null,inst_31575);
var inst_31580 = cljs.core.chunk_rest.call(null,inst_31575);
var inst_31581 = cljs.core.count.call(null,inst_31579);
var inst_31555 = inst_31580;
var inst_31556 = inst_31579;
var inst_31557 = inst_31581;
var inst_31558 = (0);
var state_31674__$1 = (function (){var statearr_31729 = state_31674;
(statearr_31729[(13)] = inst_31557);

(statearr_31729[(14)] = inst_31555);

(statearr_31729[(15)] = inst_31558);

(statearr_31729[(17)] = inst_31556);

return statearr_31729;
})();
var statearr_31730_31797 = state_31674__$1;
(statearr_31730_31797[(2)] = null);

(statearr_31730_31797[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (11))){
var inst_31555 = (state_31674[(14)]);
var inst_31575 = (state_31674[(7)]);
var inst_31575__$1 = cljs.core.seq.call(null,inst_31555);
var state_31674__$1 = (function (){var statearr_31731 = state_31674;
(statearr_31731[(7)] = inst_31575__$1);

return statearr_31731;
})();
if(inst_31575__$1){
var statearr_31732_31798 = state_31674__$1;
(statearr_31732_31798[(1)] = (16));

} else {
var statearr_31733_31799 = state_31674__$1;
(statearr_31733_31799[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (9))){
var inst_31603 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31734_31800 = state_31674__$1;
(statearr_31734_31800[(2)] = inst_31603);

(statearr_31734_31800[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (5))){
var inst_31553 = cljs.core.deref.call(null,cs);
var inst_31554 = cljs.core.seq.call(null,inst_31553);
var inst_31555 = inst_31554;
var inst_31556 = null;
var inst_31557 = (0);
var inst_31558 = (0);
var state_31674__$1 = (function (){var statearr_31735 = state_31674;
(statearr_31735[(13)] = inst_31557);

(statearr_31735[(14)] = inst_31555);

(statearr_31735[(15)] = inst_31558);

(statearr_31735[(17)] = inst_31556);

return statearr_31735;
})();
var statearr_31736_31801 = state_31674__$1;
(statearr_31736_31801[(2)] = null);

(statearr_31736_31801[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (14))){
var state_31674__$1 = state_31674;
var statearr_31737_31802 = state_31674__$1;
(statearr_31737_31802[(2)] = null);

(statearr_31737_31802[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (45))){
var inst_31664 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31738_31803 = state_31674__$1;
(statearr_31738_31803[(2)] = inst_31664);

(statearr_31738_31803[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (26))){
var inst_31606 = (state_31674[(29)]);
var inst_31660 = (state_31674[(2)]);
var inst_31661 = cljs.core.seq.call(null,inst_31606);
var state_31674__$1 = (function (){var statearr_31739 = state_31674;
(statearr_31739[(31)] = inst_31660);

return statearr_31739;
})();
if(inst_31661){
var statearr_31740_31804 = state_31674__$1;
(statearr_31740_31804[(1)] = (42));

} else {
var statearr_31741_31805 = state_31674__$1;
(statearr_31741_31805[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (16))){
var inst_31575 = (state_31674[(7)]);
var inst_31577 = cljs.core.chunked_seq_QMARK_.call(null,inst_31575);
var state_31674__$1 = state_31674;
if(inst_31577){
var statearr_31742_31806 = state_31674__$1;
(statearr_31742_31806[(1)] = (19));

} else {
var statearr_31743_31807 = state_31674__$1;
(statearr_31743_31807[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (38))){
var inst_31653 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31744_31808 = state_31674__$1;
(statearr_31744_31808[(2)] = inst_31653);

(statearr_31744_31808[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (30))){
var state_31674__$1 = state_31674;
var statearr_31745_31809 = state_31674__$1;
(statearr_31745_31809[(2)] = null);

(statearr_31745_31809[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (10))){
var inst_31558 = (state_31674[(15)]);
var inst_31556 = (state_31674[(17)]);
var inst_31564 = cljs.core._nth.call(null,inst_31556,inst_31558);
var inst_31565 = cljs.core.nth.call(null,inst_31564,(0),null);
var inst_31566 = cljs.core.nth.call(null,inst_31564,(1),null);
var state_31674__$1 = (function (){var statearr_31746 = state_31674;
(statearr_31746[(26)] = inst_31565);

return statearr_31746;
})();
if(cljs.core.truth_(inst_31566)){
var statearr_31747_31810 = state_31674__$1;
(statearr_31747_31810[(1)] = (13));

} else {
var statearr_31748_31811 = state_31674__$1;
(statearr_31748_31811[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (18))){
var inst_31599 = (state_31674[(2)]);
var state_31674__$1 = state_31674;
var statearr_31749_31812 = state_31674__$1;
(statearr_31749_31812[(2)] = inst_31599);

(statearr_31749_31812[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (42))){
var state_31674__$1 = state_31674;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_31674__$1,(45),dchan);
} else {
if((state_val_31675 === (37))){
var inst_31546 = (state_31674[(10)]);
var inst_31633 = (state_31674[(25)]);
var inst_31642 = (state_31674[(23)]);
var inst_31642__$1 = cljs.core.first.call(null,inst_31633);
var inst_31643 = cljs.core.async.put_BANG_.call(null,inst_31642__$1,inst_31546,done);
var state_31674__$1 = (function (){var statearr_31750 = state_31674;
(statearr_31750[(23)] = inst_31642__$1);

return statearr_31750;
})();
if(cljs.core.truth_(inst_31643)){
var statearr_31751_31813 = state_31674__$1;
(statearr_31751_31813[(1)] = (39));

} else {
var statearr_31752_31814 = state_31674__$1;
(statearr_31752_31814[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31675 === (8))){
var inst_31557 = (state_31674[(13)]);
var inst_31558 = (state_31674[(15)]);
var inst_31560 = (inst_31558 < inst_31557);
var inst_31561 = inst_31560;
var state_31674__$1 = state_31674;
if(cljs.core.truth_(inst_31561)){
var statearr_31753_31815 = state_31674__$1;
(statearr_31753_31815[(1)] = (10));

} else {
var statearr_31754_31816 = state_31674__$1;
(statearr_31754_31816[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___31762,cs,m,dchan,dctr,done))
;
return ((function (switch__25937__auto__,c__25999__auto___31762,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__25938__auto__ = null;
var cljs$core$async$mult_$_state_machine__25938__auto____0 = (function (){
var statearr_31758 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31758[(0)] = cljs$core$async$mult_$_state_machine__25938__auto__);

(statearr_31758[(1)] = (1));

return statearr_31758;
});
var cljs$core$async$mult_$_state_machine__25938__auto____1 = (function (state_31674){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_31674);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e31759){if((e31759 instanceof Object)){
var ex__25941__auto__ = e31759;
var statearr_31760_31817 = state_31674;
(statearr_31760_31817[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_31674);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e31759;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__31818 = state_31674;
state_31674 = G__31818;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__25938__auto__ = function(state_31674){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__25938__auto____1.call(this,state_31674);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__25938__auto____0;
cljs$core$async$mult_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__25938__auto____1;
return cljs$core$async$mult_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___31762,cs,m,dchan,dctr,done))
})();
var state__26001__auto__ = (function (){var statearr_31761 = f__26000__auto__.call(null);
(statearr_31761[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___31762);

return statearr_31761;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___31762,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 * By default the channel will be closed when the source closes,
 * but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(){
var G__31820 = arguments.length;
switch (G__31820) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;
/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

cljs.core.async.Mix = (function (){var obj31823 = {};
return obj31823;
})();

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((function (){var and__22763__auto__ = m;
if(and__22763__auto__){
return m.cljs$core$async$Mix$admix_STAR_$arity$2;
} else {
return and__22763__auto__;
}
})()){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__23411__auto__ = (((m == null))?null:m);
return (function (){var or__22775__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
})().call(null,m,ch);
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((function (){var and__22763__auto__ = m;
if(and__22763__auto__){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2;
} else {
return and__22763__auto__;
}
})()){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__23411__auto__ = (((m == null))?null:m);
return (function (){var or__22775__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
})().call(null,m,ch);
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((function (){var and__22763__auto__ = m;
if(and__22763__auto__){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1;
} else {
return and__22763__auto__;
}
})()){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__23411__auto__ = (((m == null))?null:m);
return (function (){var or__22775__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
})().call(null,m);
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((function (){var and__22763__auto__ = m;
if(and__22763__auto__){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2;
} else {
return and__22763__auto__;
}
})()){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__23411__auto__ = (((m == null))?null:m);
return (function (){var or__22775__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
})().call(null,m,state_map);
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((function (){var and__22763__auto__ = m;
if(and__22763__auto__){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2;
} else {
return and__22763__auto__;
}
})()){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__23411__auto__ = (((m == null))?null:m);
return (function (){var or__22775__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
})().call(null,m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(){
var argseq__23815__auto__ = ((((3) < arguments.length))?(new cljs.core.IndexedSeq(Array.prototype.slice.call(arguments,(3)),(0))):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__23815__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__31828){
var map__31829 = p__31828;
var map__31829__$1 = ((cljs.core.seq_QMARK_.call(null,map__31829))?cljs.core.apply.call(null,cljs.core.hash_map,map__31829):map__31829);
var opts = map__31829__$1;
var statearr_31830_31833 = state;
(statearr_31830_31833[cljs.core.async.impl.ioc_helpers.STATE_IDX] = cont_block);


var temp__4126__auto__ = cljs.core.async.do_alts.call(null,((function (map__31829,map__31829__$1,opts){
return (function (val){
var statearr_31831_31834 = state;
(statearr_31831_31834[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__31829,map__31829__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__4126__auto__)){
var cb = temp__4126__auto__;
var statearr_31832_31835 = state;
(statearr_31832_31835[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq31824){
var G__31825 = cljs.core.first.call(null,seq31824);
var seq31824__$1 = cljs.core.next.call(null,seq31824);
var G__31826 = cljs.core.first.call(null,seq31824__$1);
var seq31824__$2 = cljs.core.next.call(null,seq31824__$1);
var G__31827 = cljs.core.first.call(null,seq31824__$2);
var seq31824__$3 = cljs.core.next.call(null,seq31824__$2);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__31825,G__31826,G__31827,seq31824__$3);
});
/**
 * Creates and returns a mix of one or more input channels which will
 * be put on the supplied out channel. Input sources can be added to
 * the mix with 'admix', and removed with 'unmix'. A mix supports
 * soloing, muting and pausing multiple inputs atomically using
 * 'toggle', and can solo using either muting or pausing as determined
 * by 'solo-mode'.
 * 
 * Each channel can have zero or more boolean modes set via 'toggle':
 * 
 * :solo - when true, only this (ond other soloed) channel(s) will appear
 * in the mix output channel. :mute and :pause states of soloed
 * channels are ignored. If solo-mode is :mute, non-soloed
 * channels are muted, if :pause, non-soloed channels are
 * paused.
 * 
 * :mute - muted channels will have their contents consumed but not included in the mix
 * :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t31955 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t31955 = (function (change,mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta31956){
this.change = change;
this.mix = mix;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta31956 = meta31956;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t31955.prototype.cljs$core$async$Mix$ = true;

cljs.core.async.t31955.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t31955.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t31955.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t31955.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t31955.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str([cljs.core.str("mode must be one of: "),cljs.core.str(self__.solo_modes)].join('')),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"mode","mode",-2000032078,null))))].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t31955.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t31955.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t31955.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_31957){
var self__ = this;
var _31957__$1 = this;
return self__.meta31956;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t31955.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_31957,meta31956__$1){
var self__ = this;
var _31957__$1 = this;
return (new cljs.core.async.t31955(self__.change,self__.mix,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta31956__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t31955.cljs$lang$type = true;

cljs.core.async.t31955.cljs$lang$ctorStr = "cljs.core.async/t31955";

cljs.core.async.t31955.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t31955");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t31955 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t31955(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta31956){
return (new cljs.core.async.t31955(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta31956));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t31955(change,cljs$core$async$mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__25999__auto___32074 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___32074,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___32074,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_32027){
var state_val_32028 = (state_32027[(1)]);
if((state_val_32028 === (7))){
var inst_31971 = (state_32027[(7)]);
var inst_31976 = cljs.core.apply.call(null,cljs.core.hash_map,inst_31971);
var state_32027__$1 = state_32027;
var statearr_32029_32075 = state_32027__$1;
(statearr_32029_32075[(2)] = inst_31976);

(statearr_32029_32075[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (20))){
var inst_31986 = (state_32027[(8)]);
var state_32027__$1 = state_32027;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32027__$1,(23),out,inst_31986);
} else {
if((state_val_32028 === (1))){
var inst_31961 = (state_32027[(9)]);
var inst_31961__$1 = calc_state.call(null);
var inst_31962 = cljs.core.seq_QMARK_.call(null,inst_31961__$1);
var state_32027__$1 = (function (){var statearr_32030 = state_32027;
(statearr_32030[(9)] = inst_31961__$1);

return statearr_32030;
})();
if(inst_31962){
var statearr_32031_32076 = state_32027__$1;
(statearr_32031_32076[(1)] = (2));

} else {
var statearr_32032_32077 = state_32027__$1;
(statearr_32032_32077[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (24))){
var inst_31979 = (state_32027[(10)]);
var inst_31971 = inst_31979;
var state_32027__$1 = (function (){var statearr_32033 = state_32027;
(statearr_32033[(7)] = inst_31971);

return statearr_32033;
})();
var statearr_32034_32078 = state_32027__$1;
(statearr_32034_32078[(2)] = null);

(statearr_32034_32078[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (4))){
var inst_31961 = (state_32027[(9)]);
var inst_31967 = (state_32027[(2)]);
var inst_31968 = cljs.core.get.call(null,inst_31967,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_31969 = cljs.core.get.call(null,inst_31967,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_31970 = cljs.core.get.call(null,inst_31967,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_31971 = inst_31961;
var state_32027__$1 = (function (){var statearr_32035 = state_32027;
(statearr_32035[(11)] = inst_31969);

(statearr_32035[(12)] = inst_31968);

(statearr_32035[(7)] = inst_31971);

(statearr_32035[(13)] = inst_31970);

return statearr_32035;
})();
var statearr_32036_32079 = state_32027__$1;
(statearr_32036_32079[(2)] = null);

(statearr_32036_32079[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (15))){
var state_32027__$1 = state_32027;
var statearr_32037_32080 = state_32027__$1;
(statearr_32037_32080[(2)] = null);

(statearr_32037_32080[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (21))){
var inst_31979 = (state_32027[(10)]);
var inst_31971 = inst_31979;
var state_32027__$1 = (function (){var statearr_32038 = state_32027;
(statearr_32038[(7)] = inst_31971);

return statearr_32038;
})();
var statearr_32039_32081 = state_32027__$1;
(statearr_32039_32081[(2)] = null);

(statearr_32039_32081[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (13))){
var inst_32023 = (state_32027[(2)]);
var state_32027__$1 = state_32027;
var statearr_32040_32082 = state_32027__$1;
(statearr_32040_32082[(2)] = inst_32023);

(statearr_32040_32082[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (22))){
var inst_32021 = (state_32027[(2)]);
var state_32027__$1 = state_32027;
var statearr_32041_32083 = state_32027__$1;
(statearr_32041_32083[(2)] = inst_32021);

(statearr_32041_32083[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (6))){
var inst_32025 = (state_32027[(2)]);
var state_32027__$1 = state_32027;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32027__$1,inst_32025);
} else {
if((state_val_32028 === (25))){
var state_32027__$1 = state_32027;
var statearr_32042_32084 = state_32027__$1;
(statearr_32042_32084[(2)] = null);

(statearr_32042_32084[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (17))){
var inst_32001 = (state_32027[(14)]);
var state_32027__$1 = state_32027;
var statearr_32043_32085 = state_32027__$1;
(statearr_32043_32085[(2)] = inst_32001);

(statearr_32043_32085[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (3))){
var inst_31961 = (state_32027[(9)]);
var state_32027__$1 = state_32027;
var statearr_32044_32086 = state_32027__$1;
(statearr_32044_32086[(2)] = inst_31961);

(statearr_32044_32086[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (12))){
var inst_32001 = (state_32027[(14)]);
var inst_31982 = (state_32027[(15)]);
var inst_31987 = (state_32027[(16)]);
var inst_32001__$1 = inst_31982.call(null,inst_31987);
var state_32027__$1 = (function (){var statearr_32045 = state_32027;
(statearr_32045[(14)] = inst_32001__$1);

return statearr_32045;
})();
if(cljs.core.truth_(inst_32001__$1)){
var statearr_32046_32087 = state_32027__$1;
(statearr_32046_32087[(1)] = (17));

} else {
var statearr_32047_32088 = state_32027__$1;
(statearr_32047_32088[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (2))){
var inst_31961 = (state_32027[(9)]);
var inst_31964 = cljs.core.apply.call(null,cljs.core.hash_map,inst_31961);
var state_32027__$1 = state_32027;
var statearr_32048_32089 = state_32027__$1;
(statearr_32048_32089[(2)] = inst_31964);

(statearr_32048_32089[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (23))){
var inst_32012 = (state_32027[(2)]);
var state_32027__$1 = state_32027;
if(cljs.core.truth_(inst_32012)){
var statearr_32049_32090 = state_32027__$1;
(statearr_32049_32090[(1)] = (24));

} else {
var statearr_32050_32091 = state_32027__$1;
(statearr_32050_32091[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (19))){
var inst_32009 = (state_32027[(2)]);
var state_32027__$1 = state_32027;
if(cljs.core.truth_(inst_32009)){
var statearr_32051_32092 = state_32027__$1;
(statearr_32051_32092[(1)] = (20));

} else {
var statearr_32052_32093 = state_32027__$1;
(statearr_32052_32093[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (11))){
var inst_31986 = (state_32027[(8)]);
var inst_31992 = (inst_31986 == null);
var state_32027__$1 = state_32027;
if(cljs.core.truth_(inst_31992)){
var statearr_32053_32094 = state_32027__$1;
(statearr_32053_32094[(1)] = (14));

} else {
var statearr_32054_32095 = state_32027__$1;
(statearr_32054_32095[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (9))){
var inst_31979 = (state_32027[(10)]);
var inst_31979__$1 = (state_32027[(2)]);
var inst_31980 = cljs.core.get.call(null,inst_31979__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_31981 = cljs.core.get.call(null,inst_31979__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_31982 = cljs.core.get.call(null,inst_31979__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var state_32027__$1 = (function (){var statearr_32055 = state_32027;
(statearr_32055[(10)] = inst_31979__$1);

(statearr_32055[(15)] = inst_31982);

(statearr_32055[(17)] = inst_31981);

return statearr_32055;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_32027__$1,(10),inst_31980);
} else {
if((state_val_32028 === (5))){
var inst_31971 = (state_32027[(7)]);
var inst_31974 = cljs.core.seq_QMARK_.call(null,inst_31971);
var state_32027__$1 = state_32027;
if(inst_31974){
var statearr_32056_32096 = state_32027__$1;
(statearr_32056_32096[(1)] = (7));

} else {
var statearr_32057_32097 = state_32027__$1;
(statearr_32057_32097[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (14))){
var inst_31987 = (state_32027[(16)]);
var inst_31994 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_31987);
var state_32027__$1 = state_32027;
var statearr_32058_32098 = state_32027__$1;
(statearr_32058_32098[(2)] = inst_31994);

(statearr_32058_32098[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (26))){
var inst_32017 = (state_32027[(2)]);
var state_32027__$1 = state_32027;
var statearr_32059_32099 = state_32027__$1;
(statearr_32059_32099[(2)] = inst_32017);

(statearr_32059_32099[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (16))){
var inst_31997 = (state_32027[(2)]);
var inst_31998 = calc_state.call(null);
var inst_31971 = inst_31998;
var state_32027__$1 = (function (){var statearr_32060 = state_32027;
(statearr_32060[(18)] = inst_31997);

(statearr_32060[(7)] = inst_31971);

return statearr_32060;
})();
var statearr_32061_32100 = state_32027__$1;
(statearr_32061_32100[(2)] = null);

(statearr_32061_32100[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (10))){
var inst_31986 = (state_32027[(8)]);
var inst_31987 = (state_32027[(16)]);
var inst_31985 = (state_32027[(2)]);
var inst_31986__$1 = cljs.core.nth.call(null,inst_31985,(0),null);
var inst_31987__$1 = cljs.core.nth.call(null,inst_31985,(1),null);
var inst_31988 = (inst_31986__$1 == null);
var inst_31989 = cljs.core._EQ_.call(null,inst_31987__$1,change);
var inst_31990 = (inst_31988) || (inst_31989);
var state_32027__$1 = (function (){var statearr_32062 = state_32027;
(statearr_32062[(8)] = inst_31986__$1);

(statearr_32062[(16)] = inst_31987__$1);

return statearr_32062;
})();
if(cljs.core.truth_(inst_31990)){
var statearr_32063_32101 = state_32027__$1;
(statearr_32063_32101[(1)] = (11));

} else {
var statearr_32064_32102 = state_32027__$1;
(statearr_32064_32102[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (18))){
var inst_31982 = (state_32027[(15)]);
var inst_31987 = (state_32027[(16)]);
var inst_31981 = (state_32027[(17)]);
var inst_32004 = cljs.core.empty_QMARK_.call(null,inst_31982);
var inst_32005 = inst_31981.call(null,inst_31987);
var inst_32006 = cljs.core.not.call(null,inst_32005);
var inst_32007 = (inst_32004) && (inst_32006);
var state_32027__$1 = state_32027;
var statearr_32065_32103 = state_32027__$1;
(statearr_32065_32103[(2)] = inst_32007);

(statearr_32065_32103[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32028 === (8))){
var inst_31971 = (state_32027[(7)]);
var state_32027__$1 = state_32027;
var statearr_32066_32104 = state_32027__$1;
(statearr_32066_32104[(2)] = inst_31971);

(statearr_32066_32104[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___32074,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__25937__auto__,c__25999__auto___32074,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__25938__auto__ = null;
var cljs$core$async$mix_$_state_machine__25938__auto____0 = (function (){
var statearr_32070 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32070[(0)] = cljs$core$async$mix_$_state_machine__25938__auto__);

(statearr_32070[(1)] = (1));

return statearr_32070;
});
var cljs$core$async$mix_$_state_machine__25938__auto____1 = (function (state_32027){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32027);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32071){if((e32071 instanceof Object)){
var ex__25941__auto__ = e32071;
var statearr_32072_32105 = state_32027;
(statearr_32072_32105[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32027);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32071;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32106 = state_32027;
state_32027 = G__32106;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__25938__auto__ = function(state_32027){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__25938__auto____1.call(this,state_32027);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__25938__auto____0;
cljs$core$async$mix_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__25938__auto____1;
return cljs$core$async$mix_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___32074,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__26001__auto__ = (function (){var statearr_32073 = f__26000__auto__.call(null);
(statearr_32073[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___32074);

return statearr_32073;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___32074,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 * state map is a map of channels -> channel-state-map. A
 * channel-state-map is a map of attrs -> boolean, where attr is one or
 * more of :mute, :pause or :solo. Any states supplied are merged with
 * the current state.
 * 
 * Note that channels can be added to a mix via toggle, which can be
 * used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

cljs.core.async.Pub = (function (){var obj32108 = {};
return obj32108;
})();

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((function (){var and__22763__auto__ = p;
if(and__22763__auto__){
return p.cljs$core$async$Pub$sub_STAR_$arity$4;
} else {
return and__22763__auto__;
}
})()){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__23411__auto__ = (((p == null))?null:p);
return (function (){var or__22775__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
})().call(null,p,v,ch,close_QMARK_);
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((function (){var and__22763__auto__ = p;
if(and__22763__auto__){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3;
} else {
return and__22763__auto__;
}
})()){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__23411__auto__ = (((p == null))?null:p);
return (function (){var or__22775__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
})().call(null,p,v,ch);
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(){
var G__32110 = arguments.length;
switch (G__32110) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((function (){var and__22763__auto__ = p;
if(and__22763__auto__){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1;
} else {
return and__22763__auto__;
}
})()){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__23411__auto__ = (((p == null))?null:p);
return (function (){var or__22775__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p);
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((function (){var and__22763__auto__ = p;
if(and__22763__auto__){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2;
} else {
return and__22763__auto__;
}
})()){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__23411__auto__ = (((p == null))?null:p);
return (function (){var or__22775__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p,v);
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;

/**
 * Creates and returns a pub(lication) of the supplied channel,
 * partitioned into topics by the topic-fn. topic-fn will be applied to
 * each value on the channel and the result will determine the 'topic'
 * on which that value will be put. Channels can be subscribed to
 * receive copies of topics using 'sub', and unsubscribed using
 * 'unsub'. Each topic will be handled by an internal mult on a
 * dedicated channel. By default these internal channels are
 * unbuffered, but a buf-fn can be supplied which, given a topic,
 * creates a buffer with desired properties.
 * 
 * Each item is distributed to all subs in parallel and synchronously,
 * i.e. each sub must accept before the next item is distributed. Use
 * buffering/windowing to prevent slow subs from holding up the pub.
 * 
 * Items received when there are no matching subs get dropped.
 * 
 * Note that if buf-fns are used then each topic is handled
 * asynchronously, i.e. if a channel is subscribed to more than one
 * topic it should not expect them to be interleaved identically with
 * the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(){
var G__32114 = arguments.length;
switch (G__32114) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__22775__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__22775__auto__,mults){
return (function (p1__32112_SHARP_){
if(cljs.core.truth_(p1__32112_SHARP_.call(null,topic))){
return p1__32112_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__32112_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__22775__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t32115 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t32115 = (function (ensure_mult,mults,buf_fn,topic_fn,ch,meta32116){
this.ensure_mult = ensure_mult;
this.mults = mults;
this.buf_fn = buf_fn;
this.topic_fn = topic_fn;
this.ch = ch;
this.meta32116 = meta32116;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t32115.prototype.cljs$core$async$Pub$ = true;

cljs.core.async.t32115.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t32115.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__4126__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__4126__auto__)){
var m = temp__4126__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t32115.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t32115.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t32115.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t32115.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t32115.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_32117){
var self__ = this;
var _32117__$1 = this;
return self__.meta32116;
});})(mults,ensure_mult))
;

cljs.core.async.t32115.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_32117,meta32116__$1){
var self__ = this;
var _32117__$1 = this;
return (new cljs.core.async.t32115(self__.ensure_mult,self__.mults,self__.buf_fn,self__.topic_fn,self__.ch,meta32116__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t32115.cljs$lang$type = true;

cljs.core.async.t32115.cljs$lang$ctorStr = "cljs.core.async/t32115";

cljs.core.async.t32115.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t32115");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t32115 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t32115(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,meta32116){
return (new cljs.core.async.t32115(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,meta32116));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t32115(ensure_mult,mults,buf_fn,topic_fn,ch,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__25999__auto___32238 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___32238,mults,ensure_mult,p){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___32238,mults,ensure_mult,p){
return (function (state_32189){
var state_val_32190 = (state_32189[(1)]);
if((state_val_32190 === (7))){
var inst_32185 = (state_32189[(2)]);
var state_32189__$1 = state_32189;
var statearr_32191_32239 = state_32189__$1;
(statearr_32191_32239[(2)] = inst_32185);

(statearr_32191_32239[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (20))){
var state_32189__$1 = state_32189;
var statearr_32192_32240 = state_32189__$1;
(statearr_32192_32240[(2)] = null);

(statearr_32192_32240[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (1))){
var state_32189__$1 = state_32189;
var statearr_32193_32241 = state_32189__$1;
(statearr_32193_32241[(2)] = null);

(statearr_32193_32241[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (24))){
var inst_32168 = (state_32189[(7)]);
var inst_32177 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_32168);
var state_32189__$1 = state_32189;
var statearr_32194_32242 = state_32189__$1;
(statearr_32194_32242[(2)] = inst_32177);

(statearr_32194_32242[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (4))){
var inst_32120 = (state_32189[(8)]);
var inst_32120__$1 = (state_32189[(2)]);
var inst_32121 = (inst_32120__$1 == null);
var state_32189__$1 = (function (){var statearr_32195 = state_32189;
(statearr_32195[(8)] = inst_32120__$1);

return statearr_32195;
})();
if(cljs.core.truth_(inst_32121)){
var statearr_32196_32243 = state_32189__$1;
(statearr_32196_32243[(1)] = (5));

} else {
var statearr_32197_32244 = state_32189__$1;
(statearr_32197_32244[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (15))){
var inst_32162 = (state_32189[(2)]);
var state_32189__$1 = state_32189;
var statearr_32198_32245 = state_32189__$1;
(statearr_32198_32245[(2)] = inst_32162);

(statearr_32198_32245[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (21))){
var inst_32182 = (state_32189[(2)]);
var state_32189__$1 = (function (){var statearr_32199 = state_32189;
(statearr_32199[(9)] = inst_32182);

return statearr_32199;
})();
var statearr_32200_32246 = state_32189__$1;
(statearr_32200_32246[(2)] = null);

(statearr_32200_32246[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (13))){
var inst_32144 = (state_32189[(10)]);
var inst_32146 = cljs.core.chunked_seq_QMARK_.call(null,inst_32144);
var state_32189__$1 = state_32189;
if(inst_32146){
var statearr_32201_32247 = state_32189__$1;
(statearr_32201_32247[(1)] = (16));

} else {
var statearr_32202_32248 = state_32189__$1;
(statearr_32202_32248[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (22))){
var inst_32174 = (state_32189[(2)]);
var state_32189__$1 = state_32189;
if(cljs.core.truth_(inst_32174)){
var statearr_32203_32249 = state_32189__$1;
(statearr_32203_32249[(1)] = (23));

} else {
var statearr_32204_32250 = state_32189__$1;
(statearr_32204_32250[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (6))){
var inst_32170 = (state_32189[(11)]);
var inst_32168 = (state_32189[(7)]);
var inst_32120 = (state_32189[(8)]);
var inst_32168__$1 = topic_fn.call(null,inst_32120);
var inst_32169 = cljs.core.deref.call(null,mults);
var inst_32170__$1 = cljs.core.get.call(null,inst_32169,inst_32168__$1);
var state_32189__$1 = (function (){var statearr_32205 = state_32189;
(statearr_32205[(11)] = inst_32170__$1);

(statearr_32205[(7)] = inst_32168__$1);

return statearr_32205;
})();
if(cljs.core.truth_(inst_32170__$1)){
var statearr_32206_32251 = state_32189__$1;
(statearr_32206_32251[(1)] = (19));

} else {
var statearr_32207_32252 = state_32189__$1;
(statearr_32207_32252[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (25))){
var inst_32179 = (state_32189[(2)]);
var state_32189__$1 = state_32189;
var statearr_32208_32253 = state_32189__$1;
(statearr_32208_32253[(2)] = inst_32179);

(statearr_32208_32253[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (17))){
var inst_32144 = (state_32189[(10)]);
var inst_32153 = cljs.core.first.call(null,inst_32144);
var inst_32154 = cljs.core.async.muxch_STAR_.call(null,inst_32153);
var inst_32155 = cljs.core.async.close_BANG_.call(null,inst_32154);
var inst_32156 = cljs.core.next.call(null,inst_32144);
var inst_32130 = inst_32156;
var inst_32131 = null;
var inst_32132 = (0);
var inst_32133 = (0);
var state_32189__$1 = (function (){var statearr_32209 = state_32189;
(statearr_32209[(12)] = inst_32131);

(statearr_32209[(13)] = inst_32130);

(statearr_32209[(14)] = inst_32132);

(statearr_32209[(15)] = inst_32155);

(statearr_32209[(16)] = inst_32133);

return statearr_32209;
})();
var statearr_32210_32254 = state_32189__$1;
(statearr_32210_32254[(2)] = null);

(statearr_32210_32254[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (3))){
var inst_32187 = (state_32189[(2)]);
var state_32189__$1 = state_32189;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32189__$1,inst_32187);
} else {
if((state_val_32190 === (12))){
var inst_32164 = (state_32189[(2)]);
var state_32189__$1 = state_32189;
var statearr_32211_32255 = state_32189__$1;
(statearr_32211_32255[(2)] = inst_32164);

(statearr_32211_32255[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (2))){
var state_32189__$1 = state_32189;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32189__$1,(4),ch);
} else {
if((state_val_32190 === (23))){
var state_32189__$1 = state_32189;
var statearr_32212_32256 = state_32189__$1;
(statearr_32212_32256[(2)] = null);

(statearr_32212_32256[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (19))){
var inst_32170 = (state_32189[(11)]);
var inst_32120 = (state_32189[(8)]);
var inst_32172 = cljs.core.async.muxch_STAR_.call(null,inst_32170);
var state_32189__$1 = state_32189;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32189__$1,(22),inst_32172,inst_32120);
} else {
if((state_val_32190 === (11))){
var inst_32144 = (state_32189[(10)]);
var inst_32130 = (state_32189[(13)]);
var inst_32144__$1 = cljs.core.seq.call(null,inst_32130);
var state_32189__$1 = (function (){var statearr_32213 = state_32189;
(statearr_32213[(10)] = inst_32144__$1);

return statearr_32213;
})();
if(inst_32144__$1){
var statearr_32214_32257 = state_32189__$1;
(statearr_32214_32257[(1)] = (13));

} else {
var statearr_32215_32258 = state_32189__$1;
(statearr_32215_32258[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (9))){
var inst_32166 = (state_32189[(2)]);
var state_32189__$1 = state_32189;
var statearr_32216_32259 = state_32189__$1;
(statearr_32216_32259[(2)] = inst_32166);

(statearr_32216_32259[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (5))){
var inst_32127 = cljs.core.deref.call(null,mults);
var inst_32128 = cljs.core.vals.call(null,inst_32127);
var inst_32129 = cljs.core.seq.call(null,inst_32128);
var inst_32130 = inst_32129;
var inst_32131 = null;
var inst_32132 = (0);
var inst_32133 = (0);
var state_32189__$1 = (function (){var statearr_32217 = state_32189;
(statearr_32217[(12)] = inst_32131);

(statearr_32217[(13)] = inst_32130);

(statearr_32217[(14)] = inst_32132);

(statearr_32217[(16)] = inst_32133);

return statearr_32217;
})();
var statearr_32218_32260 = state_32189__$1;
(statearr_32218_32260[(2)] = null);

(statearr_32218_32260[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (14))){
var state_32189__$1 = state_32189;
var statearr_32222_32261 = state_32189__$1;
(statearr_32222_32261[(2)] = null);

(statearr_32222_32261[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (16))){
var inst_32144 = (state_32189[(10)]);
var inst_32148 = cljs.core.chunk_first.call(null,inst_32144);
var inst_32149 = cljs.core.chunk_rest.call(null,inst_32144);
var inst_32150 = cljs.core.count.call(null,inst_32148);
var inst_32130 = inst_32149;
var inst_32131 = inst_32148;
var inst_32132 = inst_32150;
var inst_32133 = (0);
var state_32189__$1 = (function (){var statearr_32223 = state_32189;
(statearr_32223[(12)] = inst_32131);

(statearr_32223[(13)] = inst_32130);

(statearr_32223[(14)] = inst_32132);

(statearr_32223[(16)] = inst_32133);

return statearr_32223;
})();
var statearr_32224_32262 = state_32189__$1;
(statearr_32224_32262[(2)] = null);

(statearr_32224_32262[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (10))){
var inst_32131 = (state_32189[(12)]);
var inst_32130 = (state_32189[(13)]);
var inst_32132 = (state_32189[(14)]);
var inst_32133 = (state_32189[(16)]);
var inst_32138 = cljs.core._nth.call(null,inst_32131,inst_32133);
var inst_32139 = cljs.core.async.muxch_STAR_.call(null,inst_32138);
var inst_32140 = cljs.core.async.close_BANG_.call(null,inst_32139);
var inst_32141 = (inst_32133 + (1));
var tmp32219 = inst_32131;
var tmp32220 = inst_32130;
var tmp32221 = inst_32132;
var inst_32130__$1 = tmp32220;
var inst_32131__$1 = tmp32219;
var inst_32132__$1 = tmp32221;
var inst_32133__$1 = inst_32141;
var state_32189__$1 = (function (){var statearr_32225 = state_32189;
(statearr_32225[(17)] = inst_32140);

(statearr_32225[(12)] = inst_32131__$1);

(statearr_32225[(13)] = inst_32130__$1);

(statearr_32225[(14)] = inst_32132__$1);

(statearr_32225[(16)] = inst_32133__$1);

return statearr_32225;
})();
var statearr_32226_32263 = state_32189__$1;
(statearr_32226_32263[(2)] = null);

(statearr_32226_32263[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (18))){
var inst_32159 = (state_32189[(2)]);
var state_32189__$1 = state_32189;
var statearr_32227_32264 = state_32189__$1;
(statearr_32227_32264[(2)] = inst_32159);

(statearr_32227_32264[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32190 === (8))){
var inst_32132 = (state_32189[(14)]);
var inst_32133 = (state_32189[(16)]);
var inst_32135 = (inst_32133 < inst_32132);
var inst_32136 = inst_32135;
var state_32189__$1 = state_32189;
if(cljs.core.truth_(inst_32136)){
var statearr_32228_32265 = state_32189__$1;
(statearr_32228_32265[(1)] = (10));

} else {
var statearr_32229_32266 = state_32189__$1;
(statearr_32229_32266[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___32238,mults,ensure_mult,p))
;
return ((function (switch__25937__auto__,c__25999__auto___32238,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_32233 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32233[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_32233[(1)] = (1));

return statearr_32233;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_32189){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32189);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32234){if((e32234 instanceof Object)){
var ex__25941__auto__ = e32234;
var statearr_32235_32267 = state_32189;
(statearr_32235_32267[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32189);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32234;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32268 = state_32189;
state_32189 = G__32268;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_32189){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_32189);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___32238,mults,ensure_mult,p))
})();
var state__26001__auto__ = (function (){var statearr_32236 = f__26000__auto__.call(null);
(statearr_32236[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___32238);

return statearr_32236;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___32238,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;
/**
 * Subscribes a channel to a topic of a pub.
 * 
 * By default the channel will be closed when the source closes,
 * but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(){
var G__32270 = arguments.length;
switch (G__32270) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;
/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(){
var G__32273 = arguments.length;
switch (G__32273) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;
/**
 * Takes a function and a collection of source channels, and returns a
 * channel which contains the values produced by applying f to the set
 * of first items taken from each source channel, followed by applying
 * f to the set of second items from each channel, until any one of the
 * channels is closed, at which point the output channel will be
 * closed. The returned channel will be unbuffered by default, or a
 * buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(){
var G__32276 = arguments.length;
switch (G__32276) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__25999__auto___32346 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___32346,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___32346,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_32315){
var state_val_32316 = (state_32315[(1)]);
if((state_val_32316 === (7))){
var state_32315__$1 = state_32315;
var statearr_32317_32347 = state_32315__$1;
(statearr_32317_32347[(2)] = null);

(statearr_32317_32347[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (1))){
var state_32315__$1 = state_32315;
var statearr_32318_32348 = state_32315__$1;
(statearr_32318_32348[(2)] = null);

(statearr_32318_32348[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (4))){
var inst_32279 = (state_32315[(7)]);
var inst_32281 = (inst_32279 < cnt);
var state_32315__$1 = state_32315;
if(cljs.core.truth_(inst_32281)){
var statearr_32319_32349 = state_32315__$1;
(statearr_32319_32349[(1)] = (6));

} else {
var statearr_32320_32350 = state_32315__$1;
(statearr_32320_32350[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (15))){
var inst_32311 = (state_32315[(2)]);
var state_32315__$1 = state_32315;
var statearr_32321_32351 = state_32315__$1;
(statearr_32321_32351[(2)] = inst_32311);

(statearr_32321_32351[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (13))){
var inst_32304 = cljs.core.async.close_BANG_.call(null,out);
var state_32315__$1 = state_32315;
var statearr_32322_32352 = state_32315__$1;
(statearr_32322_32352[(2)] = inst_32304);

(statearr_32322_32352[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (6))){
var state_32315__$1 = state_32315;
var statearr_32323_32353 = state_32315__$1;
(statearr_32323_32353[(2)] = null);

(statearr_32323_32353[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (3))){
var inst_32313 = (state_32315[(2)]);
var state_32315__$1 = state_32315;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32315__$1,inst_32313);
} else {
if((state_val_32316 === (12))){
var inst_32301 = (state_32315[(8)]);
var inst_32301__$1 = (state_32315[(2)]);
var inst_32302 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_32301__$1);
var state_32315__$1 = (function (){var statearr_32324 = state_32315;
(statearr_32324[(8)] = inst_32301__$1);

return statearr_32324;
})();
if(cljs.core.truth_(inst_32302)){
var statearr_32325_32354 = state_32315__$1;
(statearr_32325_32354[(1)] = (13));

} else {
var statearr_32326_32355 = state_32315__$1;
(statearr_32326_32355[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (2))){
var inst_32278 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_32279 = (0);
var state_32315__$1 = (function (){var statearr_32327 = state_32315;
(statearr_32327[(9)] = inst_32278);

(statearr_32327[(7)] = inst_32279);

return statearr_32327;
})();
var statearr_32328_32356 = state_32315__$1;
(statearr_32328_32356[(2)] = null);

(statearr_32328_32356[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (11))){
var inst_32279 = (state_32315[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_32315,(10),Object,null,(9));
var inst_32288 = chs__$1.call(null,inst_32279);
var inst_32289 = done.call(null,inst_32279);
var inst_32290 = cljs.core.async.take_BANG_.call(null,inst_32288,inst_32289);
var state_32315__$1 = state_32315;
var statearr_32329_32357 = state_32315__$1;
(statearr_32329_32357[(2)] = inst_32290);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32315__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (9))){
var inst_32279 = (state_32315[(7)]);
var inst_32292 = (state_32315[(2)]);
var inst_32293 = (inst_32279 + (1));
var inst_32279__$1 = inst_32293;
var state_32315__$1 = (function (){var statearr_32330 = state_32315;
(statearr_32330[(10)] = inst_32292);

(statearr_32330[(7)] = inst_32279__$1);

return statearr_32330;
})();
var statearr_32331_32358 = state_32315__$1;
(statearr_32331_32358[(2)] = null);

(statearr_32331_32358[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (5))){
var inst_32299 = (state_32315[(2)]);
var state_32315__$1 = (function (){var statearr_32332 = state_32315;
(statearr_32332[(11)] = inst_32299);

return statearr_32332;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32315__$1,(12),dchan);
} else {
if((state_val_32316 === (14))){
var inst_32301 = (state_32315[(8)]);
var inst_32306 = cljs.core.apply.call(null,f,inst_32301);
var state_32315__$1 = state_32315;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32315__$1,(16),out,inst_32306);
} else {
if((state_val_32316 === (16))){
var inst_32308 = (state_32315[(2)]);
var state_32315__$1 = (function (){var statearr_32333 = state_32315;
(statearr_32333[(12)] = inst_32308);

return statearr_32333;
})();
var statearr_32334_32359 = state_32315__$1;
(statearr_32334_32359[(2)] = null);

(statearr_32334_32359[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (10))){
var inst_32283 = (state_32315[(2)]);
var inst_32284 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_32315__$1 = (function (){var statearr_32335 = state_32315;
(statearr_32335[(13)] = inst_32283);

return statearr_32335;
})();
var statearr_32336_32360 = state_32315__$1;
(statearr_32336_32360[(2)] = inst_32284);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32315__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32316 === (8))){
var inst_32297 = (state_32315[(2)]);
var state_32315__$1 = state_32315;
var statearr_32337_32361 = state_32315__$1;
(statearr_32337_32361[(2)] = inst_32297);

(statearr_32337_32361[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___32346,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__25937__auto__,c__25999__auto___32346,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_32341 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32341[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_32341[(1)] = (1));

return statearr_32341;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_32315){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32315);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32342){if((e32342 instanceof Object)){
var ex__25941__auto__ = e32342;
var statearr_32343_32362 = state_32315;
(statearr_32343_32362[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32315);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32342;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32363 = state_32315;
state_32315 = G__32363;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_32315){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_32315);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___32346,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__26001__auto__ = (function (){var statearr_32344 = f__26000__auto__.call(null);
(statearr_32344[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___32346);

return statearr_32344;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___32346,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;
/**
 * Takes a collection of source channels and returns a channel which
 * contains all values taken from them. The returned channel will be
 * unbuffered by default, or a buf-or-n can be supplied. The channel
 * will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(){
var G__32366 = arguments.length;
switch (G__32366) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__25999__auto___32421 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___32421,out){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___32421,out){
return (function (state_32396){
var state_val_32397 = (state_32396[(1)]);
if((state_val_32397 === (7))){
var inst_32376 = (state_32396[(7)]);
var inst_32375 = (state_32396[(8)]);
var inst_32375__$1 = (state_32396[(2)]);
var inst_32376__$1 = cljs.core.nth.call(null,inst_32375__$1,(0),null);
var inst_32377 = cljs.core.nth.call(null,inst_32375__$1,(1),null);
var inst_32378 = (inst_32376__$1 == null);
var state_32396__$1 = (function (){var statearr_32398 = state_32396;
(statearr_32398[(9)] = inst_32377);

(statearr_32398[(7)] = inst_32376__$1);

(statearr_32398[(8)] = inst_32375__$1);

return statearr_32398;
})();
if(cljs.core.truth_(inst_32378)){
var statearr_32399_32422 = state_32396__$1;
(statearr_32399_32422[(1)] = (8));

} else {
var statearr_32400_32423 = state_32396__$1;
(statearr_32400_32423[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32397 === (1))){
var inst_32367 = cljs.core.vec.call(null,chs);
var inst_32368 = inst_32367;
var state_32396__$1 = (function (){var statearr_32401 = state_32396;
(statearr_32401[(10)] = inst_32368);

return statearr_32401;
})();
var statearr_32402_32424 = state_32396__$1;
(statearr_32402_32424[(2)] = null);

(statearr_32402_32424[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32397 === (4))){
var inst_32368 = (state_32396[(10)]);
var state_32396__$1 = state_32396;
return cljs.core.async.ioc_alts_BANG_.call(null,state_32396__$1,(7),inst_32368);
} else {
if((state_val_32397 === (6))){
var inst_32392 = (state_32396[(2)]);
var state_32396__$1 = state_32396;
var statearr_32403_32425 = state_32396__$1;
(statearr_32403_32425[(2)] = inst_32392);

(statearr_32403_32425[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32397 === (3))){
var inst_32394 = (state_32396[(2)]);
var state_32396__$1 = state_32396;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32396__$1,inst_32394);
} else {
if((state_val_32397 === (2))){
var inst_32368 = (state_32396[(10)]);
var inst_32370 = cljs.core.count.call(null,inst_32368);
var inst_32371 = (inst_32370 > (0));
var state_32396__$1 = state_32396;
if(cljs.core.truth_(inst_32371)){
var statearr_32405_32426 = state_32396__$1;
(statearr_32405_32426[(1)] = (4));

} else {
var statearr_32406_32427 = state_32396__$1;
(statearr_32406_32427[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32397 === (11))){
var inst_32368 = (state_32396[(10)]);
var inst_32385 = (state_32396[(2)]);
var tmp32404 = inst_32368;
var inst_32368__$1 = tmp32404;
var state_32396__$1 = (function (){var statearr_32407 = state_32396;
(statearr_32407[(10)] = inst_32368__$1);

(statearr_32407[(11)] = inst_32385);

return statearr_32407;
})();
var statearr_32408_32428 = state_32396__$1;
(statearr_32408_32428[(2)] = null);

(statearr_32408_32428[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32397 === (9))){
var inst_32376 = (state_32396[(7)]);
var state_32396__$1 = state_32396;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32396__$1,(11),out,inst_32376);
} else {
if((state_val_32397 === (5))){
var inst_32390 = cljs.core.async.close_BANG_.call(null,out);
var state_32396__$1 = state_32396;
var statearr_32409_32429 = state_32396__$1;
(statearr_32409_32429[(2)] = inst_32390);

(statearr_32409_32429[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32397 === (10))){
var inst_32388 = (state_32396[(2)]);
var state_32396__$1 = state_32396;
var statearr_32410_32430 = state_32396__$1;
(statearr_32410_32430[(2)] = inst_32388);

(statearr_32410_32430[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32397 === (8))){
var inst_32368 = (state_32396[(10)]);
var inst_32377 = (state_32396[(9)]);
var inst_32376 = (state_32396[(7)]);
var inst_32375 = (state_32396[(8)]);
var inst_32380 = (function (){var c = inst_32377;
var v = inst_32376;
var vec__32373 = inst_32375;
var cs = inst_32368;
return ((function (c,v,vec__32373,cs,inst_32368,inst_32377,inst_32376,inst_32375,state_val_32397,c__25999__auto___32421,out){
return (function (p1__32364_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__32364_SHARP_);
});
;})(c,v,vec__32373,cs,inst_32368,inst_32377,inst_32376,inst_32375,state_val_32397,c__25999__auto___32421,out))
})();
var inst_32381 = cljs.core.filterv.call(null,inst_32380,inst_32368);
var inst_32368__$1 = inst_32381;
var state_32396__$1 = (function (){var statearr_32411 = state_32396;
(statearr_32411[(10)] = inst_32368__$1);

return statearr_32411;
})();
var statearr_32412_32431 = state_32396__$1;
(statearr_32412_32431[(2)] = null);

(statearr_32412_32431[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___32421,out))
;
return ((function (switch__25937__auto__,c__25999__auto___32421,out){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_32416 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32416[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_32416[(1)] = (1));

return statearr_32416;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_32396){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32396);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32417){if((e32417 instanceof Object)){
var ex__25941__auto__ = e32417;
var statearr_32418_32432 = state_32396;
(statearr_32418_32432[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32396);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32417;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32433 = state_32396;
state_32396 = G__32433;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_32396){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_32396);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___32421,out))
})();
var state__26001__auto__ = (function (){var statearr_32419 = f__26000__auto__.call(null);
(statearr_32419[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___32421);

return statearr_32419;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___32421,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;
/**
 * Returns a channel containing the single (collection) result of the
 * items taken from the channel conjoined to the supplied
 * collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 * The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(){
var G__32435 = arguments.length;
switch (G__32435) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__25999__auto___32483 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___32483,out){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___32483,out){
return (function (state_32459){
var state_val_32460 = (state_32459[(1)]);
if((state_val_32460 === (7))){
var inst_32441 = (state_32459[(7)]);
var inst_32441__$1 = (state_32459[(2)]);
var inst_32442 = (inst_32441__$1 == null);
var inst_32443 = cljs.core.not.call(null,inst_32442);
var state_32459__$1 = (function (){var statearr_32461 = state_32459;
(statearr_32461[(7)] = inst_32441__$1);

return statearr_32461;
})();
if(inst_32443){
var statearr_32462_32484 = state_32459__$1;
(statearr_32462_32484[(1)] = (8));

} else {
var statearr_32463_32485 = state_32459__$1;
(statearr_32463_32485[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32460 === (1))){
var inst_32436 = (0);
var state_32459__$1 = (function (){var statearr_32464 = state_32459;
(statearr_32464[(8)] = inst_32436);

return statearr_32464;
})();
var statearr_32465_32486 = state_32459__$1;
(statearr_32465_32486[(2)] = null);

(statearr_32465_32486[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32460 === (4))){
var state_32459__$1 = state_32459;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32459__$1,(7),ch);
} else {
if((state_val_32460 === (6))){
var inst_32454 = (state_32459[(2)]);
var state_32459__$1 = state_32459;
var statearr_32466_32487 = state_32459__$1;
(statearr_32466_32487[(2)] = inst_32454);

(statearr_32466_32487[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32460 === (3))){
var inst_32456 = (state_32459[(2)]);
var inst_32457 = cljs.core.async.close_BANG_.call(null,out);
var state_32459__$1 = (function (){var statearr_32467 = state_32459;
(statearr_32467[(9)] = inst_32456);

return statearr_32467;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32459__$1,inst_32457);
} else {
if((state_val_32460 === (2))){
var inst_32436 = (state_32459[(8)]);
var inst_32438 = (inst_32436 < n);
var state_32459__$1 = state_32459;
if(cljs.core.truth_(inst_32438)){
var statearr_32468_32488 = state_32459__$1;
(statearr_32468_32488[(1)] = (4));

} else {
var statearr_32469_32489 = state_32459__$1;
(statearr_32469_32489[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32460 === (11))){
var inst_32436 = (state_32459[(8)]);
var inst_32446 = (state_32459[(2)]);
var inst_32447 = (inst_32436 + (1));
var inst_32436__$1 = inst_32447;
var state_32459__$1 = (function (){var statearr_32470 = state_32459;
(statearr_32470[(8)] = inst_32436__$1);

(statearr_32470[(10)] = inst_32446);

return statearr_32470;
})();
var statearr_32471_32490 = state_32459__$1;
(statearr_32471_32490[(2)] = null);

(statearr_32471_32490[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32460 === (9))){
var state_32459__$1 = state_32459;
var statearr_32472_32491 = state_32459__$1;
(statearr_32472_32491[(2)] = null);

(statearr_32472_32491[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32460 === (5))){
var state_32459__$1 = state_32459;
var statearr_32473_32492 = state_32459__$1;
(statearr_32473_32492[(2)] = null);

(statearr_32473_32492[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32460 === (10))){
var inst_32451 = (state_32459[(2)]);
var state_32459__$1 = state_32459;
var statearr_32474_32493 = state_32459__$1;
(statearr_32474_32493[(2)] = inst_32451);

(statearr_32474_32493[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32460 === (8))){
var inst_32441 = (state_32459[(7)]);
var state_32459__$1 = state_32459;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32459__$1,(11),out,inst_32441);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___32483,out))
;
return ((function (switch__25937__auto__,c__25999__auto___32483,out){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_32478 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_32478[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_32478[(1)] = (1));

return statearr_32478;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_32459){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32459);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32479){if((e32479 instanceof Object)){
var ex__25941__auto__ = e32479;
var statearr_32480_32494 = state_32459;
(statearr_32480_32494[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32459);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32479;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32495 = state_32459;
state_32459 = G__32495;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_32459){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_32459);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___32483,out))
})();
var state__26001__auto__ = (function (){var statearr_32481 = f__26000__auto__.call(null);
(statearr_32481[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___32483);

return statearr_32481;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___32483,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if(typeof cljs.core.async.t32503 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t32503 = (function (ch,f,map_LT_,meta32504){
this.ch = ch;
this.f = f;
this.map_LT_ = map_LT_;
this.meta32504 = meta32504;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t32503.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t32503.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t32503.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t32503.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t32506 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t32506 = (function (fn1,_,meta32504,map_LT_,f,ch,meta32507){
this.fn1 = fn1;
this._ = _;
this.meta32504 = meta32504;
this.map_LT_ = map_LT_;
this.f = f;
this.ch = ch;
this.meta32507 = meta32507;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t32506.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t32506.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t32506.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__32496_SHARP_){
return f1.call(null,(((p1__32496_SHARP_ == null))?null:self__.f.call(null,p1__32496_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t32506.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_32508){
var self__ = this;
var _32508__$1 = this;
return self__.meta32507;
});})(___$1))
;

cljs.core.async.t32506.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_32508,meta32507__$1){
var self__ = this;
var _32508__$1 = this;
return (new cljs.core.async.t32506(self__.fn1,self__._,self__.meta32504,self__.map_LT_,self__.f,self__.ch,meta32507__$1));
});})(___$1))
;

cljs.core.async.t32506.cljs$lang$type = true;

cljs.core.async.t32506.cljs$lang$ctorStr = "cljs.core.async/t32506";

cljs.core.async.t32506.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t32506");
});})(___$1))
;

cljs.core.async.__GT_t32506 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t32506(fn1__$1,___$2,meta32504__$1,map_LT___$1,f__$1,ch__$1,meta32507){
return (new cljs.core.async.t32506(fn1__$1,___$2,meta32504__$1,map_LT___$1,f__$1,ch__$1,meta32507));
});})(___$1))
;

}

return (new cljs.core.async.t32506(fn1,___$1,self__.meta32504,self__.map_LT_,self__.f,self__.ch,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__22763__auto__ = ret;
if(cljs.core.truth_(and__22763__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__22763__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t32503.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t32503.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t32503.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t32503.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32505){
var self__ = this;
var _32505__$1 = this;
return self__.meta32504;
});

cljs.core.async.t32503.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32505,meta32504__$1){
var self__ = this;
var _32505__$1 = this;
return (new cljs.core.async.t32503(self__.ch,self__.f,self__.map_LT_,meta32504__$1));
});

cljs.core.async.t32503.cljs$lang$type = true;

cljs.core.async.t32503.cljs$lang$ctorStr = "cljs.core.async/t32503";

cljs.core.async.t32503.cljs$lang$ctorPrWriter = (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t32503");
});

cljs.core.async.__GT_t32503 = (function cljs$core$async$map_LT__$___GT_t32503(ch__$1,f__$1,map_LT___$1,meta32504){
return (new cljs.core.async.t32503(ch__$1,f__$1,map_LT___$1,meta32504));
});

}

return (new cljs.core.async.t32503(ch,f,cljs$core$async$map_LT_,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if(typeof cljs.core.async.t32512 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t32512 = (function (ch,f,map_GT_,meta32513){
this.ch = ch;
this.f = f;
this.map_GT_ = map_GT_;
this.meta32513 = meta32513;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t32512.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t32512.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t32512.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t32512.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t32512.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t32512.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t32512.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32514){
var self__ = this;
var _32514__$1 = this;
return self__.meta32513;
});

cljs.core.async.t32512.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32514,meta32513__$1){
var self__ = this;
var _32514__$1 = this;
return (new cljs.core.async.t32512(self__.ch,self__.f,self__.map_GT_,meta32513__$1));
});

cljs.core.async.t32512.cljs$lang$type = true;

cljs.core.async.t32512.cljs$lang$ctorStr = "cljs.core.async/t32512";

cljs.core.async.t32512.cljs$lang$ctorPrWriter = (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t32512");
});

cljs.core.async.__GT_t32512 = (function cljs$core$async$map_GT__$___GT_t32512(ch__$1,f__$1,map_GT___$1,meta32513){
return (new cljs.core.async.t32512(ch__$1,f__$1,map_GT___$1,meta32513));
});

}

return (new cljs.core.async.t32512(ch,f,cljs$core$async$map_GT_,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if(typeof cljs.core.async.t32518 !== 'undefined'){
} else {

/**
* @constructor
*/
cljs.core.async.t32518 = (function (ch,p,filter_GT_,meta32519){
this.ch = ch;
this.p = p;
this.filter_GT_ = filter_GT_;
this.meta32519 = meta32519;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t32518.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t32518.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t32518.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t32518.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t32518.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t32518.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t32518.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t32518.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32520){
var self__ = this;
var _32520__$1 = this;
return self__.meta32519;
});

cljs.core.async.t32518.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32520,meta32519__$1){
var self__ = this;
var _32520__$1 = this;
return (new cljs.core.async.t32518(self__.ch,self__.p,self__.filter_GT_,meta32519__$1));
});

cljs.core.async.t32518.cljs$lang$type = true;

cljs.core.async.t32518.cljs$lang$ctorStr = "cljs.core.async/t32518";

cljs.core.async.t32518.cljs$lang$ctorPrWriter = (function (this__23354__auto__,writer__23355__auto__,opt__23356__auto__){
return cljs.core._write.call(null,writer__23355__auto__,"cljs.core.async/t32518");
});

cljs.core.async.__GT_t32518 = (function cljs$core$async$filter_GT__$___GT_t32518(ch__$1,p__$1,filter_GT___$1,meta32519){
return (new cljs.core.async.t32518(ch__$1,p__$1,filter_GT___$1,meta32519));
});

}

return (new cljs.core.async.t32518(ch,p,cljs$core$async$filter_GT_,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(){
var G__32522 = arguments.length;
switch (G__32522) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__25999__auto___32565 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___32565,out){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___32565,out){
return (function (state_32543){
var state_val_32544 = (state_32543[(1)]);
if((state_val_32544 === (7))){
var inst_32539 = (state_32543[(2)]);
var state_32543__$1 = state_32543;
var statearr_32545_32566 = state_32543__$1;
(statearr_32545_32566[(2)] = inst_32539);

(statearr_32545_32566[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32544 === (1))){
var state_32543__$1 = state_32543;
var statearr_32546_32567 = state_32543__$1;
(statearr_32546_32567[(2)] = null);

(statearr_32546_32567[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32544 === (4))){
var inst_32525 = (state_32543[(7)]);
var inst_32525__$1 = (state_32543[(2)]);
var inst_32526 = (inst_32525__$1 == null);
var state_32543__$1 = (function (){var statearr_32547 = state_32543;
(statearr_32547[(7)] = inst_32525__$1);

return statearr_32547;
})();
if(cljs.core.truth_(inst_32526)){
var statearr_32548_32568 = state_32543__$1;
(statearr_32548_32568[(1)] = (5));

} else {
var statearr_32549_32569 = state_32543__$1;
(statearr_32549_32569[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32544 === (6))){
var inst_32525 = (state_32543[(7)]);
var inst_32530 = p.call(null,inst_32525);
var state_32543__$1 = state_32543;
if(cljs.core.truth_(inst_32530)){
var statearr_32550_32570 = state_32543__$1;
(statearr_32550_32570[(1)] = (8));

} else {
var statearr_32551_32571 = state_32543__$1;
(statearr_32551_32571[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32544 === (3))){
var inst_32541 = (state_32543[(2)]);
var state_32543__$1 = state_32543;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32543__$1,inst_32541);
} else {
if((state_val_32544 === (2))){
var state_32543__$1 = state_32543;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32543__$1,(4),ch);
} else {
if((state_val_32544 === (11))){
var inst_32533 = (state_32543[(2)]);
var state_32543__$1 = state_32543;
var statearr_32552_32572 = state_32543__$1;
(statearr_32552_32572[(2)] = inst_32533);

(statearr_32552_32572[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32544 === (9))){
var state_32543__$1 = state_32543;
var statearr_32553_32573 = state_32543__$1;
(statearr_32553_32573[(2)] = null);

(statearr_32553_32573[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32544 === (5))){
var inst_32528 = cljs.core.async.close_BANG_.call(null,out);
var state_32543__$1 = state_32543;
var statearr_32554_32574 = state_32543__$1;
(statearr_32554_32574[(2)] = inst_32528);

(statearr_32554_32574[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32544 === (10))){
var inst_32536 = (state_32543[(2)]);
var state_32543__$1 = (function (){var statearr_32555 = state_32543;
(statearr_32555[(8)] = inst_32536);

return statearr_32555;
})();
var statearr_32556_32575 = state_32543__$1;
(statearr_32556_32575[(2)] = null);

(statearr_32556_32575[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32544 === (8))){
var inst_32525 = (state_32543[(7)]);
var state_32543__$1 = state_32543;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32543__$1,(11),out,inst_32525);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___32565,out))
;
return ((function (switch__25937__auto__,c__25999__auto___32565,out){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_32560 = [null,null,null,null,null,null,null,null,null];
(statearr_32560[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_32560[(1)] = (1));

return statearr_32560;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_32543){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32543);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32561){if((e32561 instanceof Object)){
var ex__25941__auto__ = e32561;
var statearr_32562_32576 = state_32543;
(statearr_32562_32576[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32543);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32561;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32577 = state_32543;
state_32543 = G__32577;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_32543){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_32543);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___32565,out))
})();
var state__26001__auto__ = (function (){var statearr_32563 = f__26000__auto__.call(null);
(statearr_32563[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___32565);

return statearr_32563;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___32565,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(){
var G__32579 = arguments.length;
switch (G__32579) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;
cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__){
return (function (state_32746){
var state_val_32747 = (state_32746[(1)]);
if((state_val_32747 === (7))){
var inst_32742 = (state_32746[(2)]);
var state_32746__$1 = state_32746;
var statearr_32748_32789 = state_32746__$1;
(statearr_32748_32789[(2)] = inst_32742);

(statearr_32748_32789[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (20))){
var inst_32712 = (state_32746[(7)]);
var inst_32723 = (state_32746[(2)]);
var inst_32724 = cljs.core.next.call(null,inst_32712);
var inst_32698 = inst_32724;
var inst_32699 = null;
var inst_32700 = (0);
var inst_32701 = (0);
var state_32746__$1 = (function (){var statearr_32749 = state_32746;
(statearr_32749[(8)] = inst_32701);

(statearr_32749[(9)] = inst_32700);

(statearr_32749[(10)] = inst_32699);

(statearr_32749[(11)] = inst_32723);

(statearr_32749[(12)] = inst_32698);

return statearr_32749;
})();
var statearr_32750_32790 = state_32746__$1;
(statearr_32750_32790[(2)] = null);

(statearr_32750_32790[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (1))){
var state_32746__$1 = state_32746;
var statearr_32751_32791 = state_32746__$1;
(statearr_32751_32791[(2)] = null);

(statearr_32751_32791[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (4))){
var inst_32687 = (state_32746[(13)]);
var inst_32687__$1 = (state_32746[(2)]);
var inst_32688 = (inst_32687__$1 == null);
var state_32746__$1 = (function (){var statearr_32752 = state_32746;
(statearr_32752[(13)] = inst_32687__$1);

return statearr_32752;
})();
if(cljs.core.truth_(inst_32688)){
var statearr_32753_32792 = state_32746__$1;
(statearr_32753_32792[(1)] = (5));

} else {
var statearr_32754_32793 = state_32746__$1;
(statearr_32754_32793[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (15))){
var state_32746__$1 = state_32746;
var statearr_32758_32794 = state_32746__$1;
(statearr_32758_32794[(2)] = null);

(statearr_32758_32794[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (21))){
var state_32746__$1 = state_32746;
var statearr_32759_32795 = state_32746__$1;
(statearr_32759_32795[(2)] = null);

(statearr_32759_32795[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (13))){
var inst_32701 = (state_32746[(8)]);
var inst_32700 = (state_32746[(9)]);
var inst_32699 = (state_32746[(10)]);
var inst_32698 = (state_32746[(12)]);
var inst_32708 = (state_32746[(2)]);
var inst_32709 = (inst_32701 + (1));
var tmp32755 = inst_32700;
var tmp32756 = inst_32699;
var tmp32757 = inst_32698;
var inst_32698__$1 = tmp32757;
var inst_32699__$1 = tmp32756;
var inst_32700__$1 = tmp32755;
var inst_32701__$1 = inst_32709;
var state_32746__$1 = (function (){var statearr_32760 = state_32746;
(statearr_32760[(14)] = inst_32708);

(statearr_32760[(8)] = inst_32701__$1);

(statearr_32760[(9)] = inst_32700__$1);

(statearr_32760[(10)] = inst_32699__$1);

(statearr_32760[(12)] = inst_32698__$1);

return statearr_32760;
})();
var statearr_32761_32796 = state_32746__$1;
(statearr_32761_32796[(2)] = null);

(statearr_32761_32796[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (22))){
var state_32746__$1 = state_32746;
var statearr_32762_32797 = state_32746__$1;
(statearr_32762_32797[(2)] = null);

(statearr_32762_32797[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (6))){
var inst_32687 = (state_32746[(13)]);
var inst_32696 = f.call(null,inst_32687);
var inst_32697 = cljs.core.seq.call(null,inst_32696);
var inst_32698 = inst_32697;
var inst_32699 = null;
var inst_32700 = (0);
var inst_32701 = (0);
var state_32746__$1 = (function (){var statearr_32763 = state_32746;
(statearr_32763[(8)] = inst_32701);

(statearr_32763[(9)] = inst_32700);

(statearr_32763[(10)] = inst_32699);

(statearr_32763[(12)] = inst_32698);

return statearr_32763;
})();
var statearr_32764_32798 = state_32746__$1;
(statearr_32764_32798[(2)] = null);

(statearr_32764_32798[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (17))){
var inst_32712 = (state_32746[(7)]);
var inst_32716 = cljs.core.chunk_first.call(null,inst_32712);
var inst_32717 = cljs.core.chunk_rest.call(null,inst_32712);
var inst_32718 = cljs.core.count.call(null,inst_32716);
var inst_32698 = inst_32717;
var inst_32699 = inst_32716;
var inst_32700 = inst_32718;
var inst_32701 = (0);
var state_32746__$1 = (function (){var statearr_32765 = state_32746;
(statearr_32765[(8)] = inst_32701);

(statearr_32765[(9)] = inst_32700);

(statearr_32765[(10)] = inst_32699);

(statearr_32765[(12)] = inst_32698);

return statearr_32765;
})();
var statearr_32766_32799 = state_32746__$1;
(statearr_32766_32799[(2)] = null);

(statearr_32766_32799[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (3))){
var inst_32744 = (state_32746[(2)]);
var state_32746__$1 = state_32746;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32746__$1,inst_32744);
} else {
if((state_val_32747 === (12))){
var inst_32732 = (state_32746[(2)]);
var state_32746__$1 = state_32746;
var statearr_32767_32800 = state_32746__$1;
(statearr_32767_32800[(2)] = inst_32732);

(statearr_32767_32800[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (2))){
var state_32746__$1 = state_32746;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32746__$1,(4),in$);
} else {
if((state_val_32747 === (23))){
var inst_32740 = (state_32746[(2)]);
var state_32746__$1 = state_32746;
var statearr_32768_32801 = state_32746__$1;
(statearr_32768_32801[(2)] = inst_32740);

(statearr_32768_32801[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (19))){
var inst_32727 = (state_32746[(2)]);
var state_32746__$1 = state_32746;
var statearr_32769_32802 = state_32746__$1;
(statearr_32769_32802[(2)] = inst_32727);

(statearr_32769_32802[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (11))){
var inst_32698 = (state_32746[(12)]);
var inst_32712 = (state_32746[(7)]);
var inst_32712__$1 = cljs.core.seq.call(null,inst_32698);
var state_32746__$1 = (function (){var statearr_32770 = state_32746;
(statearr_32770[(7)] = inst_32712__$1);

return statearr_32770;
})();
if(inst_32712__$1){
var statearr_32771_32803 = state_32746__$1;
(statearr_32771_32803[(1)] = (14));

} else {
var statearr_32772_32804 = state_32746__$1;
(statearr_32772_32804[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (9))){
var inst_32734 = (state_32746[(2)]);
var inst_32735 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_32746__$1 = (function (){var statearr_32773 = state_32746;
(statearr_32773[(15)] = inst_32734);

return statearr_32773;
})();
if(cljs.core.truth_(inst_32735)){
var statearr_32774_32805 = state_32746__$1;
(statearr_32774_32805[(1)] = (21));

} else {
var statearr_32775_32806 = state_32746__$1;
(statearr_32775_32806[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (5))){
var inst_32690 = cljs.core.async.close_BANG_.call(null,out);
var state_32746__$1 = state_32746;
var statearr_32776_32807 = state_32746__$1;
(statearr_32776_32807[(2)] = inst_32690);

(statearr_32776_32807[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (14))){
var inst_32712 = (state_32746[(7)]);
var inst_32714 = cljs.core.chunked_seq_QMARK_.call(null,inst_32712);
var state_32746__$1 = state_32746;
if(inst_32714){
var statearr_32777_32808 = state_32746__$1;
(statearr_32777_32808[(1)] = (17));

} else {
var statearr_32778_32809 = state_32746__$1;
(statearr_32778_32809[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (16))){
var inst_32730 = (state_32746[(2)]);
var state_32746__$1 = state_32746;
var statearr_32779_32810 = state_32746__$1;
(statearr_32779_32810[(2)] = inst_32730);

(statearr_32779_32810[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32747 === (10))){
var inst_32701 = (state_32746[(8)]);
var inst_32699 = (state_32746[(10)]);
var inst_32706 = cljs.core._nth.call(null,inst_32699,inst_32701);
var state_32746__$1 = state_32746;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32746__$1,(13),out,inst_32706);
} else {
if((state_val_32747 === (18))){
var inst_32712 = (state_32746[(7)]);
var inst_32721 = cljs.core.first.call(null,inst_32712);
var state_32746__$1 = state_32746;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32746__$1,(20),out,inst_32721);
} else {
if((state_val_32747 === (8))){
var inst_32701 = (state_32746[(8)]);
var inst_32700 = (state_32746[(9)]);
var inst_32703 = (inst_32701 < inst_32700);
var inst_32704 = inst_32703;
var state_32746__$1 = state_32746;
if(cljs.core.truth_(inst_32704)){
var statearr_32780_32811 = state_32746__$1;
(statearr_32780_32811[(1)] = (10));

} else {
var statearr_32781_32812 = state_32746__$1;
(statearr_32781_32812[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto__))
;
return ((function (switch__25937__auto__,c__25999__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__25938__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__25938__auto____0 = (function (){
var statearr_32785 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32785[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__25938__auto__);

(statearr_32785[(1)] = (1));

return statearr_32785;
});
var cljs$core$async$mapcat_STAR__$_state_machine__25938__auto____1 = (function (state_32746){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32746);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32786){if((e32786 instanceof Object)){
var ex__25941__auto__ = e32786;
var statearr_32787_32813 = state_32746;
(statearr_32787_32813[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32746);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32786;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32814 = state_32746;
state_32746 = G__32814;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__25938__auto__ = function(state_32746){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__25938__auto____1.call(this,state_32746);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__25938__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__25938__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__))
})();
var state__26001__auto__ = (function (){var statearr_32788 = f__26000__auto__.call(null);
(statearr_32788[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_32788;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__))
);

return c__25999__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(){
var G__32816 = arguments.length;
switch (G__32816) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(){
var G__32819 = arguments.length;
switch (G__32819) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(){
var G__32822 = arguments.length;
switch (G__32822) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__25999__auto___32872 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___32872,out){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___32872,out){
return (function (state_32846){
var state_val_32847 = (state_32846[(1)]);
if((state_val_32847 === (7))){
var inst_32841 = (state_32846[(2)]);
var state_32846__$1 = state_32846;
var statearr_32848_32873 = state_32846__$1;
(statearr_32848_32873[(2)] = inst_32841);

(statearr_32848_32873[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32847 === (1))){
var inst_32823 = null;
var state_32846__$1 = (function (){var statearr_32849 = state_32846;
(statearr_32849[(7)] = inst_32823);

return statearr_32849;
})();
var statearr_32850_32874 = state_32846__$1;
(statearr_32850_32874[(2)] = null);

(statearr_32850_32874[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32847 === (4))){
var inst_32826 = (state_32846[(8)]);
var inst_32826__$1 = (state_32846[(2)]);
var inst_32827 = (inst_32826__$1 == null);
var inst_32828 = cljs.core.not.call(null,inst_32827);
var state_32846__$1 = (function (){var statearr_32851 = state_32846;
(statearr_32851[(8)] = inst_32826__$1);

return statearr_32851;
})();
if(inst_32828){
var statearr_32852_32875 = state_32846__$1;
(statearr_32852_32875[(1)] = (5));

} else {
var statearr_32853_32876 = state_32846__$1;
(statearr_32853_32876[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32847 === (6))){
var state_32846__$1 = state_32846;
var statearr_32854_32877 = state_32846__$1;
(statearr_32854_32877[(2)] = null);

(statearr_32854_32877[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32847 === (3))){
var inst_32843 = (state_32846[(2)]);
var inst_32844 = cljs.core.async.close_BANG_.call(null,out);
var state_32846__$1 = (function (){var statearr_32855 = state_32846;
(statearr_32855[(9)] = inst_32843);

return statearr_32855;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32846__$1,inst_32844);
} else {
if((state_val_32847 === (2))){
var state_32846__$1 = state_32846;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32846__$1,(4),ch);
} else {
if((state_val_32847 === (11))){
var inst_32826 = (state_32846[(8)]);
var inst_32835 = (state_32846[(2)]);
var inst_32823 = inst_32826;
var state_32846__$1 = (function (){var statearr_32856 = state_32846;
(statearr_32856[(10)] = inst_32835);

(statearr_32856[(7)] = inst_32823);

return statearr_32856;
})();
var statearr_32857_32878 = state_32846__$1;
(statearr_32857_32878[(2)] = null);

(statearr_32857_32878[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32847 === (9))){
var inst_32826 = (state_32846[(8)]);
var state_32846__$1 = state_32846;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32846__$1,(11),out,inst_32826);
} else {
if((state_val_32847 === (5))){
var inst_32826 = (state_32846[(8)]);
var inst_32823 = (state_32846[(7)]);
var inst_32830 = cljs.core._EQ_.call(null,inst_32826,inst_32823);
var state_32846__$1 = state_32846;
if(inst_32830){
var statearr_32859_32879 = state_32846__$1;
(statearr_32859_32879[(1)] = (8));

} else {
var statearr_32860_32880 = state_32846__$1;
(statearr_32860_32880[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32847 === (10))){
var inst_32838 = (state_32846[(2)]);
var state_32846__$1 = state_32846;
var statearr_32861_32881 = state_32846__$1;
(statearr_32861_32881[(2)] = inst_32838);

(statearr_32861_32881[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32847 === (8))){
var inst_32823 = (state_32846[(7)]);
var tmp32858 = inst_32823;
var inst_32823__$1 = tmp32858;
var state_32846__$1 = (function (){var statearr_32862 = state_32846;
(statearr_32862[(7)] = inst_32823__$1);

return statearr_32862;
})();
var statearr_32863_32882 = state_32846__$1;
(statearr_32863_32882[(2)] = null);

(statearr_32863_32882[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___32872,out))
;
return ((function (switch__25937__auto__,c__25999__auto___32872,out){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_32867 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_32867[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_32867[(1)] = (1));

return statearr_32867;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_32846){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32846);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32868){if((e32868 instanceof Object)){
var ex__25941__auto__ = e32868;
var statearr_32869_32883 = state_32846;
(statearr_32869_32883[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32846);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32868;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32884 = state_32846;
state_32846 = G__32884;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_32846){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_32846);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___32872,out))
})();
var state__26001__auto__ = (function (){var statearr_32870 = f__26000__auto__.call(null);
(statearr_32870[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___32872);

return statearr_32870;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___32872,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(){
var G__32886 = arguments.length;
switch (G__32886) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__25999__auto___32955 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___32955,out){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___32955,out){
return (function (state_32924){
var state_val_32925 = (state_32924[(1)]);
if((state_val_32925 === (7))){
var inst_32920 = (state_32924[(2)]);
var state_32924__$1 = state_32924;
var statearr_32926_32956 = state_32924__$1;
(statearr_32926_32956[(2)] = inst_32920);

(statearr_32926_32956[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (1))){
var inst_32887 = (new Array(n));
var inst_32888 = inst_32887;
var inst_32889 = (0);
var state_32924__$1 = (function (){var statearr_32927 = state_32924;
(statearr_32927[(7)] = inst_32888);

(statearr_32927[(8)] = inst_32889);

return statearr_32927;
})();
var statearr_32928_32957 = state_32924__$1;
(statearr_32928_32957[(2)] = null);

(statearr_32928_32957[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (4))){
var inst_32892 = (state_32924[(9)]);
var inst_32892__$1 = (state_32924[(2)]);
var inst_32893 = (inst_32892__$1 == null);
var inst_32894 = cljs.core.not.call(null,inst_32893);
var state_32924__$1 = (function (){var statearr_32929 = state_32924;
(statearr_32929[(9)] = inst_32892__$1);

return statearr_32929;
})();
if(inst_32894){
var statearr_32930_32958 = state_32924__$1;
(statearr_32930_32958[(1)] = (5));

} else {
var statearr_32931_32959 = state_32924__$1;
(statearr_32931_32959[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (15))){
var inst_32914 = (state_32924[(2)]);
var state_32924__$1 = state_32924;
var statearr_32932_32960 = state_32924__$1;
(statearr_32932_32960[(2)] = inst_32914);

(statearr_32932_32960[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (13))){
var state_32924__$1 = state_32924;
var statearr_32933_32961 = state_32924__$1;
(statearr_32933_32961[(2)] = null);

(statearr_32933_32961[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (6))){
var inst_32889 = (state_32924[(8)]);
var inst_32910 = (inst_32889 > (0));
var state_32924__$1 = state_32924;
if(cljs.core.truth_(inst_32910)){
var statearr_32934_32962 = state_32924__$1;
(statearr_32934_32962[(1)] = (12));

} else {
var statearr_32935_32963 = state_32924__$1;
(statearr_32935_32963[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (3))){
var inst_32922 = (state_32924[(2)]);
var state_32924__$1 = state_32924;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_32924__$1,inst_32922);
} else {
if((state_val_32925 === (12))){
var inst_32888 = (state_32924[(7)]);
var inst_32912 = cljs.core.vec.call(null,inst_32888);
var state_32924__$1 = state_32924;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32924__$1,(15),out,inst_32912);
} else {
if((state_val_32925 === (2))){
var state_32924__$1 = state_32924;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_32924__$1,(4),ch);
} else {
if((state_val_32925 === (11))){
var inst_32904 = (state_32924[(2)]);
var inst_32905 = (new Array(n));
var inst_32888 = inst_32905;
var inst_32889 = (0);
var state_32924__$1 = (function (){var statearr_32936 = state_32924;
(statearr_32936[(7)] = inst_32888);

(statearr_32936[(10)] = inst_32904);

(statearr_32936[(8)] = inst_32889);

return statearr_32936;
})();
var statearr_32937_32964 = state_32924__$1;
(statearr_32937_32964[(2)] = null);

(statearr_32937_32964[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (9))){
var inst_32888 = (state_32924[(7)]);
var inst_32902 = cljs.core.vec.call(null,inst_32888);
var state_32924__$1 = state_32924;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_32924__$1,(11),out,inst_32902);
} else {
if((state_val_32925 === (5))){
var inst_32892 = (state_32924[(9)]);
var inst_32888 = (state_32924[(7)]);
var inst_32889 = (state_32924[(8)]);
var inst_32897 = (state_32924[(11)]);
var inst_32896 = (inst_32888[inst_32889] = inst_32892);
var inst_32897__$1 = (inst_32889 + (1));
var inst_32898 = (inst_32897__$1 < n);
var state_32924__$1 = (function (){var statearr_32938 = state_32924;
(statearr_32938[(11)] = inst_32897__$1);

(statearr_32938[(12)] = inst_32896);

return statearr_32938;
})();
if(cljs.core.truth_(inst_32898)){
var statearr_32939_32965 = state_32924__$1;
(statearr_32939_32965[(1)] = (8));

} else {
var statearr_32940_32966 = state_32924__$1;
(statearr_32940_32966[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (14))){
var inst_32917 = (state_32924[(2)]);
var inst_32918 = cljs.core.async.close_BANG_.call(null,out);
var state_32924__$1 = (function (){var statearr_32942 = state_32924;
(statearr_32942[(13)] = inst_32917);

return statearr_32942;
})();
var statearr_32943_32967 = state_32924__$1;
(statearr_32943_32967[(2)] = inst_32918);

(statearr_32943_32967[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (10))){
var inst_32908 = (state_32924[(2)]);
var state_32924__$1 = state_32924;
var statearr_32944_32968 = state_32924__$1;
(statearr_32944_32968[(2)] = inst_32908);

(statearr_32944_32968[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32925 === (8))){
var inst_32888 = (state_32924[(7)]);
var inst_32897 = (state_32924[(11)]);
var tmp32941 = inst_32888;
var inst_32888__$1 = tmp32941;
var inst_32889 = inst_32897;
var state_32924__$1 = (function (){var statearr_32945 = state_32924;
(statearr_32945[(7)] = inst_32888__$1);

(statearr_32945[(8)] = inst_32889);

return statearr_32945;
})();
var statearr_32946_32969 = state_32924__$1;
(statearr_32946_32969[(2)] = null);

(statearr_32946_32969[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___32955,out))
;
return ((function (switch__25937__auto__,c__25999__auto___32955,out){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_32950 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32950[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_32950[(1)] = (1));

return statearr_32950;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_32924){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_32924);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e32951){if((e32951 instanceof Object)){
var ex__25941__auto__ = e32951;
var statearr_32952_32970 = state_32924;
(statearr_32952_32970[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_32924);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e32951;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__32971 = state_32924;
state_32924 = G__32971;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_32924){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_32924);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___32955,out))
})();
var state__26001__auto__ = (function (){var statearr_32953 = f__26000__auto__.call(null);
(statearr_32953[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___32955);

return statearr_32953;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___32955,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(){
var G__32973 = arguments.length;
switch (G__32973) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__25999__auto___33046 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___33046,out){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___33046,out){
return (function (state_33015){
var state_val_33016 = (state_33015[(1)]);
if((state_val_33016 === (7))){
var inst_33011 = (state_33015[(2)]);
var state_33015__$1 = state_33015;
var statearr_33017_33047 = state_33015__$1;
(statearr_33017_33047[(2)] = inst_33011);

(statearr_33017_33047[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (1))){
var inst_32974 = [];
var inst_32975 = inst_32974;
var inst_32976 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_33015__$1 = (function (){var statearr_33018 = state_33015;
(statearr_33018[(7)] = inst_32976);

(statearr_33018[(8)] = inst_32975);

return statearr_33018;
})();
var statearr_33019_33048 = state_33015__$1;
(statearr_33019_33048[(2)] = null);

(statearr_33019_33048[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (4))){
var inst_32979 = (state_33015[(9)]);
var inst_32979__$1 = (state_33015[(2)]);
var inst_32980 = (inst_32979__$1 == null);
var inst_32981 = cljs.core.not.call(null,inst_32980);
var state_33015__$1 = (function (){var statearr_33020 = state_33015;
(statearr_33020[(9)] = inst_32979__$1);

return statearr_33020;
})();
if(inst_32981){
var statearr_33021_33049 = state_33015__$1;
(statearr_33021_33049[(1)] = (5));

} else {
var statearr_33022_33050 = state_33015__$1;
(statearr_33022_33050[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (15))){
var inst_33005 = (state_33015[(2)]);
var state_33015__$1 = state_33015;
var statearr_33023_33051 = state_33015__$1;
(statearr_33023_33051[(2)] = inst_33005);

(statearr_33023_33051[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (13))){
var state_33015__$1 = state_33015;
var statearr_33024_33052 = state_33015__$1;
(statearr_33024_33052[(2)] = null);

(statearr_33024_33052[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (6))){
var inst_32975 = (state_33015[(8)]);
var inst_33000 = inst_32975.length;
var inst_33001 = (inst_33000 > (0));
var state_33015__$1 = state_33015;
if(cljs.core.truth_(inst_33001)){
var statearr_33025_33053 = state_33015__$1;
(statearr_33025_33053[(1)] = (12));

} else {
var statearr_33026_33054 = state_33015__$1;
(statearr_33026_33054[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (3))){
var inst_33013 = (state_33015[(2)]);
var state_33015__$1 = state_33015;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33015__$1,inst_33013);
} else {
if((state_val_33016 === (12))){
var inst_32975 = (state_33015[(8)]);
var inst_33003 = cljs.core.vec.call(null,inst_32975);
var state_33015__$1 = state_33015;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33015__$1,(15),out,inst_33003);
} else {
if((state_val_33016 === (2))){
var state_33015__$1 = state_33015;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33015__$1,(4),ch);
} else {
if((state_val_33016 === (11))){
var inst_32979 = (state_33015[(9)]);
var inst_32983 = (state_33015[(10)]);
var inst_32993 = (state_33015[(2)]);
var inst_32994 = [];
var inst_32995 = inst_32994.push(inst_32979);
var inst_32975 = inst_32994;
var inst_32976 = inst_32983;
var state_33015__$1 = (function (){var statearr_33027 = state_33015;
(statearr_33027[(7)] = inst_32976);

(statearr_33027[(11)] = inst_32995);

(statearr_33027[(12)] = inst_32993);

(statearr_33027[(8)] = inst_32975);

return statearr_33027;
})();
var statearr_33028_33055 = state_33015__$1;
(statearr_33028_33055[(2)] = null);

(statearr_33028_33055[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (9))){
var inst_32975 = (state_33015[(8)]);
var inst_32991 = cljs.core.vec.call(null,inst_32975);
var state_33015__$1 = state_33015;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_33015__$1,(11),out,inst_32991);
} else {
if((state_val_33016 === (5))){
var inst_32976 = (state_33015[(7)]);
var inst_32979 = (state_33015[(9)]);
var inst_32983 = (state_33015[(10)]);
var inst_32983__$1 = f.call(null,inst_32979);
var inst_32984 = cljs.core._EQ_.call(null,inst_32983__$1,inst_32976);
var inst_32985 = cljs.core.keyword_identical_QMARK_.call(null,inst_32976,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_32986 = (inst_32984) || (inst_32985);
var state_33015__$1 = (function (){var statearr_33029 = state_33015;
(statearr_33029[(10)] = inst_32983__$1);

return statearr_33029;
})();
if(cljs.core.truth_(inst_32986)){
var statearr_33030_33056 = state_33015__$1;
(statearr_33030_33056[(1)] = (8));

} else {
var statearr_33031_33057 = state_33015__$1;
(statearr_33031_33057[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (14))){
var inst_33008 = (state_33015[(2)]);
var inst_33009 = cljs.core.async.close_BANG_.call(null,out);
var state_33015__$1 = (function (){var statearr_33033 = state_33015;
(statearr_33033[(13)] = inst_33008);

return statearr_33033;
})();
var statearr_33034_33058 = state_33015__$1;
(statearr_33034_33058[(2)] = inst_33009);

(statearr_33034_33058[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (10))){
var inst_32998 = (state_33015[(2)]);
var state_33015__$1 = state_33015;
var statearr_33035_33059 = state_33015__$1;
(statearr_33035_33059[(2)] = inst_32998);

(statearr_33035_33059[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33016 === (8))){
var inst_32979 = (state_33015[(9)]);
var inst_32975 = (state_33015[(8)]);
var inst_32983 = (state_33015[(10)]);
var inst_32988 = inst_32975.push(inst_32979);
var tmp33032 = inst_32975;
var inst_32975__$1 = tmp33032;
var inst_32976 = inst_32983;
var state_33015__$1 = (function (){var statearr_33036 = state_33015;
(statearr_33036[(7)] = inst_32976);

(statearr_33036[(8)] = inst_32975__$1);

(statearr_33036[(14)] = inst_32988);

return statearr_33036;
})();
var statearr_33037_33060 = state_33015__$1;
(statearr_33037_33060[(2)] = null);

(statearr_33037_33060[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___33046,out))
;
return ((function (switch__25937__auto__,c__25999__auto___33046,out){
return (function() {
var cljs$core$async$state_machine__25938__auto__ = null;
var cljs$core$async$state_machine__25938__auto____0 = (function (){
var statearr_33041 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33041[(0)] = cljs$core$async$state_machine__25938__auto__);

(statearr_33041[(1)] = (1));

return statearr_33041;
});
var cljs$core$async$state_machine__25938__auto____1 = (function (state_33015){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_33015);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e33042){if((e33042 instanceof Object)){
var ex__25941__auto__ = e33042;
var statearr_33043_33061 = state_33015;
(statearr_33043_33061[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33015);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33042;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33062 = state_33015;
state_33015 = G__33062;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
cljs$core$async$state_machine__25938__auto__ = function(state_33015){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__25938__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__25938__auto____1.call(this,state_33015);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__25938__auto____0;
cljs$core$async$state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__25938__auto____1;
return cljs$core$async$state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___33046,out))
})();
var state__26001__auto__ = (function (){var statearr_33044 = f__26000__auto__.call(null);
(statearr_33044[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___33046);

return statearr_33044;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___33046,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;

//# sourceMappingURL=async.js.map?rel=1446838350107