// Compiled by ClojureScript 0.0-3211 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
cljs.repl.print_doc = (function cljs$repl$print_doc(m){
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str((function (){var temp__4126__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4126__auto__)){
var ns = temp__4126__auto__;
return [cljs.core.str(ns),cljs.core.str("/")].join('');
} else {
return null;
}
})()),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__34226_34238 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__34227_34239 = null;
var count__34228_34240 = (0);
var i__34229_34241 = (0);
while(true){
if((i__34229_34241 < count__34228_34240)){
var f_34242 = cljs.core._nth.call(null,chunk__34227_34239,i__34229_34241);
cljs.core.println.call(null,"  ",f_34242);

var G__34243 = seq__34226_34238;
var G__34244 = chunk__34227_34239;
var G__34245 = count__34228_34240;
var G__34246 = (i__34229_34241 + (1));
seq__34226_34238 = G__34243;
chunk__34227_34239 = G__34244;
count__34228_34240 = G__34245;
i__34229_34241 = G__34246;
continue;
} else {
var temp__4126__auto___34247 = cljs.core.seq.call(null,seq__34226_34238);
if(temp__4126__auto___34247){
var seq__34226_34248__$1 = temp__4126__auto___34247;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__34226_34248__$1)){
var c__23560__auto___34249 = cljs.core.chunk_first.call(null,seq__34226_34248__$1);
var G__34250 = cljs.core.chunk_rest.call(null,seq__34226_34248__$1);
var G__34251 = c__23560__auto___34249;
var G__34252 = cljs.core.count.call(null,c__23560__auto___34249);
var G__34253 = (0);
seq__34226_34238 = G__34250;
chunk__34227_34239 = G__34251;
count__34228_34240 = G__34252;
i__34229_34241 = G__34253;
continue;
} else {
var f_34254 = cljs.core.first.call(null,seq__34226_34248__$1);
cljs.core.println.call(null,"  ",f_34254);

var G__34255 = cljs.core.next.call(null,seq__34226_34248__$1);
var G__34256 = null;
var G__34257 = (0);
var G__34258 = (0);
seq__34226_34238 = G__34255;
chunk__34227_34239 = G__34256;
count__34228_34240 = G__34257;
i__34229_34241 = G__34258;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
if(cljs.core.truth_((function (){var or__22775__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m));
} else {
cljs.core.prn.call(null,cljs.core.second.call(null,new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m)));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/"),cljs.core.str(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/special_forms#"),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__34230 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__34231 = null;
var count__34232 = (0);
var i__34233 = (0);
while(true){
if((i__34233 < count__34232)){
var vec__34234 = cljs.core._nth.call(null,chunk__34231,i__34233);
var name = cljs.core.nth.call(null,vec__34234,(0),null);
var map__34235 = cljs.core.nth.call(null,vec__34234,(1),null);
var map__34235__$1 = ((cljs.core.seq_QMARK_.call(null,map__34235))?cljs.core.apply.call(null,cljs.core.hash_map,map__34235):map__34235);
var arglists = cljs.core.get.call(null,map__34235__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
var doc = cljs.core.get.call(null,map__34235__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__34259 = seq__34230;
var G__34260 = chunk__34231;
var G__34261 = count__34232;
var G__34262 = (i__34233 + (1));
seq__34230 = G__34259;
chunk__34231 = G__34260;
count__34232 = G__34261;
i__34233 = G__34262;
continue;
} else {
var temp__4126__auto__ = cljs.core.seq.call(null,seq__34230);
if(temp__4126__auto__){
var seq__34230__$1 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__34230__$1)){
var c__23560__auto__ = cljs.core.chunk_first.call(null,seq__34230__$1);
var G__34263 = cljs.core.chunk_rest.call(null,seq__34230__$1);
var G__34264 = c__23560__auto__;
var G__34265 = cljs.core.count.call(null,c__23560__auto__);
var G__34266 = (0);
seq__34230 = G__34263;
chunk__34231 = G__34264;
count__34232 = G__34265;
i__34233 = G__34266;
continue;
} else {
var vec__34236 = cljs.core.first.call(null,seq__34230__$1);
var name = cljs.core.nth.call(null,vec__34236,(0),null);
var map__34237 = cljs.core.nth.call(null,vec__34236,(1),null);
var map__34237__$1 = ((cljs.core.seq_QMARK_.call(null,map__34237))?cljs.core.apply.call(null,cljs.core.hash_map,map__34237):map__34237);
var arglists = cljs.core.get.call(null,map__34237__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
var doc = cljs.core.get.call(null,map__34237__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name);

cljs.core.println.call(null," ",arglists);

if(cljs.core.truth_(doc)){
cljs.core.println.call(null," ",doc);
} else {
}

var G__34267 = cljs.core.next.call(null,seq__34230__$1);
var G__34268 = null;
var G__34269 = (0);
var G__34270 = (0);
seq__34230 = G__34267;
chunk__34231 = G__34268;
count__34232 = G__34269;
i__34233 = G__34270;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map?rel=1446838351780