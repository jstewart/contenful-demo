// Compiled by ClojureScript 0.0-3211 {}
goog.provide('re_frame.router');
goog.require('cljs.core');
goog.require('cljs.core.async');
goog.require('re_frame.utils');
goog.require('re_frame.handlers');
goog.require('reagent.core');
re_frame.router.event_chan = cljs.core.async.chan.call(null);
/**
 * read all pending events from the channel and drop them on the floor
 */
re_frame.router.purge_chan = (function re_frame$router$purge_chan(){
return null;
});
re_frame.router.router_loop = (function re_frame$router$router_loop(){
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__){
return (function (state_30533){
var state_val_30534 = (state_30533[(1)]);
if((state_val_30534 === (7))){
var inst_30518 = (state_30533[(2)]);
var state_30533__$1 = (function (){var statearr_30535 = state_30533;
(statearr_30535[(7)] = inst_30518);

return statearr_30535;
})();
var statearr_30536_30556 = state_30533__$1;
(statearr_30536_30556[(2)] = null);

(statearr_30536_30556[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30534 === (1))){
var state_30533__$1 = state_30533;
var statearr_30537_30557 = state_30533__$1;
(statearr_30537_30557[(2)] = null);

(statearr_30537_30557[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30534 === (4))){
var inst_30505 = (state_30533[(8)]);
var inst_30505__$1 = (state_30533[(2)]);
var inst_30506 = cljs.core.meta.call(null,inst_30505__$1);
var inst_30507 = new cljs.core.Keyword(null,"flush-dom","flush-dom",-933676816).cljs$core$IFn$_invoke$arity$1(inst_30506);
var state_30533__$1 = (function (){var statearr_30538 = state_30533;
(statearr_30538[(8)] = inst_30505__$1);

return statearr_30538;
})();
if(cljs.core.truth_(inst_30507)){
var statearr_30539_30558 = state_30533__$1;
(statearr_30539_30558[(1)] = (5));

} else {
var statearr_30540_30559 = state_30533__$1;
(statearr_30540_30559[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30534 === (6))){
var inst_30514 = cljs.core.async.timeout.call(null,(0));
var state_30533__$1 = state_30533;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30533__$1,(9),inst_30514);
} else {
if((state_val_30534 === (3))){
var inst_30531 = (state_30533[(2)]);
var state_30533__$1 = state_30533;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30533__$1,inst_30531);
} else {
if((state_val_30534 === (12))){
var inst_30505 = (state_30533[(8)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_30533,(11),Object,null,(10));
var inst_30526 = re_frame.handlers.handle.call(null,inst_30505);
var state_30533__$1 = state_30533;
var statearr_30541_30560 = state_30533__$1;
(statearr_30541_30560[(2)] = inst_30526);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30533__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30534 === (2))){
var state_30533__$1 = state_30533;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30533__$1,(4),re_frame.router.event_chan);
} else {
if((state_val_30534 === (11))){
var inst_30519 = (state_30533[(2)]);
var inst_30520 = re_frame.router.purge_chan.call(null);
var inst_30521 = re_frame$router$router_loop.call(null);
var inst_30522 = (function(){throw inst_30519})();
var state_30533__$1 = (function (){var statearr_30542 = state_30533;
(statearr_30542[(9)] = inst_30520);

(statearr_30542[(10)] = inst_30521);

return statearr_30542;
})();
var statearr_30543_30561 = state_30533__$1;
(statearr_30543_30561[(2)] = inst_30522);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30533__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30534 === (9))){
var inst_30516 = (state_30533[(2)]);
var state_30533__$1 = state_30533;
var statearr_30544_30562 = state_30533__$1;
(statearr_30544_30562[(2)] = inst_30516);

(statearr_30544_30562[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30534 === (5))){
var inst_30509 = reagent.core.flush.call(null);
var inst_30510 = cljs.core.async.timeout.call(null,(20));
var state_30533__$1 = (function (){var statearr_30545 = state_30533;
(statearr_30545[(11)] = inst_30509);

return statearr_30545;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30533__$1,(8),inst_30510);
} else {
if((state_val_30534 === (10))){
var inst_30528 = (state_30533[(2)]);
var state_30533__$1 = (function (){var statearr_30546 = state_30533;
(statearr_30546[(12)] = inst_30528);

return statearr_30546;
})();
var statearr_30547_30563 = state_30533__$1;
(statearr_30547_30563[(2)] = null);

(statearr_30547_30563[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30534 === (8))){
var inst_30512 = (state_30533[(2)]);
var state_30533__$1 = state_30533;
var statearr_30548_30564 = state_30533__$1;
(statearr_30548_30564[(2)] = inst_30512);

(statearr_30548_30564[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto__))
;
return ((function (switch__25937__auto__,c__25999__auto__){
return (function() {
var re_frame$router$router_loop_$_state_machine__25938__auto__ = null;
var re_frame$router$router_loop_$_state_machine__25938__auto____0 = (function (){
var statearr_30552 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30552[(0)] = re_frame$router$router_loop_$_state_machine__25938__auto__);

(statearr_30552[(1)] = (1));

return statearr_30552;
});
var re_frame$router$router_loop_$_state_machine__25938__auto____1 = (function (state_30533){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_30533);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e30553){if((e30553 instanceof Object)){
var ex__25941__auto__ = e30553;
var statearr_30554_30565 = state_30533;
(statearr_30554_30565[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30533);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30553;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30566 = state_30533;
state_30533 = G__30566;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
re_frame$router$router_loop_$_state_machine__25938__auto__ = function(state_30533){
switch(arguments.length){
case 0:
return re_frame$router$router_loop_$_state_machine__25938__auto____0.call(this);
case 1:
return re_frame$router$router_loop_$_state_machine__25938__auto____1.call(this,state_30533);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
re_frame$router$router_loop_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = re_frame$router$router_loop_$_state_machine__25938__auto____0;
re_frame$router$router_loop_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = re_frame$router$router_loop_$_state_machine__25938__auto____1;
return re_frame$router$router_loop_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__))
})();
var state__26001__auto__ = (function (){var statearr_30555 = f__26000__auto__.call(null);
(statearr_30555[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_30555;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__))
);

return c__25999__auto__;
});
re_frame.router.router_loop.call(null);
/**
 * Send an event to be processed by the registered handler.
 * 
 * Usage example:
 * (dispatch [:delete-item 42])
 * 
 */
re_frame.router.dispatch = (function re_frame$router$dispatch(event_v){
if((event_v == null)){
re_frame.utils.error.call(null,"re-frame: \"dispatch\" is ignoring a nil event.");
} else {
cljs.core.async.put_BANG_.call(null,re_frame.router.event_chan,event_v);
}

return null;
});
/**
 * Send an event to be processed by the registered handler, but avoid the async-inducing
 * use of core.async/chan.
 * 
 * Usage example:
 * (dispatch-sync [:delete-item 42])
 */
re_frame.router.dispatch_sync = (function re_frame$router$dispatch_sync(event_v){
re_frame.handlers.handle.call(null,event_v);

return null;
});

//# sourceMappingURL=router.js.map?rel=1446838348483