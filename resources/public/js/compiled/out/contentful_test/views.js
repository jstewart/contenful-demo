// Compiled by ClojureScript 0.0-3211 {}
goog.provide('contentful_test.views');
goog.require('cljs.core');
goog.require('re_frame.core');
contentful_test.views.navbar = (function contentful_test$views$navbar(){
return (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"nav.navbar.navbar-inverse.navbar-embossed","nav.navbar.navbar-inverse.navbar-embossed",-279280953),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"role","role",-736691072),"navigation"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.navbar-header","div.navbar-header",-515823511),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.navbar-toggle","button.navbar-toggle",1737318847),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"data-toggle","data-toggle",436966687),"collapse",new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"data-target","data-target",-113904678),"#main-nav",new cljs.core.Keyword(null,"aria-expanded","aria-expanded",-1360942393),"false"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.sr-only","span.sr-only",2081743235),"Toggle navigation"], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.icon-bar","span.icon-bar",618689172)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.icon-bar","span.icon-bar",618689172)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.icon-bar","span.icon-bar",618689172)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a.navbar-brand","a.navbar-brand",691442118),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"#/"], null),"Contentful Test"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#main-nav.collapse.navbar-collapse","div#main-nav.collapse.navbar-collapse",1976562169),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul.nav.navbar-nav.navbar-left","ul.nav.navbar-nav.navbar-left",2047252324),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"#/entries"], null),"Entries"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"#/spaces"], null),"Spaces"], null)], null)], null)], null)], null);
});
});
contentful_test.views.error_message = (function contentful_test$views$error_message(){
var error = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"api-error","api-error",1506636439)], null));
return ((function (error){
return (function (){
var temp__4126__auto__ = cljs.core.deref.call(null,error);
if(cljs.core.truth_(temp__4126__auto__)){
var map__29170 = temp__4126__auto__;
var map__29170__$1 = ((cljs.core.seq_QMARK_.call(null,map__29170))?cljs.core.apply.call(null,cljs.core.hash_map,map__29170):map__29170);
var response = cljs.core.get.call(null,map__29170__$1,new cljs.core.Keyword(null,"response","response",-1068424192));
var status_text = cljs.core.get.call(null,map__29170__$1,new cljs.core.Keyword(null,"status-text","status-text",-1834235478));
var status = cljs.core.get.call(null,map__29170__$1,new cljs.core.Keyword(null,"status","status",-1997798413));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.bg-danger.text-danger","p.bg-danger.text-danger",1357263483),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong","strong",269529000),"Error: "], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span","span",1394872991),[cljs.core.str(status),cljs.core.str(" - "),cljs.core.str(response)].join('')], null)], null);
} else {
return null;
}
});
;})(error))
});
contentful_test.views.home_panel = (function contentful_test$views$home_panel(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"Contentful Test"], null)], null);
});
contentful_test.views.entry_panel = (function contentful_test$views$entry_panel(){
var current_entry = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"current-entry","current-entry",1312484279)], null));
return ((function (current_entry){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.entry","div.entry",-1702818662),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),(function (){var iter__23529__auto__ = ((function (current_entry){
return (function contentful_test$views$entry_panel_$_iter__29179(s__29180){
return (new cljs.core.LazySeq(null,((function (current_entry){
return (function (){
var s__29180__$1 = s__29180;
while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__29180__$1);
if(temp__4126__auto__){
var s__29180__$2 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__29180__$2)){
var c__23527__auto__ = cljs.core.chunk_first.call(null,s__29180__$2);
var size__23528__auto__ = cljs.core.count.call(null,c__23527__auto__);
var b__29182 = cljs.core.chunk_buffer.call(null,size__23528__auto__);
if((function (){var i__29181 = (0);
while(true){
if((i__29181 < size__23528__auto__)){
var vec__29185 = cljs.core._nth.call(null,c__23527__auto__,i__29181);
var title = cljs.core.nth.call(null,vec__29185,(0),null);
var value = cljs.core.nth.call(null,vec__29185,(1),null);
cljs.core.chunk_append.call(null,b__29182,cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong","strong",269529000),[cljs.core.str(cljs.core.name.call(null,title)),cljs.core.str(": "),cljs.core.str(value)].join('')], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),title], null)));

var G__29187 = (i__29181 + (1));
i__29181 = G__29187;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__29182),contentful_test$views$entry_panel_$_iter__29179.call(null,cljs.core.chunk_rest.call(null,s__29180__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__29182),null);
}
} else {
var vec__29186 = cljs.core.first.call(null,s__29180__$2);
var title = cljs.core.nth.call(null,vec__29186,(0),null);
var value = cljs.core.nth.call(null,vec__29186,(1),null);
return cljs.core.cons.call(null,cljs.core.with_meta(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"strong","strong",269529000),[cljs.core.str(cljs.core.name.call(null,title)),cljs.core.str(": "),cljs.core.str(value)].join('')], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),title], null)),contentful_test$views$entry_panel_$_iter__29179.call(null,cljs.core.rest.call(null,s__29180__$2)));
}
} else {
return null;
}
break;
}
});})(current_entry))
,null,null));
});})(current_entry))
;
return iter__23529__auto__.call(null,new cljs.core.Keyword(null,"fields","fields",-1932066230).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,current_entry)));
})()], null)], null);
});
;})(current_entry))
});
contentful_test.views.entries_panel = (function contentful_test$views$entries_panel(){
var entries = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"entries","entries",-86943161)], null));
return ((function (entries){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"Entries"], null),((cljs.core.seq.call(null,cljs.core.deref.call(null,entries)))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"table.table.table-striped.table-hover","table.table.table-striped.table-hover",726875574),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"thead","thead",-291875296),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"ID"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Type"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Slug"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Updated"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tbody","tbody",-80678300),(function (){var iter__23529__auto__ = ((function (entries){
return (function contentful_test$views$entries_panel_$_iter__29192(s__29193){
return (new cljs.core.LazySeq(null,((function (entries){
return (function (){
var s__29193__$1 = s__29193;
while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__29193__$1);
if(temp__4126__auto__){
var s__29193__$2 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__29193__$2)){
var c__23527__auto__ = cljs.core.chunk_first.call(null,s__29193__$2);
var size__23528__auto__ = cljs.core.count.call(null,c__23527__auto__);
var b__29195 = cljs.core.chunk_buffer.call(null,size__23528__auto__);
if((function (){var i__29194 = (0);
while(true){
if((i__29194 < size__23528__auto__)){
var entry = cljs.core._nth.call(null,c__23527__auto__,i__29194);
cljs.core.chunk_append.call(null,b__29195,(function (){var id = new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(entry));
return cljs.core.with_meta(new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),[cljs.core.str("#/entries/"),cljs.core.str(id)].join('')], null),id], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(entry))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"slug","slug",2029314850).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"fields","fields",-1932066230).cljs$core$IFn$_invoke$arity$1(entry))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"updatedAt","updatedAt",1796679523).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(entry))], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),id], null));
})());

var G__29196 = (i__29194 + (1));
i__29194 = G__29196;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__29195),contentful_test$views$entries_panel_$_iter__29192.call(null,cljs.core.chunk_rest.call(null,s__29193__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__29195),null);
}
} else {
var entry = cljs.core.first.call(null,s__29193__$2);
return cljs.core.cons.call(null,(function (){var id = new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(entry));
return cljs.core.with_meta(new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),[cljs.core.str("#/entries/"),cljs.core.str(id)].join('')], null),id], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(entry))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"slug","slug",2029314850).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"fields","fields",-1932066230).cljs$core$IFn$_invoke$arity$1(entry))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"updatedAt","updatedAt",1796679523).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(entry))], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),id], null));
})(),contentful_test$views$entries_panel_$_iter__29192.call(null,cljs.core.rest.call(null,s__29193__$2)));
}
} else {
return null;
}
break;
}
});})(entries))
,null,null));
});})(entries))
;
return iter__23529__auto__.call(null,cljs.core.deref.call(null,entries));
})()], null)], null):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"No entries to display"], null))], null);
});
;})(entries))
});
contentful_test.views.spaces_panel = (function contentful_test$views$spaces_panel(){
var spaces = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"spaces","spaces",365984563)], null));
return ((function (spaces){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"Spaces"], null),((cljs.core.seq.call(null,cljs.core.deref.call(null,spaces)))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"table.table.table-striped.table-hover","table.table.table-striped.table-hover",726875574),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"thead","thead",-291875296),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"ID"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Type"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Slug"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Updated"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tbody","tbody",-80678300),(function (){var iter__23529__auto__ = ((function (spaces){
return (function contentful_test$views$spaces_panel_$_iter__29201(s__29202){
return (new cljs.core.LazySeq(null,((function (spaces){
return (function (){
var s__29202__$1 = s__29202;
while(true){
var temp__4126__auto__ = cljs.core.seq.call(null,s__29202__$1);
if(temp__4126__auto__){
var s__29202__$2 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__29202__$2)){
var c__23527__auto__ = cljs.core.chunk_first.call(null,s__29202__$2);
var size__23528__auto__ = cljs.core.count.call(null,c__23527__auto__);
var b__29204 = cljs.core.chunk_buffer.call(null,size__23528__auto__);
if((function (){var i__29203 = (0);
while(true){
if((i__29203 < size__23528__auto__)){
var space = cljs.core._nth.call(null,c__23527__auto__,i__29203);
cljs.core.chunk_append.call(null,b__29204,(function (){var id = new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(space));
return cljs.core.with_meta(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(space))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"slug","slug",2029314850).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"fields","fields",-1932066230).cljs$core$IFn$_invoke$arity$1(space))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"updatedAt","updatedAt",1796679523).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(space))], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),id], null));
})());

var G__29205 = (i__29203 + (1));
i__29203 = G__29205;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__29204),contentful_test$views$spaces_panel_$_iter__29201.call(null,cljs.core.chunk_rest.call(null,s__29202__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__29204),null);
}
} else {
var space = cljs.core.first.call(null,s__29202__$2);
return cljs.core.cons.call(null,(function (){var id = new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(space));
return cljs.core.with_meta(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(space))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"slug","slug",2029314850).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"fields","fields",-1932066230).cljs$core$IFn$_invoke$arity$1(space))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.Keyword(null,"updatedAt","updatedAt",1796679523).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"sys","sys",-592279430).cljs$core$IFn$_invoke$arity$1(space))], null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),id], null));
})(),contentful_test$views$spaces_panel_$_iter__29201.call(null,cljs.core.rest.call(null,s__29202__$2)));
}
} else {
return null;
}
break;
}
});})(spaces))
,null,null));
});})(spaces))
;
return iter__23529__auto__.call(null,cljs.core.deref.call(null,spaces));
})()], null)], null):new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"No spaces to display"], null))], null);
});
;})(spaces))
});
if(typeof contentful_test.views.panels !== 'undefined'){
} else {
contentful_test.views.panels = (function (){var method_table__23670__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__23671__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__23672__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__23673__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__23674__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"contentful-test.views","panels"),cljs.core.identity,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__23674__auto__,method_table__23670__auto__,prefer_table__23671__auto__,method_cache__23672__auto__,cached_hierarchy__23673__auto__));
})();
}
cljs.core._add_method.call(null,contentful_test.views.panels,new cljs.core.Keyword(null,"home-panel","home-panel",1226198754),(function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [contentful_test.views.home_panel], null);
}));
cljs.core._add_method.call(null,contentful_test.views.panels,new cljs.core.Keyword(null,"entries-panel","entries-panel",236233826),(function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [contentful_test.views.entries_panel], null);
}));
cljs.core._add_method.call(null,contentful_test.views.panels,new cljs.core.Keyword(null,"entry-panel","entry-panel",1378907019),(function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [contentful_test.views.entry_panel], null);
}));
cljs.core._add_method.call(null,contentful_test.views.panels,new cljs.core.Keyword(null,"spaces-panel","spaces-panel",795844418),(function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [contentful_test.views.spaces_panel], null);
}));
cljs.core._add_method.call(null,contentful_test.views.panels,new cljs.core.Keyword(null,"default","default",-1987822328),(function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632)], null);
}));
contentful_test.views.main_panel = (function contentful_test$views$main_panel(){
var active_panel = re_frame.core.subscribe.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"active-panel","active-panel",-1802545994)], null));
return ((function (active_panel){
return (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [contentful_test.views.navbar], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [contentful_test.views.error_message], null),contentful_test.views.panels.call(null,cljs.core.deref.call(null,active_panel))], null);
});
;})(active_panel))
});

//# sourceMappingURL=views.js.map?rel=1446846988864