// Compiled by ClojureScript 0.0-3211 {}
goog.provide('contentful_test.routes');
goog.require('cljs.core');
goog.require('re_frame.core');
goog.require('goog.history.EventType');
goog.require('goog.events');
goog.require('secretary.core');
goog.require('goog.History');
contentful_test.routes.hook_browser_navigation_BANG_ = (function contentful_test$routes$hook_browser_navigation_BANG_(){
var G__29270 = (new goog.History());
goog.events.listen(G__29270,goog.history.EventType.NAVIGATE,((function (G__29270){
return (function (event){
return secretary.core.dispatch_BANG_.call(null,event.token);
});})(G__29270))
);

G__29270.setEnabled(true);

return G__29270;
});
contentful_test.routes.app_routes = (function contentful_test$routes$app_routes(){
secretary.core.set_config_BANG_.call(null,new cljs.core.Keyword(null,"prefix","prefix",-265908465),"#");

var action__25860__auto___29287 = (function (params__25861__auto__){
if(cljs.core.map_QMARK_.call(null,params__25861__auto__)){
var map__29279 = params__25861__auto__;
var map__29279__$1 = ((cljs.core.seq_QMARK_.call(null,map__29279))?cljs.core.apply.call(null,cljs.core.hash_map,map__29279):map__29279);
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"home-panel","home-panel",1226198754)], null));
} else {
if(cljs.core.vector_QMARK_.call(null,params__25861__auto__)){
var vec__29280 = params__25861__auto__;
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"home-panel","home-panel",1226198754)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/",action__25860__auto___29287);


var action__25860__auto___29288 = (function (params__25861__auto__){
if(cljs.core.map_QMARK_.call(null,params__25861__auto__)){
var map__29281 = params__25861__auto__;
var map__29281__$1 = ((cljs.core.seq_QMARK_.call(null,map__29281))?cljs.core.apply.call(null,cljs.core.hash_map,map__29281):map__29281);
re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"load-entries","load-entries",-1010074927)], null));

return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"entries-panel","entries-panel",236233826)], null));
} else {
if(cljs.core.vector_QMARK_.call(null,params__25861__auto__)){
var vec__29282 = params__25861__auto__;
re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"load-entries","load-entries",-1010074927)], null));

return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"entries-panel","entries-panel",236233826)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/entries",action__25860__auto___29288);


var action__25860__auto___29289 = (function (params__25861__auto__){
if(cljs.core.map_QMARK_.call(null,params__25861__auto__)){
var map__29283 = params__25861__auto__;
var map__29283__$1 = ((cljs.core.seq_QMARK_.call(null,map__29283))?cljs.core.apply.call(null,cljs.core.hash_map,map__29283):map__29283);
var id = cljs.core.get.call(null,map__29283__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"load-entry","load-entry",2126066654),id], null));

return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"entry-panel","entry-panel",1378907019)], null));
} else {
if(cljs.core.vector_QMARK_.call(null,params__25861__auto__)){
var vec__29284 = params__25861__auto__;
var id = cljs.core.nth.call(null,vec__29284,(0),null);
re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"load-entry","load-entry",2126066654),id], null));

return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"entry-panel","entry-panel",1378907019)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/entries/:id",action__25860__auto___29289);


var action__25860__auto___29290 = (function (params__25861__auto__){
if(cljs.core.map_QMARK_.call(null,params__25861__auto__)){
var map__29285 = params__25861__auto__;
var map__29285__$1 = ((cljs.core.seq_QMARK_.call(null,map__29285))?cljs.core.apply.call(null,cljs.core.hash_map,map__29285):map__29285);
re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"load-spaces","load-spaces",-1851802176)], null));

return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"spaces-panel","spaces-panel",795844418)], null));
} else {
if(cljs.core.vector_QMARK_.call(null,params__25861__auto__)){
var vec__29286 = params__25861__auto__;
re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"load-spaces","load-spaces",-1851802176)], null));

return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),new cljs.core.Keyword(null,"spaces-panel","spaces-panel",795844418)], null));
} else {
return null;
}
}
});
secretary.core.add_route_BANG_.call(null,"/spaces",action__25860__auto___29290);


return contentful_test.routes.hook_browser_navigation_BANG_.call(null);
});

//# sourceMappingURL=routes.js.map?rel=1446847433502