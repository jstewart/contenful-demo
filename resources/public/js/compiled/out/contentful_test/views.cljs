(ns contentful-test.views
    (:require [re-frame.core :as re-frame]))

;; --------------------
;; Bootstrap stuff
(defn navbar
  []
  (fn []
    [:nav.navbar.navbar-inverse.navbar-embossed {:role "navigation"}
     [:div.navbar-header
      [:button.navbar-toggle
       {:data-toggle "collapse"
        :type "button"
        :data-target "#main-nav"
        :aria-expanded "false"}
       [:span.sr-only "Toggle navigation"]
       [:span.icon-bar]
       [:span.icon-bar]
       [:span.icon-bar]]
      [:a.navbar-brand {:href "#/"} "Contentful Test"]]
     [:div#main-nav.collapse.navbar-collapse
      [:ul.nav.navbar-nav.navbar-left
       [:li [:a {:href "#/entries"} "Entries"]]
       [:li [:a {:href "#/spaces"} "Spaces"]]]]]))

(defn error-message
  []
  (let [error (re-frame/subscribe [:api-error])]
    (fn []
      (when-let [{:keys [status status-text response]} @error]
        [:p.bg-danger.text-danger
         [:strong "Error: "]
         [:span (str status " - " response)]]))))

;; --------------------
;; Panels

(defn home-panel
  []
  [:div
   [:h1 "Contentful Test"]])

(defn entry-panel
  []
  (let [current-entry (re-frame/subscribe [:current-entry])]
    (fn []
      [:div.entry
       [:ul
        (for [[title value] (:fields @current-entry)]
          ^{:key title}
          [:li
           [:strong (str (name title) ": " value)]])]])))

(defn entries-panel
  []
  (let [entries (re-frame/subscribe [:entries])]
    (fn []
      [:div
       [:h1 "Entries"]
       (if (seq @entries)
         [:table.table.table-striped.table-hover
          [:thead
           [:th "ID"]
           [:th "Type"]
           [:th "Slug"]
           [:th "Updated"]]
          [:tbody
           (for [entry @entries]
             (let [id (-> entry :sys :id)]
               ^{:key id}
               [:tr
                [:td [:a {:href (str "#/entries/" id)} id]]
                [:td (-> entry :sys :type)]
                [:td (-> entry :fields :slug)]
                [:td (-> entry :sys :updatedAt)]]))]]
         [:div "No entries to display"])])))

(defn spaces-panel
  []
  (let [spaces (re-frame/subscribe [:spaces])]
    (fn []
      [:div
       [:h1 "Spaces"]
       (if (seq @spaces)
         [:table.table.table-striped.table-hover
          [:thead
           [:th "ID"]
           [:th "Type"]
           [:th "Slug"]
           [:th "Updated"]]
          [:tbody
           (for [space @spaces]
             (let [id (-> space :sys :id)]
               ^{:key id}
               [:tr
                [:td (-> space :sys :type)]
                [:td (-> space :fields :slug)]
                [:td (-> space :sys :updatedAt)]]))]]
         [:div "No spaces to display"])])))

;; --------------------
(defmulti panels identity)
(defmethod panels :home-panel [] [home-panel])
(defmethod panels :entries-panel [] [entries-panel])
(defmethod panels :entry-panel [] [entry-panel])
(defmethod panels :spaces-panel [] [spaces-panel])
(defmethod panels :default [] [:div])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [:active-panel])]
    (fn []
      [:div
       [navbar]
       [error-message]
       (panels @active-panel)])))
