(ns contentful-test.db)

(def default-db
  {:entries []
   :current-entry nil
   :spaces []
   :loading? false
   :api-error nil})
