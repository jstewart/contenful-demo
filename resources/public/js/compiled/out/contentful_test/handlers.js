// Compiled by ClojureScript 0.0-3211 {}
goog.provide('contentful_test.handlers');
goog.require('cljs.core');
goog.require('contentful_test.db');
goog.require('ajax.core');
goog.require('re_frame.core');
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"initialize-db","initialize-db",230998432),(function (_,___$1){
return contentful_test.db.default_db;
}));
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"set-active-panel","set-active-panel",-965871124),(function (db,p__29294){
var vec__29295 = p__29294;
var _ = cljs.core.nth.call(null,vec__29295,(0),null);
var active_panel = cljs.core.nth.call(null,vec__29295,(1),null);
return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"active-panel","active-panel",-1802545994),active_panel);
}));
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"load-entries","load-entries",-1010074927),(function (db,_){
ajax.core.GET.call(null,[cljs.core.str("https://cdn.contentful.com/spaces/"),cljs.core.str(space),cljs.core.str("/entries?access_token="),cljs.core.str(access_token)].join(''),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handler","handler",-195596612),(function (p1__29296_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"process-response","process-response",-26308683),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"entries","entries",-86943161),p1__29296_SHARP_], null)], null));
}),new cljs.core.Keyword(null,"error-handler","error-handler",-484945776),(function (p1__29297_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"api-error","api-error",1506636439),p1__29297_SHARP_], null));
}),new cljs.core.Keyword(null,"response-format","response-format",1664465322),new cljs.core.Keyword(null,"json","json",1279968570),new cljs.core.Keyword(null,"keywords?","keywords?",764949733),true], null));

return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"loading?","loading?",1905707049),true);
}));
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"load-spaces","load-spaces",-1851802176),(function (db,_){
ajax.core.GET.call(null,[cljs.core.str("https://cdn.contentful.com/spaces?access_token="),cljs.core.str(access_token)].join(''),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handler","handler",-195596612),(function (p1__29298_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"process-response","process-response",-26308683),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"spaces","spaces",365984563),p1__29298_SHARP_], null)], null));
}),new cljs.core.Keyword(null,"error-handler","error-handler",-484945776),(function (p1__29299_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"api-error","api-error",1506636439),p1__29299_SHARP_], null));
}),new cljs.core.Keyword(null,"response-format","response-format",1664465322),new cljs.core.Keyword(null,"json","json",1279968570),new cljs.core.Keyword(null,"keywords?","keywords?",764949733),true], null));

return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"loading?","loading?",1905707049),true);
}));
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"load-entry","load-entry",2126066654),(function (db,p__29302){
var vec__29303 = p__29302;
var _ = cljs.core.nth.call(null,vec__29303,(0),null);
var id = cljs.core.nth.call(null,vec__29303,(1),null);
ajax.core.GET.call(null,[cljs.core.str("https://cdn.contentful.com/spaces/"),cljs.core.str(space),cljs.core.str("/entries/"),cljs.core.str(id),cljs.core.str("?access_token="),cljs.core.str(access_token)].join(''),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handler","handler",-195596612),((function (vec__29303,_,id){
return (function (p1__29300_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"process-response","process-response",-26308683),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"entry","entry",505168823),p1__29300_SHARP_], null)], null));
});})(vec__29303,_,id))
,new cljs.core.Keyword(null,"error-handler","error-handler",-484945776),((function (vec__29303,_,id){
return (function (p1__29301_SHARP_){
return re_frame.core.dispatch.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"api-error","api-error",1506636439),p1__29301_SHARP_], null));
});})(vec__29303,_,id))
,new cljs.core.Keyword(null,"response-format","response-format",1664465322),new cljs.core.Keyword(null,"json","json",1279968570),new cljs.core.Keyword(null,"keywords?","keywords?",764949733),true], null));

return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"loading?","loading?",1905707049),true);
}));
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"process-response","process-response",-26308683),(function (db,p__29304){
var vec__29305 = p__29304;
var _ = cljs.core.nth.call(null,vec__29305,(0),null);
var vec__29306 = cljs.core.nth.call(null,vec__29305,(1),null);
var type = cljs.core.nth.call(null,vec__29306,(0),null);
var response = cljs.core.nth.call(null,vec__29306,(1),null);
var thisdb = cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"loading?","loading?",1905707049),false,new cljs.core.Keyword(null,"api-error","api-error",1506636439),null);
var pred__29307 = cljs.core._EQ_;
var expr__29308 = type;
if(cljs.core.truth_(pred__29307.call(null,new cljs.core.Keyword(null,"entries","entries",-86943161),expr__29308))){
return cljs.core.assoc.call(null,thisdb,new cljs.core.Keyword(null,"entries","entries",-86943161),new cljs.core.Keyword(null,"items","items",1031954938).cljs$core$IFn$_invoke$arity$1(response));
} else {
if(cljs.core.truth_(pred__29307.call(null,new cljs.core.Keyword(null,"spaces","spaces",365984563),expr__29308))){
return cljs.core.assoc.call(null,thisdb,new cljs.core.Keyword(null,"spaces","spaces",365984563),new cljs.core.Keyword(null,"items","items",1031954938).cljs$core$IFn$_invoke$arity$1(response));
} else {
if(cljs.core.truth_(pred__29307.call(null,new cljs.core.Keyword(null,"entry","entry",505168823),expr__29308))){
return cljs.core.assoc.call(null,thisdb,new cljs.core.Keyword(null,"current-entry","current-entry",1312484279),response);
} else {
if(cljs.core.truth_(pred__29307.call(null,new cljs.core.Keyword(null,"default","default",-1987822328),expr__29308))){
return thisdb;
} else {
throw (new Error([cljs.core.str("No matching clause: "),cljs.core.str(expr__29308)].join('')));
}
}
}
}
}));
re_frame.core.register_handler.call(null,new cljs.core.Keyword(null,"api-error","api-error",1506636439),(function (db,p__29310){
var vec__29311 = p__29310;
var _ = cljs.core.nth.call(null,vec__29311,(0),null);
var error = cljs.core.nth.call(null,vec__29311,(1),null);
return cljs.core.assoc.call(null,db,new cljs.core.Keyword(null,"api-error","api-error",1506636439),error);
}));

//# sourceMappingURL=handlers.js.map?rel=1446847527079