(ns contentful-test.core
    (:require [reagent.core :as reagent]
              [re-frame.core :as re-frame]
              [contentful-test.handlers]
              [contentful-test.subs]
              [contentful-test.routes :as routes]
              [contentful-test.views :as views]))

(defn mount-root []
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [:initialize-db])
  (mount-root))
