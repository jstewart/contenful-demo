// Compiled by ClojureScript 0.0-3211 {}
goog.provide('contentful_test.db');
goog.require('cljs.core');
contentful_test.db.default_db = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"entries","entries",-86943161),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword(null,"current-entry","current-entry",1312484279),null,new cljs.core.Keyword(null,"spaces","spaces",365984563),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword(null,"loading?","loading?",1905707049),false,new cljs.core.Keyword(null,"api-error","api-error",1506636439),null], null);

//# sourceMappingURL=db.js.map?rel=1446845395045