// Compiled by ClojureScript 0.0-3211 {}
goog.provide('contentful_test.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('contentful_test.views');
goog.require('contentful_test.routes');
goog.require('contentful_test.handlers');
goog.require('contentful_test.subs');
goog.require('re_frame.core');
contentful_test.core.mount_root = (function contentful_test$core$mount_root(){
return reagent.core.render.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [contentful_test.views.main_panel], null),document.getElementById("app"));
});
contentful_test.core.init = (function contentful_test$core$init(){
contentful_test.routes.app_routes.call(null);

re_frame.core.dispatch_sync.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"initialize-db","initialize-db",230998432)], null));

return contentful_test.core.mount_root.call(null);
});
goog.exportSymbol('contentful_test.core.init', contentful_test.core.init);

//# sourceMappingURL=core.js.map?rel=1446838558102