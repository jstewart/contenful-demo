// Compiled by ClojureScript 0.0-3211 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('goog.Uri');
goog.require('goog.string');
goog.require('goog.net.jsloader');
goog.require('cljs.core.async');
goog.require('clojure.set');
goog.require('clojure.string');
goog.require('figwheel.client.utils');

figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.all_QMARK_ = (function figwheel$client$file_reloading$all_QMARK_(pred,coll){
return cljs.core.reduce.call(null,(function (p1__34273_SHARP_,p2__34274_SHARP_){
var and__22763__auto__ = p1__34273_SHARP_;
if(cljs.core.truth_(and__22763__auto__)){
return p2__34274_SHARP_;
} else {
return and__22763__auto__;
}
}),true,cljs.core.map.call(null,pred,coll));
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__22775__auto__ = (cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372)));
if(or__22775__auto__){
return or__22775__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.ns_to_js_file = (function figwheel$client$file_reloading$ns_to_js_file(ns){

return [cljs.core.str(clojure.string.replace.call(null,ns,".","/")),cljs.core.str(".js")].join('');
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){

return [cljs.core.str(figwheel.client.utils.base_url_path.call(null)),cljs.core.str(figwheel.client.file_reloading.ns_to_js_file.call(null,ns))].join('');
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
goog.isProvided = (function (x){
return false;
});

if(((cljs.core._STAR_loaded_libs_STAR_ == null)) || (cljs.core.empty_QMARK_.call(null,cljs.core._STAR_loaded_libs_STAR_))){
cljs.core._STAR_loaded_libs_STAR_ = (function (){var gntp = goog.dependencies_.nameToPath;
return cljs.core.into.call(null,cljs.core.PersistentHashSet.EMPTY,cljs.core.filter.call(null,((function (gntp){
return (function (name){
return (goog.dependencies_.visited[(gntp[name])]);
});})(gntp))
,cljs.core.js_keys.call(null,gntp)));
})();
} else {
}

goog.require = (function (name,reload){
if(cljs.core.truth_((function (){var or__22775__auto__ = !(cljs.core.contains_QMARK_.call(null,cljs.core._STAR_loaded_libs_STAR_,name));
if(or__22775__auto__){
return or__22775__auto__;
} else {
return reload;
}
})())){
cljs.core._STAR_loaded_libs_STAR_ = cljs.core.conj.call(null,(function (){var or__22775__auto__ = cljs.core._STAR_loaded_libs_STAR_;
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return cljs.core.PersistentHashSet.EMPTY;
}
})(),name);

return figwheel.client.file_reloading.reload_file_STAR_.call(null,figwheel.client.file_reloading.resolve_ns.call(null,name));
} else {
return null;
}
});

goog.provide = goog.exportPath_;

return goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.reload_file_STAR_;
});
if(typeof figwheel.client.file_reloading.resolve_url !== 'undefined'){
} else {
figwheel.client.file_reloading.resolve_url = (function (){var method_table__23670__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__23671__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__23672__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__23673__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__23674__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"figwheel.client.file-reloading","resolve-url"),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__23674__auto__,method_table__23670__auto__,prefer_table__23671__auto__,method_cache__23672__auto__,cached_hierarchy__23673__auto__));
})();
}
cljs.core._add_method.call(null,figwheel.client.file_reloading.resolve_url,new cljs.core.Keyword(null,"default","default",-1987822328),(function (p__34275){
var map__34276 = p__34275;
var map__34276__$1 = ((cljs.core.seq_QMARK_.call(null,map__34276))?cljs.core.apply.call(null,cljs.core.hash_map,map__34276):map__34276);
var file = cljs.core.get.call(null,map__34276__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
return file;
}));
cljs.core._add_method.call(null,figwheel.client.file_reloading.resolve_url,new cljs.core.Keyword(null,"namespace","namespace",-377510372),(function (p__34277){
var map__34278 = p__34277;
var map__34278__$1 = ((cljs.core.seq_QMARK_.call(null,map__34278))?cljs.core.apply.call(null,cljs.core.hash_map,map__34278):map__34278);
var namespace = cljs.core.get.call(null,map__34278__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

return figwheel.client.file_reloading.resolve_ns.call(null,namespace);
}));
if(typeof figwheel.client.file_reloading.reload_base !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_base = (function (){var method_table__23670__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__23671__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__23672__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__23673__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__23674__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"figwheel.client.file-reloading","reload-base"),figwheel.client.utils.host_env_QMARK_,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__23674__auto__,method_table__23670__auto__,prefer_table__23671__auto__,method_cache__23672__auto__,cached_hierarchy__23673__auto__));
})();
}
cljs.core._add_method.call(null,figwheel.client.file_reloading.reload_base,new cljs.core.Keyword(null,"node","node",581201198),(function (request_url,callback){

var root = clojure.string.join.call(null,"/",cljs.core.reverse.call(null,cljs.core.drop.call(null,(2),cljs.core.reverse.call(null,clojure.string.split.call(null,__dirname,"/")))));
var path = [cljs.core.str(root),cljs.core.str("/"),cljs.core.str(request_url)].join('');
(require.cache[path] = null);

return callback.call(null,(function (){try{return require(path);
}catch (e34279){if((e34279 instanceof Error)){
var e = e34279;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e34279;

}
}})());
}));
cljs.core._add_method.call(null,figwheel.client.file_reloading.reload_base,new cljs.core.Keyword(null,"html","html",-998796897),(function (request_url,callback){

var deferred = goog.net.jsloader.load(figwheel.client.file_reloading.add_cache_buster.call(null,request_url),{"cleanupWhenDone": true});
deferred.addCallback(((function (deferred){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(deferred))
);

return deferred.addErrback(((function (deferred){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(deferred))
);
}));
figwheel.client.file_reloading.reload_file_STAR_ = (function figwheel$client$file_reloading$reload_file_STAR_(){
var G__34281 = arguments.length;
switch (G__34281) {
case 2:
return figwheel.client.file_reloading.reload_file_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return figwheel.client.file_reloading.reload_file_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

figwheel.client.file_reloading.reload_file_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (request_url,callback){
return figwheel.client.file_reloading.reload_base.call(null,request_url,callback);
});

figwheel.client.file_reloading.reload_file_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (request_url){
return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,cljs.core.identity);
});

figwheel.client.file_reloading.reload_file_STAR_.cljs$lang$maxFixedArity = 2;
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__34283,callback){
var map__34285 = p__34283;
var map__34285__$1 = ((cljs.core.seq_QMARK_.call(null,map__34285))?cljs.core.apply.call(null,cljs.core.hash_map,map__34285):map__34285);
var file_msg = map__34285__$1;
var request_url = cljs.core.get.call(null,map__34285__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Attempting to load "),cljs.core.str(request_url)].join(''));

return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,((function (map__34285,map__34285__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Successfullly loaded "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__34285,map__34285__$1,file_msg,request_url))
);
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__34286){
var map__34288 = p__34286;
var map__34288__$1 = ((cljs.core.seq_QMARK_.call(null,map__34288))?cljs.core.apply.call(null,cljs.core.hash_map,map__34288):map__34288);
var file_msg = map__34288__$1;
var meta_data = cljs.core.get.call(null,map__34288__$1,new cljs.core.Keyword(null,"meta-data","meta-data",-1613399157));
var namespace = cljs.core.get.call(null,map__34288__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_data__$1 = (function (){var or__22775__auto__ = meta_data;
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
})();
var and__22763__auto__ = cljs.core.not.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_data__$1));
if(and__22763__auto__){
var or__22775__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_data__$1);
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_data__$1);
if(cljs.core.truth_(or__22775__auto____$1)){
return or__22775__auto____$1;
} else {
var and__22763__auto____$1 = cljs.core.contains_QMARK_.call(null,cljs.core._STAR_loaded_libs_STAR_,namespace);
if(and__22763__auto____$1){
var or__22775__auto____$2 = !(cljs.core.contains_QMARK_.call(null,meta_data__$1,new cljs.core.Keyword(null,"file-changed-on-disk","file-changed-on-disk",1086171932)));
if(or__22775__auto____$2){
return or__22775__auto____$2;
} else {
return new cljs.core.Keyword(null,"file-changed-on-disk","file-changed-on-disk",1086171932).cljs$core$IFn$_invoke$arity$1(meta_data__$1);
}
} else {
return and__22763__auto____$1;
}
}
}
} else {
return and__22763__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__34289,callback){
var map__34291 = p__34289;
var map__34291__$1 = ((cljs.core.seq_QMARK_.call(null,map__34291))?cljs.core.apply.call(null,cljs.core.hash_map,map__34291):map__34291);
var file_msg = map__34291__$1;
var namespace = cljs.core.get.call(null,map__34291__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = cljs.core.get.call(null,map__34291__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.reload_file.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Figwheel: Not trying to load file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
setTimeout(((function (out){
return (function (){
return figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
figwheel.client.file_reloading.patch_goog_base.call(null);

cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);
});})(out))
,(0));

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__25999__auto___34378 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___34378,out){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___34378,out){
return (function (state_34360){
var state_val_34361 = (state_34360[(1)]);
if((state_val_34361 === (7))){
var inst_34344 = (state_34360[(7)]);
var inst_34350 = (state_34360[(2)]);
var inst_34351 = cljs.core.async.put_BANG_.call(null,out,inst_34350);
var inst_34340 = inst_34344;
var state_34360__$1 = (function (){var statearr_34362 = state_34360;
(statearr_34362[(8)] = inst_34340);

(statearr_34362[(9)] = inst_34351);

return statearr_34362;
})();
var statearr_34363_34379 = state_34360__$1;
(statearr_34363_34379[(2)] = null);

(statearr_34363_34379[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34361 === (6))){
var inst_34356 = (state_34360[(2)]);
var state_34360__$1 = state_34360;
var statearr_34364_34380 = state_34360__$1;
(statearr_34364_34380[(2)] = inst_34356);

(statearr_34364_34380[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34361 === (5))){
var inst_34354 = cljs.core.async.close_BANG_.call(null,out);
var state_34360__$1 = state_34360;
var statearr_34365_34381 = state_34360__$1;
(statearr_34365_34381[(2)] = inst_34354);

(statearr_34365_34381[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34361 === (4))){
var inst_34343 = (state_34360[(10)]);
var inst_34348 = figwheel.client.file_reloading.reload_js_file.call(null,inst_34343);
var state_34360__$1 = state_34360;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34360__$1,(7),inst_34348);
} else {
if((state_val_34361 === (3))){
var inst_34358 = (state_34360[(2)]);
var state_34360__$1 = state_34360;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_34360__$1,inst_34358);
} else {
if((state_val_34361 === (2))){
var inst_34340 = (state_34360[(8)]);
var inst_34343 = (state_34360[(10)]);
var inst_34343__$1 = cljs.core.nth.call(null,inst_34340,(0),null);
var inst_34344 = cljs.core.nthnext.call(null,inst_34340,(1));
var inst_34345 = (inst_34343__$1 == null);
var inst_34346 = cljs.core.not.call(null,inst_34345);
var state_34360__$1 = (function (){var statearr_34366 = state_34360;
(statearr_34366[(7)] = inst_34344);

(statearr_34366[(10)] = inst_34343__$1);

return statearr_34366;
})();
if(inst_34346){
var statearr_34367_34382 = state_34360__$1;
(statearr_34367_34382[(1)] = (4));

} else {
var statearr_34368_34383 = state_34360__$1;
(statearr_34368_34383[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34361 === (1))){
var inst_34338 = cljs.core.nth.call(null,files,(0),null);
var inst_34339 = cljs.core.nthnext.call(null,files,(1));
var inst_34340 = files;
var state_34360__$1 = (function (){var statearr_34369 = state_34360;
(statearr_34369[(11)] = inst_34338);

(statearr_34369[(12)] = inst_34339);

(statearr_34369[(8)] = inst_34340);

return statearr_34369;
})();
var statearr_34370_34384 = state_34360__$1;
(statearr_34370_34384[(2)] = null);

(statearr_34370_34384[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__25999__auto___34378,out))
;
return ((function (switch__25937__auto__,c__25999__auto___34378,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto____0 = (function (){
var statearr_34374 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34374[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto__);

(statearr_34374[(1)] = (1));

return statearr_34374;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto____1 = (function (state_34360){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_34360);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e34375){if((e34375 instanceof Object)){
var ex__25941__auto__ = e34375;
var statearr_34376_34385 = state_34360;
(statearr_34376_34385[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_34360);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34375;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34386 = state_34360;
state_34360 = G__34386;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto__ = function(state_34360){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto____1.call(this,state_34360);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___34378,out))
})();
var state__26001__auto__ = (function (){var statearr_34377 = f__26000__auto__.call(null);
(statearr_34377[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___34378);

return statearr_34377;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___34378,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.add_request_url = (function figwheel$client$file_reloading$add_request_url(p__34387,p__34388){
var map__34391 = p__34387;
var map__34391__$1 = ((cljs.core.seq_QMARK_.call(null,map__34391))?cljs.core.apply.call(null,cljs.core.hash_map,map__34391):map__34391);
var opts = map__34391__$1;
var url_rewriter = cljs.core.get.call(null,map__34391__$1,new cljs.core.Keyword(null,"url-rewriter","url-rewriter",200543838));
var map__34392 = p__34388;
var map__34392__$1 = ((cljs.core.seq_QMARK_.call(null,map__34392))?cljs.core.apply.call(null,cljs.core.hash_map,map__34392):map__34392);
var file_msg = map__34392__$1;
var file = cljs.core.get.call(null,map__34392__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var resolved_path = figwheel.client.file_reloading.resolve_url.call(null,file_msg);
return cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"request-url","request-url",2100346596),(cljs.core.truth_(url_rewriter)?url_rewriter.call(null,resolved_path):resolved_path));
});
figwheel.client.file_reloading.add_request_urls = (function figwheel$client$file_reloading$add_request_urls(opts,files){
return cljs.core.map.call(null,cljs.core.partial.call(null,figwheel.client.file_reloading.add_request_url,opts),files);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__34393){
var map__34396 = p__34393;
var map__34396__$1 = ((cljs.core.seq_QMARK_.call(null,map__34396))?cljs.core.apply.call(null,cljs.core.hash_map,map__34396):map__34396);
var file = cljs.core.get.call(null,map__34396__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var eval_body__$1 = cljs.core.get.call(null,map__34396__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
if(cljs.core.truth_((function (){var and__22763__auto__ = eval_body__$1;
if(cljs.core.truth_(and__22763__auto__)){
return typeof eval_body__$1 === 'string';
} else {
return and__22763__auto__;
}
})())){
var code = eval_body__$1;
try{figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Evaling file "),cljs.core.str(file)].join(''));

return eval(code);
}catch (e34397){var e = e34397;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Unable to evaluate "),cljs.core.str(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__34402,p__34403){
var map__34605 = p__34402;
var map__34605__$1 = ((cljs.core.seq_QMARK_.call(null,map__34605))?cljs.core.apply.call(null,cljs.core.hash_map,map__34605):map__34605);
var opts = map__34605__$1;
var load_unchanged_files = cljs.core.get.call(null,map__34605__$1,new cljs.core.Keyword(null,"load-unchanged-files","load-unchanged-files",-1561468704));
var on_jsload = cljs.core.get.call(null,map__34605__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var before_jsload = cljs.core.get.call(null,map__34605__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var map__34606 = p__34403;
var map__34606__$1 = ((cljs.core.seq_QMARK_.call(null,map__34606))?cljs.core.apply.call(null,cljs.core.hash_map,map__34606):map__34606);
var msg = map__34606__$1;
var files = cljs.core.get.call(null,map__34606__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (state_34731){
var state_val_34732 = (state_34731[(1)]);
if((state_val_34732 === (7))){
var inst_34621 = (state_34731[(7)]);
var inst_34619 = (state_34731[(8)]);
var inst_34620 = (state_34731[(9)]);
var inst_34618 = (state_34731[(10)]);
var inst_34626 = cljs.core._nth.call(null,inst_34619,inst_34621);
var inst_34627 = figwheel.client.file_reloading.eval_body.call(null,inst_34626);
var inst_34628 = (inst_34621 + (1));
var tmp34733 = inst_34619;
var tmp34734 = inst_34620;
var tmp34735 = inst_34618;
var inst_34618__$1 = tmp34735;
var inst_34619__$1 = tmp34733;
var inst_34620__$1 = tmp34734;
var inst_34621__$1 = inst_34628;
var state_34731__$1 = (function (){var statearr_34736 = state_34731;
(statearr_34736[(7)] = inst_34621__$1);

(statearr_34736[(8)] = inst_34619__$1);

(statearr_34736[(11)] = inst_34627);

(statearr_34736[(9)] = inst_34620__$1);

(statearr_34736[(10)] = inst_34618__$1);

return statearr_34736;
})();
var statearr_34737_34806 = state_34731__$1;
(statearr_34737_34806[(2)] = null);

(statearr_34737_34806[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (20))){
var inst_34668 = (state_34731[(12)]);
var inst_34663 = (state_34731[(13)]);
var inst_34664 = (state_34731[(14)]);
var inst_34667 = (state_34731[(15)]);
var inst_34670 = (state_34731[(16)]);
var inst_34673 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_34675 = (function (){var files_not_loaded = inst_34670;
var res = inst_34668;
var res_SINGLEQUOTE_ = inst_34667;
var files_SINGLEQUOTE_ = inst_34664;
var all_files = inst_34663;
return ((function (files_not_loaded,res,res_SINGLEQUOTE_,files_SINGLEQUOTE_,all_files,inst_34668,inst_34663,inst_34664,inst_34667,inst_34670,inst_34673,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (p__34674){
var map__34738 = p__34674;
var map__34738__$1 = ((cljs.core.seq_QMARK_.call(null,map__34738))?cljs.core.apply.call(null,cljs.core.hash_map,map__34738):map__34738);
var file = cljs.core.get.call(null,map__34738__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var namespace = cljs.core.get.call(null,map__34738__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.ns_to_js_file.call(null,namespace);
} else {
return file;
}
});
;})(files_not_loaded,res,res_SINGLEQUOTE_,files_SINGLEQUOTE_,all_files,inst_34668,inst_34663,inst_34664,inst_34667,inst_34670,inst_34673,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
})();
var inst_34676 = cljs.core.map.call(null,inst_34675,inst_34668);
var inst_34677 = cljs.core.pr_str.call(null,inst_34676);
var inst_34678 = figwheel.client.utils.log.call(null,inst_34677);
var inst_34679 = (function (){var files_not_loaded = inst_34670;
var res = inst_34668;
var res_SINGLEQUOTE_ = inst_34667;
var files_SINGLEQUOTE_ = inst_34664;
var all_files = inst_34663;
return ((function (files_not_loaded,res,res_SINGLEQUOTE_,files_SINGLEQUOTE_,all_files,inst_34668,inst_34663,inst_34664,inst_34667,inst_34670,inst_34673,inst_34675,inst_34676,inst_34677,inst_34678,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(files_not_loaded,res,res_SINGLEQUOTE_,files_SINGLEQUOTE_,all_files,inst_34668,inst_34663,inst_34664,inst_34667,inst_34670,inst_34673,inst_34675,inst_34676,inst_34677,inst_34678,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
})();
var inst_34680 = setTimeout(inst_34679,(10));
var state_34731__$1 = (function (){var statearr_34739 = state_34731;
(statearr_34739[(17)] = inst_34678);

(statearr_34739[(18)] = inst_34673);

return statearr_34739;
})();
var statearr_34740_34807 = state_34731__$1;
(statearr_34740_34807[(2)] = inst_34680);

(statearr_34740_34807[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (27))){
var inst_34690 = (state_34731[(19)]);
var state_34731__$1 = state_34731;
var statearr_34741_34808 = state_34731__$1;
(statearr_34741_34808[(2)] = inst_34690);

(statearr_34741_34808[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (1))){
var inst_34610 = (state_34731[(20)]);
var inst_34607 = before_jsload.call(null,files);
var inst_34608 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_34609 = (function (){return ((function (inst_34610,inst_34607,inst_34608,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (p1__34398_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__34398_SHARP_);
});
;})(inst_34610,inst_34607,inst_34608,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
})();
var inst_34610__$1 = cljs.core.filter.call(null,inst_34609,files);
var inst_34611 = cljs.core.not_empty.call(null,inst_34610__$1);
var state_34731__$1 = (function (){var statearr_34742 = state_34731;
(statearr_34742[(21)] = inst_34607);

(statearr_34742[(22)] = inst_34608);

(statearr_34742[(20)] = inst_34610__$1);

return statearr_34742;
})();
if(cljs.core.truth_(inst_34611)){
var statearr_34743_34809 = state_34731__$1;
(statearr_34743_34809[(1)] = (2));

} else {
var statearr_34744_34810 = state_34731__$1;
(statearr_34744_34810[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (24))){
var state_34731__$1 = state_34731;
var statearr_34745_34811 = state_34731__$1;
(statearr_34745_34811[(2)] = null);

(statearr_34745_34811[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (4))){
var inst_34655 = (state_34731[(2)]);
var inst_34656 = (function (){return ((function (inst_34655,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (p1__34399_SHARP_){
var and__22763__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__34399_SHARP_);
if(cljs.core.truth_(and__22763__auto__)){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__34399_SHARP_));
} else {
return and__22763__auto__;
}
});
;})(inst_34655,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
})();
var inst_34657 = cljs.core.filter.call(null,inst_34656,files);
var state_34731__$1 = (function (){var statearr_34746 = state_34731;
(statearr_34746[(23)] = inst_34657);

(statearr_34746[(24)] = inst_34655);

return statearr_34746;
})();
if(cljs.core.truth_(load_unchanged_files)){
var statearr_34747_34812 = state_34731__$1;
(statearr_34747_34812[(1)] = (16));

} else {
var statearr_34748_34813 = state_34731__$1;
(statearr_34748_34813[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (15))){
var inst_34645 = (state_34731[(2)]);
var state_34731__$1 = state_34731;
var statearr_34749_34814 = state_34731__$1;
(statearr_34749_34814[(2)] = inst_34645);

(statearr_34749_34814[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (21))){
var state_34731__$1 = state_34731;
var statearr_34750_34815 = state_34731__$1;
(statearr_34750_34815[(2)] = null);

(statearr_34750_34815[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (31))){
var inst_34698 = (state_34731[(25)]);
var inst_34708 = (state_34731[(2)]);
var inst_34709 = cljs.core.not_empty.call(null,inst_34698);
var state_34731__$1 = (function (){var statearr_34751 = state_34731;
(statearr_34751[(26)] = inst_34708);

return statearr_34751;
})();
if(cljs.core.truth_(inst_34709)){
var statearr_34752_34816 = state_34731__$1;
(statearr_34752_34816[(1)] = (32));

} else {
var statearr_34753_34817 = state_34731__$1;
(statearr_34753_34817[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (32))){
var inst_34698 = (state_34731[(25)]);
var inst_34711 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_34698);
var inst_34712 = cljs.core.pr_str.call(null,inst_34711);
var inst_34713 = [cljs.core.str("file didn't change: "),cljs.core.str(inst_34712)].join('');
var inst_34714 = figwheel.client.utils.log.call(null,inst_34713);
var state_34731__$1 = state_34731;
var statearr_34754_34818 = state_34731__$1;
(statearr_34754_34818[(2)] = inst_34714);

(statearr_34754_34818[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (33))){
var state_34731__$1 = state_34731;
var statearr_34755_34819 = state_34731__$1;
(statearr_34755_34819[(2)] = null);

(statearr_34755_34819[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (13))){
var inst_34631 = (state_34731[(27)]);
var inst_34635 = cljs.core.chunk_first.call(null,inst_34631);
var inst_34636 = cljs.core.chunk_rest.call(null,inst_34631);
var inst_34637 = cljs.core.count.call(null,inst_34635);
var inst_34618 = inst_34636;
var inst_34619 = inst_34635;
var inst_34620 = inst_34637;
var inst_34621 = (0);
var state_34731__$1 = (function (){var statearr_34756 = state_34731;
(statearr_34756[(7)] = inst_34621);

(statearr_34756[(8)] = inst_34619);

(statearr_34756[(9)] = inst_34620);

(statearr_34756[(10)] = inst_34618);

return statearr_34756;
})();
var statearr_34757_34820 = state_34731__$1;
(statearr_34757_34820[(2)] = null);

(statearr_34757_34820[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (22))){
var inst_34670 = (state_34731[(16)]);
var inst_34683 = (state_34731[(2)]);
var inst_34684 = cljs.core.not_empty.call(null,inst_34670);
var state_34731__$1 = (function (){var statearr_34758 = state_34731;
(statearr_34758[(28)] = inst_34683);

return statearr_34758;
})();
if(cljs.core.truth_(inst_34684)){
var statearr_34759_34821 = state_34731__$1;
(statearr_34759_34821[(1)] = (23));

} else {
var statearr_34760_34822 = state_34731__$1;
(statearr_34760_34822[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (36))){
var state_34731__$1 = state_34731;
var statearr_34761_34823 = state_34731__$1;
(statearr_34761_34823[(2)] = null);

(statearr_34761_34823[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (29))){
var inst_34699 = (state_34731[(29)]);
var inst_34702 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_34699);
var inst_34703 = cljs.core.pr_str.call(null,inst_34702);
var inst_34704 = [cljs.core.str("figwheel-no-load meta-data: "),cljs.core.str(inst_34703)].join('');
var inst_34705 = figwheel.client.utils.log.call(null,inst_34704);
var state_34731__$1 = state_34731;
var statearr_34762_34824 = state_34731__$1;
(statearr_34762_34824[(2)] = inst_34705);

(statearr_34762_34824[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (6))){
var inst_34652 = (state_34731[(2)]);
var state_34731__$1 = state_34731;
var statearr_34763_34825 = state_34731__$1;
(statearr_34763_34825[(2)] = inst_34652);

(statearr_34763_34825[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (28))){
var inst_34699 = (state_34731[(29)]);
var inst_34696 = (state_34731[(2)]);
var inst_34697 = cljs.core.get.call(null,inst_34696,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_34698 = cljs.core.get.call(null,inst_34696,new cljs.core.Keyword(null,"file-changed-on-disk","file-changed-on-disk",1086171932));
var inst_34699__$1 = cljs.core.get.call(null,inst_34696,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_34700 = cljs.core.not_empty.call(null,inst_34699__$1);
var state_34731__$1 = (function (){var statearr_34764 = state_34731;
(statearr_34764[(25)] = inst_34698);

(statearr_34764[(29)] = inst_34699__$1);

(statearr_34764[(30)] = inst_34697);

return statearr_34764;
})();
if(cljs.core.truth_(inst_34700)){
var statearr_34765_34826 = state_34731__$1;
(statearr_34765_34826[(1)] = (29));

} else {
var statearr_34766_34827 = state_34731__$1;
(statearr_34766_34827[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (25))){
var inst_34729 = (state_34731[(2)]);
var state_34731__$1 = state_34731;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_34731__$1,inst_34729);
} else {
if((state_val_34732 === (34))){
var inst_34697 = (state_34731[(30)]);
var inst_34717 = (state_34731[(2)]);
var inst_34718 = cljs.core.not_empty.call(null,inst_34697);
var state_34731__$1 = (function (){var statearr_34767 = state_34731;
(statearr_34767[(31)] = inst_34717);

return statearr_34767;
})();
if(cljs.core.truth_(inst_34718)){
var statearr_34768_34828 = state_34731__$1;
(statearr_34768_34828[(1)] = (35));

} else {
var statearr_34769_34829 = state_34731__$1;
(statearr_34769_34829[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (17))){
var inst_34657 = (state_34731[(23)]);
var state_34731__$1 = state_34731;
var statearr_34770_34830 = state_34731__$1;
(statearr_34770_34830[(2)] = inst_34657);

(statearr_34770_34830[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (3))){
var state_34731__$1 = state_34731;
var statearr_34771_34831 = state_34731__$1;
(statearr_34771_34831[(2)] = null);

(statearr_34771_34831[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (12))){
var inst_34648 = (state_34731[(2)]);
var state_34731__$1 = state_34731;
var statearr_34772_34832 = state_34731__$1;
(statearr_34772_34832[(2)] = inst_34648);

(statearr_34772_34832[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (2))){
var inst_34610 = (state_34731[(20)]);
var inst_34617 = cljs.core.seq.call(null,inst_34610);
var inst_34618 = inst_34617;
var inst_34619 = null;
var inst_34620 = (0);
var inst_34621 = (0);
var state_34731__$1 = (function (){var statearr_34773 = state_34731;
(statearr_34773[(7)] = inst_34621);

(statearr_34773[(8)] = inst_34619);

(statearr_34773[(9)] = inst_34620);

(statearr_34773[(10)] = inst_34618);

return statearr_34773;
})();
var statearr_34774_34833 = state_34731__$1;
(statearr_34774_34833[(2)] = null);

(statearr_34774_34833[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (23))){
var inst_34668 = (state_34731[(12)]);
var inst_34690 = (state_34731[(19)]);
var inst_34663 = (state_34731[(13)]);
var inst_34664 = (state_34731[(14)]);
var inst_34667 = (state_34731[(15)]);
var inst_34670 = (state_34731[(16)]);
var inst_34686 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_34689 = (function (){var files_not_loaded = inst_34670;
var res = inst_34668;
var res_SINGLEQUOTE_ = inst_34667;
var files_SINGLEQUOTE_ = inst_34664;
var all_files = inst_34663;
return ((function (files_not_loaded,res,res_SINGLEQUOTE_,files_SINGLEQUOTE_,all_files,inst_34668,inst_34690,inst_34663,inst_34664,inst_34667,inst_34670,inst_34686,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (p__34688){
var map__34775 = p__34688;
var map__34775__$1 = ((cljs.core.seq_QMARK_.call(null,map__34775))?cljs.core.apply.call(null,cljs.core.hash_map,map__34775):map__34775);
var meta_data = cljs.core.get.call(null,map__34775__$1,new cljs.core.Keyword(null,"meta-data","meta-data",-1613399157));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.contains_QMARK_.call(null,meta_data,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
if((cljs.core.contains_QMARK_.call(null,meta_data,new cljs.core.Keyword(null,"file-changed-on-disk","file-changed-on-disk",1086171932))) && (cljs.core.not.call(null,new cljs.core.Keyword(null,"file-changed-on-disk","file-changed-on-disk",1086171932).cljs$core$IFn$_invoke$arity$1(meta_data)))){
return new cljs.core.Keyword(null,"file-changed-on-disk","file-changed-on-disk",1086171932);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
}
});
;})(files_not_loaded,res,res_SINGLEQUOTE_,files_SINGLEQUOTE_,all_files,inst_34668,inst_34690,inst_34663,inst_34664,inst_34667,inst_34670,inst_34686,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
})();
var inst_34690__$1 = cljs.core.group_by.call(null,inst_34689,inst_34670);
var inst_34691 = cljs.core.seq_QMARK_.call(null,inst_34690__$1);
var state_34731__$1 = (function (){var statearr_34776 = state_34731;
(statearr_34776[(19)] = inst_34690__$1);

(statearr_34776[(32)] = inst_34686);

return statearr_34776;
})();
if(inst_34691){
var statearr_34777_34834 = state_34731__$1;
(statearr_34777_34834[(1)] = (26));

} else {
var statearr_34778_34835 = state_34731__$1;
(statearr_34778_34835[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (35))){
var inst_34697 = (state_34731[(30)]);
var inst_34720 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_34697);
var inst_34721 = cljs.core.pr_str.call(null,inst_34720);
var inst_34722 = [cljs.core.str("not required: "),cljs.core.str(inst_34721)].join('');
var inst_34723 = figwheel.client.utils.log.call(null,inst_34722);
var state_34731__$1 = state_34731;
var statearr_34779_34836 = state_34731__$1;
(statearr_34779_34836[(2)] = inst_34723);

(statearr_34779_34836[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (19))){
var inst_34668 = (state_34731[(12)]);
var inst_34663 = (state_34731[(13)]);
var inst_34664 = (state_34731[(14)]);
var inst_34667 = (state_34731[(15)]);
var inst_34667__$1 = (state_34731[(2)]);
var inst_34668__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_34667__$1);
var inst_34669 = (function (){var res = inst_34668__$1;
var res_SINGLEQUOTE_ = inst_34667__$1;
var files_SINGLEQUOTE_ = inst_34664;
var all_files = inst_34663;
return ((function (res,res_SINGLEQUOTE_,files_SINGLEQUOTE_,all_files,inst_34668,inst_34663,inst_34664,inst_34667,inst_34667__$1,inst_34668__$1,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (p1__34401_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__34401_SHARP_));
});
;})(res,res_SINGLEQUOTE_,files_SINGLEQUOTE_,all_files,inst_34668,inst_34663,inst_34664,inst_34667,inst_34667__$1,inst_34668__$1,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
})();
var inst_34670 = cljs.core.filter.call(null,inst_34669,inst_34667__$1);
var inst_34671 = cljs.core.not_empty.call(null,inst_34668__$1);
var state_34731__$1 = (function (){var statearr_34780 = state_34731;
(statearr_34780[(12)] = inst_34668__$1);

(statearr_34780[(15)] = inst_34667__$1);

(statearr_34780[(16)] = inst_34670);

return statearr_34780;
})();
if(cljs.core.truth_(inst_34671)){
var statearr_34781_34837 = state_34731__$1;
(statearr_34781_34837[(1)] = (20));

} else {
var statearr_34782_34838 = state_34731__$1;
(statearr_34782_34838[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (11))){
var state_34731__$1 = state_34731;
var statearr_34783_34839 = state_34731__$1;
(statearr_34783_34839[(2)] = null);

(statearr_34783_34839[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (9))){
var inst_34650 = (state_34731[(2)]);
var state_34731__$1 = state_34731;
var statearr_34784_34840 = state_34731__$1;
(statearr_34784_34840[(2)] = inst_34650);

(statearr_34784_34840[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (5))){
var inst_34621 = (state_34731[(7)]);
var inst_34620 = (state_34731[(9)]);
var inst_34623 = (inst_34621 < inst_34620);
var inst_34624 = inst_34623;
var state_34731__$1 = state_34731;
if(cljs.core.truth_(inst_34624)){
var statearr_34785_34841 = state_34731__$1;
(statearr_34785_34841[(1)] = (7));

} else {
var statearr_34786_34842 = state_34731__$1;
(statearr_34786_34842[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (14))){
var inst_34631 = (state_34731[(27)]);
var inst_34640 = cljs.core.first.call(null,inst_34631);
var inst_34641 = figwheel.client.file_reloading.eval_body.call(null,inst_34640);
var inst_34642 = cljs.core.next.call(null,inst_34631);
var inst_34618 = inst_34642;
var inst_34619 = null;
var inst_34620 = (0);
var inst_34621 = (0);
var state_34731__$1 = (function (){var statearr_34787 = state_34731;
(statearr_34787[(7)] = inst_34621);

(statearr_34787[(8)] = inst_34619);

(statearr_34787[(33)] = inst_34641);

(statearr_34787[(9)] = inst_34620);

(statearr_34787[(10)] = inst_34618);

return statearr_34787;
})();
var statearr_34788_34843 = state_34731__$1;
(statearr_34788_34843[(2)] = null);

(statearr_34788_34843[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (26))){
var inst_34690 = (state_34731[(19)]);
var inst_34693 = cljs.core.apply.call(null,cljs.core.hash_map,inst_34690);
var state_34731__$1 = state_34731;
var statearr_34789_34844 = state_34731__$1;
(statearr_34789_34844[(2)] = inst_34693);

(statearr_34789_34844[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (16))){
var inst_34657 = (state_34731[(23)]);
var inst_34659 = (function (){var all_files = inst_34657;
return ((function (all_files,inst_34657,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function (p1__34400_SHARP_){
return cljs.core.update_in.call(null,p1__34400_SHARP_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"meta-data","meta-data",-1613399157)], null),cljs.core.dissoc,new cljs.core.Keyword(null,"file-changed-on-disk","file-changed-on-disk",1086171932));
});
;})(all_files,inst_34657,state_val_34732,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
})();
var inst_34660 = cljs.core.map.call(null,inst_34659,inst_34657);
var state_34731__$1 = state_34731;
var statearr_34790_34845 = state_34731__$1;
(statearr_34790_34845[(2)] = inst_34660);

(statearr_34790_34845[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (30))){
var state_34731__$1 = state_34731;
var statearr_34791_34846 = state_34731__$1;
(statearr_34791_34846[(2)] = null);

(statearr_34791_34846[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (10))){
var inst_34631 = (state_34731[(27)]);
var inst_34633 = cljs.core.chunked_seq_QMARK_.call(null,inst_34631);
var state_34731__$1 = state_34731;
if(inst_34633){
var statearr_34792_34847 = state_34731__$1;
(statearr_34792_34847[(1)] = (13));

} else {
var statearr_34793_34848 = state_34731__$1;
(statearr_34793_34848[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (18))){
var inst_34663 = (state_34731[(13)]);
var inst_34664 = (state_34731[(14)]);
var inst_34663__$1 = (state_34731[(2)]);
var inst_34664__$1 = figwheel.client.file_reloading.add_request_urls.call(null,opts,inst_34663__$1);
var inst_34665 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_34664__$1);
var state_34731__$1 = (function (){var statearr_34794 = state_34731;
(statearr_34794[(13)] = inst_34663__$1);

(statearr_34794[(14)] = inst_34664__$1);

return statearr_34794;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34731__$1,(19),inst_34665);
} else {
if((state_val_34732 === (37))){
var inst_34726 = (state_34731[(2)]);
var state_34731__$1 = state_34731;
var statearr_34795_34849 = state_34731__$1;
(statearr_34795_34849[(2)] = inst_34726);

(statearr_34795_34849[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34732 === (8))){
var inst_34631 = (state_34731[(27)]);
var inst_34618 = (state_34731[(10)]);
var inst_34631__$1 = cljs.core.seq.call(null,inst_34618);
var state_34731__$1 = (function (){var statearr_34796 = state_34731;
(statearr_34796[(27)] = inst_34631__$1);

return statearr_34796;
})();
if(inst_34631__$1){
var statearr_34797_34850 = state_34731__$1;
(statearr_34797_34850[(1)] = (10));

} else {
var statearr_34798_34851 = state_34731__$1;
(statearr_34798_34851[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
;
return ((function (switch__25937__auto__,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto____0 = (function (){
var statearr_34802 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34802[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto__);

(statearr_34802[(1)] = (1));

return statearr_34802;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto____1 = (function (state_34731){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_34731);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e34803){if((e34803 instanceof Object)){
var ex__25941__auto__ = e34803;
var statearr_34804_34852 = state_34731;
(statearr_34804_34852[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_34731);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34803;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34853 = state_34731;
state_34731 = G__34853;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto__ = function(state_34731){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto____1.call(this,state_34731);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
})();
var state__26001__auto__ = (function (){var statearr_34805 = f__26000__auto__.call(null);
(statearr_34805[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_34805;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__,map__34605,map__34605__$1,opts,load_unchanged_files,on_jsload,before_jsload,map__34606,map__34606__$1,msg,files))
);

return c__25999__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str(location.protocol),cljs.core.str("//")].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__34856,link){
var map__34858 = p__34856;
var map__34858__$1 = ((cljs.core.seq_QMARK_.call(null,map__34858))?cljs.core.apply.call(null,cljs.core.hash_map,map__34858):map__34858);
var file = cljs.core.get.call(null,map__34858__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4126__auto__ = link.href;
if(cljs.core.truth_(temp__4126__auto__)){
var link_href = temp__4126__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__4126__auto__,map__34858,map__34858__$1,file){
return (function (p1__34854_SHARP_,p2__34855_SHARP_){
if(cljs.core._EQ_.call(null,p1__34854_SHARP_,p2__34855_SHARP_)){
return p1__34854_SHARP_;
} else {
return false;
}
});})(link_href,temp__4126__auto__,map__34858,map__34858__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__4126__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__34862){
var map__34863 = p__34862;
var map__34863__$1 = ((cljs.core.seq_QMARK_.call(null,map__34863))?cljs.core.apply.call(null,cljs.core.hash_map,map__34863):map__34863);
var current_url_length = cljs.core.get.call(null,map__34863__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
var match_length = cljs.core.get.call(null,map__34863__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__34859_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__34859_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__4126__auto__)){
var res = temp__4126__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.add_link_to_doc = (function figwheel$client$file_reloading$add_link_to_doc(){
var G__34865 = arguments.length;
switch (G__34865) {
case 1:
return figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$1 = (function (new_link){
return (document.getElementsByTagName("head")[(0)]).appendChild(new_link);
});

figwheel.client.file_reloading.add_link_to_doc.cljs$core$IFn$_invoke$arity$2 = (function (orig_link,klone){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
return parent.removeChild(orig_link);
});})(parent))
,(300));
});

figwheel.client.file_reloading.add_link_to_doc.cljs$lang$maxFixedArity = 2;
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(p__34867){
var map__34869 = p__34867;
var map__34869__$1 = ((cljs.core.seq_QMARK_.call(null,map__34869))?cljs.core.apply.call(null,cljs.core.hash_map,map__34869):map__34869);
var f_data = map__34869__$1;
var request_url = cljs.core.get.call(null,map__34869__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var file = cljs.core.get.call(null,map__34869__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4124__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__4124__auto__)){
var link = temp__4124__auto__;
return figwheel.client.file_reloading.add_link_to_doc.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href));
} else {
return figwheel.client.file_reloading.add_link_to_doc.call(null,figwheel.client.file_reloading.create_link.call(null,(function (){var or__22775__auto__ = request_url;
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return file;
}
})()));
}
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__34870,files_msg){
var map__34892 = p__34870;
var map__34892__$1 = ((cljs.core.seq_QMARK_.call(null,map__34892))?cljs.core.apply.call(null,cljs.core.hash_map,map__34892):map__34892);
var opts = map__34892__$1;
var on_cssload = cljs.core.get.call(null,map__34892__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
var seq__34893_34913 = cljs.core.seq.call(null,figwheel.client.file_reloading.add_request_urls.call(null,opts,new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(files_msg)));
var chunk__34894_34914 = null;
var count__34895_34915 = (0);
var i__34896_34916 = (0);
while(true){
if((i__34896_34916 < count__34895_34915)){
var f_34917 = cljs.core._nth.call(null,chunk__34894_34914,i__34896_34916);
figwheel.client.file_reloading.reload_css_file.call(null,f_34917);

var G__34918 = seq__34893_34913;
var G__34919 = chunk__34894_34914;
var G__34920 = count__34895_34915;
var G__34921 = (i__34896_34916 + (1));
seq__34893_34913 = G__34918;
chunk__34894_34914 = G__34919;
count__34895_34915 = G__34920;
i__34896_34916 = G__34921;
continue;
} else {
var temp__4126__auto___34922 = cljs.core.seq.call(null,seq__34893_34913);
if(temp__4126__auto___34922){
var seq__34893_34923__$1 = temp__4126__auto___34922;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__34893_34923__$1)){
var c__23560__auto___34924 = cljs.core.chunk_first.call(null,seq__34893_34923__$1);
var G__34925 = cljs.core.chunk_rest.call(null,seq__34893_34923__$1);
var G__34926 = c__23560__auto___34924;
var G__34927 = cljs.core.count.call(null,c__23560__auto___34924);
var G__34928 = (0);
seq__34893_34913 = G__34925;
chunk__34894_34914 = G__34926;
count__34895_34915 = G__34927;
i__34896_34916 = G__34928;
continue;
} else {
var f_34929 = cljs.core.first.call(null,seq__34893_34923__$1);
figwheel.client.file_reloading.reload_css_file.call(null,f_34929);

var G__34930 = cljs.core.next.call(null,seq__34893_34923__$1);
var G__34931 = null;
var G__34932 = (0);
var G__34933 = (0);
seq__34893_34913 = G__34930;
chunk__34894_34914 = G__34931;
count__34895_34915 = G__34932;
i__34896_34916 = G__34933;
continue;
}
} else {
}
}
break;
}

var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__,map__34892,map__34892__$1,opts,on_cssload){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__,map__34892,map__34892__$1,opts,on_cssload){
return (function (state_34903){
var state_val_34904 = (state_34903[(1)]);
if((state_val_34904 === (2))){
var inst_34899 = (state_34903[(2)]);
var inst_34900 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(files_msg);
var inst_34901 = on_cssload.call(null,inst_34900);
var state_34903__$1 = (function (){var statearr_34905 = state_34903;
(statearr_34905[(7)] = inst_34899);

return statearr_34905;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_34903__$1,inst_34901);
} else {
if((state_val_34904 === (1))){
var inst_34897 = cljs.core.async.timeout.call(null,(100));
var state_34903__$1 = state_34903;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34903__$1,(2),inst_34897);
} else {
return null;
}
}
});})(c__25999__auto__,map__34892,map__34892__$1,opts,on_cssload))
;
return ((function (switch__25937__auto__,c__25999__auto__,map__34892,map__34892__$1,opts,on_cssload){
return (function() {
var figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto__ = null;
var figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto____0 = (function (){
var statearr_34909 = [null,null,null,null,null,null,null,null];
(statearr_34909[(0)] = figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto__);

(statearr_34909[(1)] = (1));

return statearr_34909;
});
var figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto____1 = (function (state_34903){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_34903);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e34910){if((e34910 instanceof Object)){
var ex__25941__auto__ = e34910;
var statearr_34911_34934 = state_34903;
(statearr_34911_34934[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_34903);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34910;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34935 = state_34903;
state_34903 = G__34935;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto__ = function(state_34903){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto____1.call(this,state_34903);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto____0;
figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto____1;
return figwheel$client$file_reloading$reload_css_files_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__,map__34892,map__34892__$1,opts,on_cssload))
})();
var state__26001__auto__ = (function (){var statearr_34912 = f__26000__auto__.call(null);
(statearr_34912[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_34912;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__,map__34892,map__34892__$1,opts,on_cssload))
);

return c__25999__auto__;
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map?rel=1446838352160