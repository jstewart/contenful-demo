// Compiled by ClojureScript 0.0-3211 {}
goog.provide('figwheel.client.heads_up');
goog.require('cljs.core');
goog.require('cljs.core.async');
goog.require('figwheel.client.socket');
goog.require('clojure.string');

figwheel.client.heads_up.node = (function figwheel$client$heads_up$node(){
var argseq__23815__auto__ = ((((2) < arguments.length))?(new cljs.core.IndexedSeq(Array.prototype.slice.call(arguments,(2)),(0))):null);
return figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__23815__auto__);
});

figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic = (function (t,attrs,children){
var e = document.createElement(cljs.core.name.call(null,t));
var seq__33983_33991 = cljs.core.seq.call(null,cljs.core.keys.call(null,attrs));
var chunk__33984_33992 = null;
var count__33985_33993 = (0);
var i__33986_33994 = (0);
while(true){
if((i__33986_33994 < count__33985_33993)){
var k_33995 = cljs.core._nth.call(null,chunk__33984_33992,i__33986_33994);
e.setAttribute(cljs.core.name.call(null,k_33995),cljs.core.get.call(null,attrs,k_33995));

var G__33996 = seq__33983_33991;
var G__33997 = chunk__33984_33992;
var G__33998 = count__33985_33993;
var G__33999 = (i__33986_33994 + (1));
seq__33983_33991 = G__33996;
chunk__33984_33992 = G__33997;
count__33985_33993 = G__33998;
i__33986_33994 = G__33999;
continue;
} else {
var temp__4126__auto___34000 = cljs.core.seq.call(null,seq__33983_33991);
if(temp__4126__auto___34000){
var seq__33983_34001__$1 = temp__4126__auto___34000;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__33983_34001__$1)){
var c__23560__auto___34002 = cljs.core.chunk_first.call(null,seq__33983_34001__$1);
var G__34003 = cljs.core.chunk_rest.call(null,seq__33983_34001__$1);
var G__34004 = c__23560__auto___34002;
var G__34005 = cljs.core.count.call(null,c__23560__auto___34002);
var G__34006 = (0);
seq__33983_33991 = G__34003;
chunk__33984_33992 = G__34004;
count__33985_33993 = G__34005;
i__33986_33994 = G__34006;
continue;
} else {
var k_34007 = cljs.core.first.call(null,seq__33983_34001__$1);
e.setAttribute(cljs.core.name.call(null,k_34007),cljs.core.get.call(null,attrs,k_34007));

var G__34008 = cljs.core.next.call(null,seq__33983_34001__$1);
var G__34009 = null;
var G__34010 = (0);
var G__34011 = (0);
seq__33983_33991 = G__34008;
chunk__33984_33992 = G__34009;
count__33985_33993 = G__34010;
i__33986_33994 = G__34011;
continue;
}
} else {
}
}
break;
}

var seq__33987_34012 = cljs.core.seq.call(null,children);
var chunk__33988_34013 = null;
var count__33989_34014 = (0);
var i__33990_34015 = (0);
while(true){
if((i__33990_34015 < count__33989_34014)){
var ch_34016 = cljs.core._nth.call(null,chunk__33988_34013,i__33990_34015);
e.appendChild(ch_34016);

var G__34017 = seq__33987_34012;
var G__34018 = chunk__33988_34013;
var G__34019 = count__33989_34014;
var G__34020 = (i__33990_34015 + (1));
seq__33987_34012 = G__34017;
chunk__33988_34013 = G__34018;
count__33989_34014 = G__34019;
i__33990_34015 = G__34020;
continue;
} else {
var temp__4126__auto___34021 = cljs.core.seq.call(null,seq__33987_34012);
if(temp__4126__auto___34021){
var seq__33987_34022__$1 = temp__4126__auto___34021;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__33987_34022__$1)){
var c__23560__auto___34023 = cljs.core.chunk_first.call(null,seq__33987_34022__$1);
var G__34024 = cljs.core.chunk_rest.call(null,seq__33987_34022__$1);
var G__34025 = c__23560__auto___34023;
var G__34026 = cljs.core.count.call(null,c__23560__auto___34023);
var G__34027 = (0);
seq__33987_34012 = G__34024;
chunk__33988_34013 = G__34025;
count__33989_34014 = G__34026;
i__33990_34015 = G__34027;
continue;
} else {
var ch_34028 = cljs.core.first.call(null,seq__33987_34022__$1);
e.appendChild(ch_34028);

var G__34029 = cljs.core.next.call(null,seq__33987_34022__$1);
var G__34030 = null;
var G__34031 = (0);
var G__34032 = (0);
seq__33987_34012 = G__34029;
chunk__33988_34013 = G__34030;
count__33989_34014 = G__34031;
i__33990_34015 = G__34032;
continue;
}
} else {
}
}
break;
}

return e;
});

figwheel.client.heads_up.node.cljs$lang$maxFixedArity = (2);

figwheel.client.heads_up.node.cljs$lang$applyTo = (function (seq33980){
var G__33981 = cljs.core.first.call(null,seq33980);
var seq33980__$1 = cljs.core.next.call(null,seq33980);
var G__33982 = cljs.core.first.call(null,seq33980__$1);
var seq33980__$2 = cljs.core.next.call(null,seq33980__$1);
return figwheel.client.heads_up.node.cljs$core$IFn$_invoke$arity$variadic(G__33981,G__33982,seq33980__$2);
});
if(typeof figwheel.client.heads_up.heads_up_event_dispatch !== 'undefined'){
} else {
figwheel.client.heads_up.heads_up_event_dispatch = (function (){var method_table__23670__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__23671__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var method_cache__23672__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__23673__auto__ = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__23674__auto__ = cljs.core.get.call(null,cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),cljs.core.get_global_hierarchy.call(null));
return (new cljs.core.MultiFn(cljs.core.symbol.call(null,"figwheel.client.heads-up","heads-up-event-dispatch"),((function (method_table__23670__auto__,prefer_table__23671__auto__,method_cache__23672__auto__,cached_hierarchy__23673__auto__,hierarchy__23674__auto__){
return (function (dataset){
return dataset.figwheelEvent;
});})(method_table__23670__auto__,prefer_table__23671__auto__,method_cache__23672__auto__,cached_hierarchy__23673__auto__,hierarchy__23674__auto__))
,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__23674__auto__,method_table__23670__auto__,prefer_table__23671__auto__,method_cache__23672__auto__,cached_hierarchy__23673__auto__));
})();
}
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,new cljs.core.Keyword(null,"default","default",-1987822328),(function (_){
return cljs.core.PersistentArrayMap.EMPTY;
}));
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,"file-selected",(function (dataset){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"file-selected",new cljs.core.Keyword(null,"file-name","file-name",-1654217259),dataset.fileName,new cljs.core.Keyword(null,"file-line","file-line",-1228823138),dataset.fileLine], null));
}));
cljs.core._add_method.call(null,figwheel.client.heads_up.heads_up_event_dispatch,"close-heads-up",(function (dataset){
return figwheel.client.heads_up.clear.call(null);
}));
figwheel.client.heads_up.ancestor_nodes = (function figwheel$client$heads_up$ancestor_nodes(el){
return cljs.core.iterate.call(null,(function (e){
return e.parentNode;
}),el);
});
figwheel.client.heads_up.get_dataset = (function figwheel$client$heads_up$get_dataset(el){
return cljs.core.first.call(null,cljs.core.keep.call(null,(function (x){
if(cljs.core.truth_(x.dataset.figwheelEvent)){
return x.dataset;
} else {
return null;
}
}),cljs.core.take.call(null,(4),figwheel.client.heads_up.ancestor_nodes.call(null,el))));
});
figwheel.client.heads_up.heads_up_onclick_handler = (function figwheel$client$heads_up$heads_up_onclick_handler(event){
var dataset = figwheel.client.heads_up.get_dataset.call(null,event.target);
event.preventDefault();

if(cljs.core.truth_(dataset)){
return figwheel.client.heads_up.heads_up_event_dispatch.call(null,dataset);
} else {
return null;
}
});
figwheel.client.heads_up.ensure_container = (function figwheel$client$heads_up$ensure_container(){
var cont_id = "figwheel-heads-up-container";
var content_id = "figwheel-heads-up-content-area";
if(cljs.core.not.call(null,document.querySelector([cljs.core.str("#"),cljs.core.str(cont_id)].join('')))){
var el_34033 = figwheel.client.heads_up.node.call(null,new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"id","id",-1388402092),cont_id,new cljs.core.Keyword(null,"style","style",-496642736),[cljs.core.str("-webkit-transition: all 0.2s ease-in-out;"),cljs.core.str("-moz-transition: all 0.2s ease-in-out;"),cljs.core.str("-o-transition: all 0.2s ease-in-out;"),cljs.core.str("transition: all 0.2s ease-in-out;"),cljs.core.str("font-size: 13px;"),cljs.core.str("border-top: 1px solid #f5f5f5;"),cljs.core.str("box-shadow: 0px 0px 1px #aaaaaa;"),cljs.core.str("line-height: 18px;"),cljs.core.str("color: #333;"),cljs.core.str("font-family: monospace;"),cljs.core.str("padding: 0px 10px 0px 70px;"),cljs.core.str("position: fixed;"),cljs.core.str("bottom: 0px;"),cljs.core.str("left: 0px;"),cljs.core.str("height: 0px;"),cljs.core.str("opacity: 0.0;"),cljs.core.str("box-sizing: border-box;"),cljs.core.str("z-index: 10000;")].join('')], null));
el_34033.onclick = figwheel.client.heads_up.heads_up_onclick_handler;

el_34033.innerHTML = [cljs.core.str(figwheel.client.heads_up.clojure_symbol_svg)].join('');

el_34033.appendChild(figwheel.client.heads_up.node.call(null,new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),content_id], null)));

document.body.appendChild(el_34033);
} else {
}

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"container-el","container-el",109664205),document.getElementById(cont_id),new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187),document.getElementById(content_id)], null);
});
figwheel.client.heads_up.set_style_BANG_ = (function figwheel$client$heads_up$set_style_BANG_(p__34034,st_map){
var map__34038 = p__34034;
var map__34038__$1 = ((cljs.core.seq_QMARK_.call(null,map__34038))?cljs.core.apply.call(null,cljs.core.hash_map,map__34038):map__34038);
var container_el = cljs.core.get.call(null,map__34038__$1,new cljs.core.Keyword(null,"container-el","container-el",109664205));
return cljs.core.mapv.call(null,((function (map__34038,map__34038__$1,container_el){
return (function (p__34039){
var vec__34040 = p__34039;
var k = cljs.core.nth.call(null,vec__34040,(0),null);
var v = cljs.core.nth.call(null,vec__34040,(1),null);
return (container_el.style[cljs.core.name.call(null,k)] = v);
});})(map__34038,map__34038__$1,container_el))
,st_map);
});
figwheel.client.heads_up.set_content_BANG_ = (function figwheel$client$heads_up$set_content_BANG_(p__34041,dom_str){
var map__34043 = p__34041;
var map__34043__$1 = ((cljs.core.seq_QMARK_.call(null,map__34043))?cljs.core.apply.call(null,cljs.core.hash_map,map__34043):map__34043);
var c = map__34043__$1;
var content_area_el = cljs.core.get.call(null,map__34043__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
return content_area_el.innerHTML = dom_str;
});
figwheel.client.heads_up.get_content = (function figwheel$client$heads_up$get_content(p__34044){
var map__34046 = p__34044;
var map__34046__$1 = ((cljs.core.seq_QMARK_.call(null,map__34046))?cljs.core.apply.call(null,cljs.core.hash_map,map__34046):map__34046);
var content_area_el = cljs.core.get.call(null,map__34046__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
return content_area_el.innerHTML;
});
figwheel.client.heads_up.close_link = (function figwheel$client$heads_up$close_link(){
return [cljs.core.str("<a style=\""),cljs.core.str("float: right;"),cljs.core.str("font-size: 18px;"),cljs.core.str("text-decoration: none;"),cljs.core.str("text-align: right;"),cljs.core.str("width: 30px;"),cljs.core.str("height: 30px;"),cljs.core.str("color: rgba(84,84,84, 0.5);"),cljs.core.str("\" href=\"#\"  data-figwheel-event=\"close-heads-up\">"),cljs.core.str("x"),cljs.core.str("</a>")].join('');
});
figwheel.client.heads_up.display_heads_up = (function figwheel$client$heads_up$display_heads_up(style,msg){
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__){
return (function (state_34088){
var state_val_34089 = (state_34088[(1)]);
if((state_val_34089 === (2))){
var inst_34073 = (state_34088[(7)]);
var inst_34082 = (state_34088[(2)]);
var inst_34083 = [new cljs.core.Keyword(null,"height","height",1025178622)];
var inst_34084 = ["auto"];
var inst_34085 = cljs.core.PersistentHashMap.fromArrays(inst_34083,inst_34084);
var inst_34086 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_34073,inst_34085);
var state_34088__$1 = (function (){var statearr_34090 = state_34088;
(statearr_34090[(8)] = inst_34082);

return statearr_34090;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_34088__$1,inst_34086);
} else {
if((state_val_34089 === (1))){
var inst_34073 = (state_34088[(7)]);
var inst_34073__$1 = figwheel.client.heads_up.ensure_container.call(null);
var inst_34074 = [new cljs.core.Keyword(null,"paddingTop","paddingTop",-1088692345),new cljs.core.Keyword(null,"paddingBottom","paddingBottom",-916694489),new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"minHeight","minHeight",-1635998980),new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_34075 = ["10px","10px","100%","68px","1.0"];
var inst_34076 = cljs.core.PersistentHashMap.fromArrays(inst_34074,inst_34075);
var inst_34077 = cljs.core.merge.call(null,inst_34076,style);
var inst_34078 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_34073__$1,inst_34077);
var inst_34079 = figwheel.client.heads_up.set_content_BANG_.call(null,inst_34073__$1,msg);
var inst_34080 = cljs.core.async.timeout.call(null,(300));
var state_34088__$1 = (function (){var statearr_34091 = state_34088;
(statearr_34091[(9)] = inst_34079);

(statearr_34091[(7)] = inst_34073__$1);

(statearr_34091[(10)] = inst_34078);

return statearr_34091;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34088__$1,(2),inst_34080);
} else {
return null;
}
}
});})(c__25999__auto__))
;
return ((function (switch__25937__auto__,c__25999__auto__){
return (function() {
var figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto__ = null;
var figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto____0 = (function (){
var statearr_34095 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_34095[(0)] = figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto__);

(statearr_34095[(1)] = (1));

return statearr_34095;
});
var figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto____1 = (function (state_34088){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_34088);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e34096){if((e34096 instanceof Object)){
var ex__25941__auto__ = e34096;
var statearr_34097_34099 = state_34088;
(statearr_34097_34099[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_34088);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34096;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34100 = state_34088;
state_34088 = G__34100;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto__ = function(state_34088){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto____1.call(this,state_34088);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto____0;
figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto____1;
return figwheel$client$heads_up$display_heads_up_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__))
})();
var state__26001__auto__ = (function (){var statearr_34098 = f__26000__auto__.call(null);
(statearr_34098[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_34098;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__))
);

return c__25999__auto__;
});
figwheel.client.heads_up.heading = (function figwheel$client$heads_up$heading(s){
return [cljs.core.str("<div style=\""),cljs.core.str("font-size: 26px;"),cljs.core.str("line-height: 26px;"),cljs.core.str("margin-bottom: 2px;"),cljs.core.str("padding-top: 1px;"),cljs.core.str("\">"),cljs.core.str(s),cljs.core.str("</div>")].join('');
});
figwheel.client.heads_up.file_and_line_number = (function figwheel$client$heads_up$file_and_line_number(msg){
if(cljs.core.truth_(cljs.core.re_matches.call(null,/.*at\sline.*/,msg))){
return cljs.core.take.call(null,(2),cljs.core.reverse.call(null,clojure.string.split.call(null,msg," ")));
} else {
return null;
}
});
figwheel.client.heads_up.file_selector_div = (function figwheel$client$heads_up$file_selector_div(file_name,line_number,msg){
return [cljs.core.str("<div data-figwheel-event=\"file-selected\" data-file-name=\""),cljs.core.str(file_name),cljs.core.str("\" data-file-line=\""),cljs.core.str(line_number),cljs.core.str("\">"),cljs.core.str(msg),cljs.core.str("</div>")].join('');
});
figwheel.client.heads_up.format_line = (function figwheel$client$heads_up$format_line(msg){
var temp__4124__auto__ = figwheel.client.heads_up.file_and_line_number.call(null,msg);
if(cljs.core.truth_(temp__4124__auto__)){
var vec__34102 = temp__4124__auto__;
var f = cljs.core.nth.call(null,vec__34102,(0),null);
var ln = cljs.core.nth.call(null,vec__34102,(1),null);
return figwheel.client.heads_up.file_selector_div.call(null,f,ln,msg);
} else {
return [cljs.core.str("<div>"),cljs.core.str(msg),cljs.core.str("</div>")].join('');
}
});
figwheel.client.heads_up.display_error = (function figwheel$client$heads_up$display_error(formatted_messages,cause){
var vec__34105 = (cljs.core.truth_(cause)?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(cause),new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause),new cljs.core.Keyword(null,"column","column",2078222095).cljs$core$IFn$_invoke$arity$1(cause)], null):cljs.core.first.call(null,cljs.core.keep.call(null,figwheel.client.heads_up.file_and_line_number,formatted_messages)));
var file_name = cljs.core.nth.call(null,vec__34105,(0),null);
var file_line = cljs.core.nth.call(null,vec__34105,(1),null);
var file_column = cljs.core.nth.call(null,vec__34105,(2),null);
var msg = cljs.core.apply.call(null,cljs.core.str,cljs.core.map.call(null,((function (vec__34105,file_name,file_line,file_column){
return (function (p1__34103_SHARP_){
return [cljs.core.str("<div>"),cljs.core.str(p1__34103_SHARP_),cljs.core.str("</div>")].join('');
});})(vec__34105,file_name,file_line,file_column))
,formatted_messages));
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(255, 161, 161, 0.95)"], null),[cljs.core.str(figwheel.client.heads_up.close_link.call(null)),cljs.core.str(figwheel.client.heads_up.heading.call(null,"Compile Error")),cljs.core.str(figwheel.client.heads_up.file_selector_div.call(null,file_name,(function (){var or__22775__auto__ = file_line;
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
var and__22763__auto__ = cause;
if(cljs.core.truth_(and__22763__auto__)){
return new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause);
} else {
return and__22763__auto__;
}
}
})(),[cljs.core.str(msg),cljs.core.str((cljs.core.truth_(cause)?[cljs.core.str("Error on file "),cljs.core.str(new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", line "),cljs.core.str(new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", column "),cljs.core.str(new cljs.core.Keyword(null,"column","column",2078222095).cljs$core$IFn$_invoke$arity$1(cause))].join(''):""))].join('')))].join(''));
});
figwheel.client.heads_up.display_system_warning = (function figwheel$client$heads_up$display_system_warning(header,msg){
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(255, 220, 110, 0.95)"], null),[cljs.core.str(figwheel.client.heads_up.close_link.call(null)),cljs.core.str(figwheel.client.heads_up.heading.call(null,header)),cljs.core.str(figwheel.client.heads_up.format_line.call(null,msg))].join(''));
});
figwheel.client.heads_up.display_warning = (function figwheel$client$heads_up$display_warning(msg){
return figwheel.client.heads_up.display_system_warning.call(null,"Compile Warning",msg);
});
figwheel.client.heads_up.append_message = (function figwheel$client$heads_up$append_message(message){
var map__34107 = figwheel.client.heads_up.ensure_container.call(null);
var map__34107__$1 = ((cljs.core.seq_QMARK_.call(null,map__34107))?cljs.core.apply.call(null,cljs.core.hash_map,map__34107):map__34107);
var content_area_el = cljs.core.get.call(null,map__34107__$1,new cljs.core.Keyword(null,"content-area-el","content-area-el",742757187));
var el = document.createElement("div");
el.innerHTML = figwheel.client.heads_up.format_line.call(null,message);

return content_area_el.appendChild(el);
});
figwheel.client.heads_up.clear = (function figwheel$client$heads_up$clear(){
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__){
return (function (state_34154){
var state_val_34155 = (state_34154[(1)]);
if((state_val_34155 === (3))){
var inst_34137 = (state_34154[(7)]);
var inst_34151 = (state_34154[(2)]);
var inst_34152 = figwheel.client.heads_up.set_content_BANG_.call(null,inst_34137,"");
var state_34154__$1 = (function (){var statearr_34156 = state_34154;
(statearr_34156[(8)] = inst_34151);

return statearr_34156;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_34154__$1,inst_34152);
} else {
if((state_val_34155 === (2))){
var inst_34137 = (state_34154[(7)]);
var inst_34144 = (state_34154[(2)]);
var inst_34145 = [new cljs.core.Keyword(null,"width","width",-384071477),new cljs.core.Keyword(null,"height","height",1025178622),new cljs.core.Keyword(null,"minHeight","minHeight",-1635998980),new cljs.core.Keyword(null,"padding","padding",1660304693),new cljs.core.Keyword(null,"borderRadius","borderRadius",-1505621083),new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491)];
var inst_34146 = ["auto","0px","0px","0px 10px 0px 70px","0px","transparent"];
var inst_34147 = cljs.core.PersistentHashMap.fromArrays(inst_34145,inst_34146);
var inst_34148 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_34137,inst_34147);
var inst_34149 = cljs.core.async.timeout.call(null,(200));
var state_34154__$1 = (function (){var statearr_34157 = state_34154;
(statearr_34157[(9)] = inst_34144);

(statearr_34157[(10)] = inst_34148);

return statearr_34157;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34154__$1,(3),inst_34149);
} else {
if((state_val_34155 === (1))){
var inst_34137 = (state_34154[(7)]);
var inst_34137__$1 = figwheel.client.heads_up.ensure_container.call(null);
var inst_34138 = [new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_34139 = ["0.0"];
var inst_34140 = cljs.core.PersistentHashMap.fromArrays(inst_34138,inst_34139);
var inst_34141 = figwheel.client.heads_up.set_style_BANG_.call(null,inst_34137__$1,inst_34140);
var inst_34142 = cljs.core.async.timeout.call(null,(300));
var state_34154__$1 = (function (){var statearr_34158 = state_34154;
(statearr_34158[(11)] = inst_34141);

(statearr_34158[(7)] = inst_34137__$1);

return statearr_34158;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34154__$1,(2),inst_34142);
} else {
return null;
}
}
}
});})(c__25999__auto__))
;
return ((function (switch__25937__auto__,c__25999__auto__){
return (function() {
var figwheel$client$heads_up$clear_$_state_machine__25938__auto__ = null;
var figwheel$client$heads_up$clear_$_state_machine__25938__auto____0 = (function (){
var statearr_34162 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34162[(0)] = figwheel$client$heads_up$clear_$_state_machine__25938__auto__);

(statearr_34162[(1)] = (1));

return statearr_34162;
});
var figwheel$client$heads_up$clear_$_state_machine__25938__auto____1 = (function (state_34154){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_34154);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e34163){if((e34163 instanceof Object)){
var ex__25941__auto__ = e34163;
var statearr_34164_34166 = state_34154;
(statearr_34164_34166[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_34154);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34163;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34167 = state_34154;
state_34154 = G__34167;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$heads_up$clear_$_state_machine__25938__auto__ = function(state_34154){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$clear_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$heads_up$clear_$_state_machine__25938__auto____1.call(this,state_34154);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$clear_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$clear_$_state_machine__25938__auto____0;
figwheel$client$heads_up$clear_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$clear_$_state_machine__25938__auto____1;
return figwheel$client$heads_up$clear_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__))
})();
var state__26001__auto__ = (function (){var statearr_34165 = f__26000__auto__.call(null);
(statearr_34165[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_34165;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__))
);

return c__25999__auto__;
});
figwheel.client.heads_up.display_loaded_start = (function figwheel$client$heads_up$display_loaded_start(){
return figwheel.client.heads_up.display_heads_up.call(null,new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"backgroundColor","backgroundColor",1738438491),"rgba(211,234,172,1.0)",new cljs.core.Keyword(null,"width","width",-384071477),"68px",new cljs.core.Keyword(null,"height","height",1025178622),"68px",new cljs.core.Keyword(null,"paddingLeft","paddingLeft",262720813),"0px",new cljs.core.Keyword(null,"paddingRight","paddingRight",-1642313463),"0px",new cljs.core.Keyword(null,"borderRadius","borderRadius",-1505621083),"35px"], null),"");
});
figwheel.client.heads_up.flash_loaded = (function figwheel$client$heads_up$flash_loaded(){
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__){
return (function (state_34199){
var state_val_34200 = (state_34199[(1)]);
if((state_val_34200 === (4))){
var inst_34197 = (state_34199[(2)]);
var state_34199__$1 = state_34199;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_34199__$1,inst_34197);
} else {
if((state_val_34200 === (3))){
var inst_34194 = (state_34199[(2)]);
var inst_34195 = figwheel.client.heads_up.clear.call(null);
var state_34199__$1 = (function (){var statearr_34201 = state_34199;
(statearr_34201[(7)] = inst_34194);

return statearr_34201;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34199__$1,(4),inst_34195);
} else {
if((state_val_34200 === (2))){
var inst_34191 = (state_34199[(2)]);
var inst_34192 = cljs.core.async.timeout.call(null,(400));
var state_34199__$1 = (function (){var statearr_34202 = state_34199;
(statearr_34202[(8)] = inst_34191);

return statearr_34202;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34199__$1,(3),inst_34192);
} else {
if((state_val_34200 === (1))){
var inst_34189 = figwheel.client.heads_up.display_loaded_start.call(null);
var state_34199__$1 = state_34199;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_34199__$1,(2),inst_34189);
} else {
return null;
}
}
}
}
});})(c__25999__auto__))
;
return ((function (switch__25937__auto__,c__25999__auto__){
return (function() {
var figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto__ = null;
var figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto____0 = (function (){
var statearr_34206 = [null,null,null,null,null,null,null,null,null];
(statearr_34206[(0)] = figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto__);

(statearr_34206[(1)] = (1));

return statearr_34206;
});
var figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto____1 = (function (state_34199){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_34199);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e34207){if((e34207 instanceof Object)){
var ex__25941__auto__ = e34207;
var statearr_34208_34210 = state_34199;
(statearr_34208_34210[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_34199);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e34207;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34211 = state_34199;
state_34199 = G__34211;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto__ = function(state_34199){
switch(arguments.length){
case 0:
return figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto____1.call(this,state_34199);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto____0;
figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto____1;
return figwheel$client$heads_up$flash_loaded_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__))
})();
var state__26001__auto__ = (function (){var statearr_34209 = f__26000__auto__.call(null);
(statearr_34209[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_34209;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__))
);

return c__25999__auto__;
});
figwheel.client.heads_up.clojure_symbol_svg = "<?xml version='1.0' encoding='UTF-8' ?>\n<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>\n<svg width='49px' height='49px' viewBox='0 0 100 99' version='1.1' xmlns='http://www.w3.org/2000/svg' style='position:absolute; top:9px; left: 10px;'>\n<circle fill='rgba(255,255,255,0.5)' cx='49.75' cy='49.5' r='48.5'/>\n<path fill='#5881d8' d=' M 39.30 6.22 C 51.71 3.11 65.45 5.64 75.83 13.16 C 88.68 22.10 96.12 38.22 94.43 53.80 C 93.66 60.11 89.40 66.01 83.37 68.24 C 79.21 69.97 74.64 69.78 70.23 69.80 C 80.77 59.67 81.41 41.33 71.45 30.60 C 63.60 21.32 49.75 18.52 38.65 23.16 C 31.27 18.80 21.83 18.68 14.27 22.69 C 20.65 14.79 29.32 8.56 39.30 6.22 Z' />\n<path fill='#90b4fe' d=' M 42.93 26.99 C 48.49 25.50 54.55 25.62 59.79 28.14 C 68.71 32.19 74.61 42.14 73.41 51.94 C 72.85 58.64 68.92 64.53 63.81 68.69 C 59.57 66.71 57.53 62.30 55.66 58.30 C 50.76 48.12 50.23 36.02 42.93 26.99 Z' />\n<path fill='#63b132' d=' M 12.30 33.30 C 17.11 28.49 24.33 26.90 30.91 28.06 C 25.22 33.49 21.44 41.03 21.46 48.99 C 21.11 58.97 26.58 68.76 35.08 73.92 C 43.28 79.06 53.95 79.28 62.66 75.29 C 70.37 77.57 78.52 77.36 86.31 75.57 C 80.05 84.00 70.94 90.35 60.69 92.84 C 48.02 96.03 34.00 93.24 23.56 85.37 C 12.16 77.09 5.12 63.11 5.44 49.00 C 5.15 43.06 8.22 37.42 12.30 33.30 Z' />\n<path fill='#91dc47' d=' M 26.94 54.00 C 24.97 45.06 29.20 35.59 36.45 30.24 C 41.99 33.71 44.23 40.14 46.55 45.91 C 43.00 53.40 38.44 60.46 35.94 68.42 C 31.50 64.74 27.96 59.77 26.94 54.00 Z' />\n<path fill='#91dc47' d=' M 41.97 71.80 C 41.46 64.27 45.31 57.52 48.11 50.80 C 50.40 58.13 51.84 66.19 57.18 72.06 C 52.17 73.37 46.93 73.26 41.97 71.80 Z' />\n</svg>";

//# sourceMappingURL=heads_up.js.map?rel=1446838351717