(ns figwheel.connect (:require [contentful-test.core] [figwheel.client] [figwheel.client.utils]))
(figwheel.client/start {:websocket-url "ws://localhost:3449/figwheel-ws", :on-jsload (fn [& x] (if js/contentful-test.core.mount-root (apply js/contentful-test.core.mount-root x) (figwheel.client.utils/log :debug "Figwheel: :on-jsload hook 'contentful-test.core/mount-root' is missing"))), :build-id "dev"})

