// Compiled by ClojureScript 0.0-3211 {}
goog.provide('figwheel.client');
goog.require('cljs.core');
goog.require('goog.Uri');
goog.require('cljs.core.async');
goog.require('figwheel.client.socket');
goog.require('figwheel.client.file_reloading');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
goog.require('cljs.repl');
goog.require('figwheel.client.heads_up');
figwheel.client.figwheel_repl_print = (function figwheel$client$figwheel_repl_print(args){
figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),"figwheel-repl-print",new cljs.core.Keyword(null,"content","content",15833224),args], null));

return args;
});
figwheel.client.console_print = (function figwheel$client$console_print(args){
console.log.apply(console,cljs.core.into_array.call(null,args));

return args;
});
figwheel.client.enable_repl_print_BANG_ = (function figwheel$client$enable_repl_print_BANG_(){
cljs.core._STAR_print_newline_STAR_ = false;

return cljs.core._STAR_print_fn_STAR_ = (function() { 
var G__33219__delegate = function (args){
return figwheel.client.figwheel_repl_print.call(null,figwheel.client.console_print.call(null,args));
};
var G__33219 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__33220__i = 0, G__33220__a = new Array(arguments.length -  0);
while (G__33220__i < G__33220__a.length) {G__33220__a[G__33220__i] = arguments[G__33220__i + 0]; ++G__33220__i;}
  args = new cljs.core.IndexedSeq(G__33220__a,0);
} 
return G__33219__delegate.call(this,args);};
G__33219.cljs$lang$maxFixedArity = 0;
G__33219.cljs$lang$applyTo = (function (arglist__33221){
var args = cljs.core.seq(arglist__33221);
return G__33219__delegate(args);
});
G__33219.cljs$core$IFn$_invoke$arity$variadic = G__33219__delegate;
return G__33219;
})()
;
});
figwheel.client.get_essential_messages = (function figwheel$client$get_essential_messages(ed){
if(cljs.core.truth_(ed)){
return cljs.core.cons.call(null,cljs.core.select_keys.call(null,ed,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"message","message",-406056002),new cljs.core.Keyword(null,"class","class",-2030961996)], null)),figwheel$client$get_essential_messages.call(null,new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(ed)));
} else {
return null;
}
});
figwheel.client.error_msg_format = (function figwheel$client$error_msg_format(p__33222){
var map__33224 = p__33222;
var map__33224__$1 = ((cljs.core.seq_QMARK_.call(null,map__33224))?cljs.core.apply.call(null,cljs.core.hash_map,map__33224):map__33224);
var class$ = cljs.core.get.call(null,map__33224__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var message = cljs.core.get.call(null,map__33224__$1,new cljs.core.Keyword(null,"message","message",-406056002));
return [cljs.core.str(class$),cljs.core.str(" : "),cljs.core.str(message)].join('');
});
figwheel.client.format_messages = cljs.core.comp.call(null,cljs.core.partial.call(null,cljs.core.map,figwheel.client.error_msg_format),figwheel.client.get_essential_messages);
figwheel.client.focus_msgs = (function figwheel$client$focus_msgs(name_set,msg_hist){
return cljs.core.cons.call(null,cljs.core.first.call(null,msg_hist),cljs.core.filter.call(null,cljs.core.comp.call(null,name_set,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863)),cljs.core.rest.call(null,msg_hist)));
});
figwheel.client.reload_file_QMARK__STAR_ = (function figwheel$client$reload_file_QMARK__STAR_(msg_name,opts){
var or__22775__auto__ = new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return cljs.core.not_EQ_.call(null,msg_name,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356));
}
});
figwheel.client.reload_file_state_QMARK_ = (function figwheel$client$reload_file_state_QMARK_(msg_names,opts){
var and__22763__auto__ = cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563));
if(and__22763__auto__){
return figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts);
} else {
return and__22763__auto__;
}
});
figwheel.client.block_reload_file_state_QMARK_ = (function figwheel$client$block_reload_file_state_QMARK_(msg_names,opts){
return (cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))) && (cljs.core.not.call(null,figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts)));
});
figwheel.client.warning_append_state_QMARK_ = (function figwheel$client$warning_append_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.warning_state_QMARK_ = (function figwheel$client$warning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),cljs.core.first.call(null,msg_names));
});
figwheel.client.rewarning_state_QMARK_ = (function figwheel$client$rewarning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(3),msg_names));
});
figwheel.client.compile_fail_state_QMARK_ = (function figwheel$client$compile_fail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),cljs.core.first.call(null,msg_names));
});
figwheel.client.compile_refail_state_QMARK_ = (function figwheel$client$compile_refail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.css_loaded_state_QMARK_ = (function figwheel$client$css_loaded_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874),cljs.core.first.call(null,msg_names));
});
figwheel.client.file_reloader_plugin = (function figwheel$client$file_reloader_plugin(opts){
var ch = cljs.core.async.chan.call(null);
var c__25999__auto___33353 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___33353,ch){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___33353,ch){
return (function (state_33327){
var state_val_33328 = (state_33327[(1)]);
if((state_val_33328 === (7))){
var inst_33323 = (state_33327[(2)]);
var state_33327__$1 = state_33327;
var statearr_33329_33354 = state_33327__$1;
(statearr_33329_33354[(2)] = inst_33323);

(statearr_33329_33354[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (1))){
var state_33327__$1 = state_33327;
var statearr_33330_33355 = state_33327__$1;
(statearr_33330_33355[(2)] = null);

(statearr_33330_33355[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (4))){
var inst_33291 = (state_33327[(7)]);
var inst_33291__$1 = (state_33327[(2)]);
var state_33327__$1 = (function (){var statearr_33331 = state_33327;
(statearr_33331[(7)] = inst_33291__$1);

return statearr_33331;
})();
if(cljs.core.truth_(inst_33291__$1)){
var statearr_33332_33356 = state_33327__$1;
(statearr_33332_33356[(1)] = (5));

} else {
var statearr_33333_33357 = state_33327__$1;
(statearr_33333_33357[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (13))){
var state_33327__$1 = state_33327;
var statearr_33334_33358 = state_33327__$1;
(statearr_33334_33358[(2)] = null);

(statearr_33334_33358[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (6))){
var state_33327__$1 = state_33327;
var statearr_33335_33359 = state_33327__$1;
(statearr_33335_33359[(2)] = null);

(statearr_33335_33359[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (3))){
var inst_33325 = (state_33327[(2)]);
var state_33327__$1 = state_33327;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33327__$1,inst_33325);
} else {
if((state_val_33328 === (12))){
var inst_33298 = (state_33327[(8)]);
var inst_33311 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_33298);
var inst_33312 = cljs.core.first.call(null,inst_33311);
var inst_33313 = new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(inst_33312);
var inst_33314 = console.warn("Figwheel: Not loading code with warnings - ",inst_33313);
var state_33327__$1 = state_33327;
var statearr_33336_33360 = state_33327__$1;
(statearr_33336_33360[(2)] = inst_33314);

(statearr_33336_33360[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (2))){
var state_33327__$1 = state_33327;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33327__$1,(4),ch);
} else {
if((state_val_33328 === (11))){
var inst_33307 = (state_33327[(2)]);
var state_33327__$1 = state_33327;
var statearr_33337_33361 = state_33327__$1;
(statearr_33337_33361[(2)] = inst_33307);

(statearr_33337_33361[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (9))){
var inst_33297 = (state_33327[(9)]);
var inst_33309 = figwheel.client.block_reload_file_state_QMARK_.call(null,inst_33297,opts);
var state_33327__$1 = state_33327;
if(cljs.core.truth_(inst_33309)){
var statearr_33338_33362 = state_33327__$1;
(statearr_33338_33362[(1)] = (12));

} else {
var statearr_33339_33363 = state_33327__$1;
(statearr_33339_33363[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (5))){
var inst_33291 = (state_33327[(7)]);
var inst_33297 = (state_33327[(9)]);
var inst_33293 = [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null];
var inst_33294 = (new cljs.core.PersistentArrayMap(null,2,inst_33293,null));
var inst_33295 = (new cljs.core.PersistentHashSet(null,inst_33294,null));
var inst_33296 = figwheel.client.focus_msgs.call(null,inst_33295,inst_33291);
var inst_33297__$1 = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),inst_33296);
var inst_33298 = cljs.core.first.call(null,inst_33296);
var inst_33299 = figwheel.client.reload_file_state_QMARK_.call(null,inst_33297__$1,opts);
var state_33327__$1 = (function (){var statearr_33340 = state_33327;
(statearr_33340[(8)] = inst_33298);

(statearr_33340[(9)] = inst_33297__$1);

return statearr_33340;
})();
if(cljs.core.truth_(inst_33299)){
var statearr_33341_33364 = state_33327__$1;
(statearr_33341_33364[(1)] = (8));

} else {
var statearr_33342_33365 = state_33327__$1;
(statearr_33342_33365[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (14))){
var inst_33317 = (state_33327[(2)]);
var state_33327__$1 = state_33327;
var statearr_33343_33366 = state_33327__$1;
(statearr_33343_33366[(2)] = inst_33317);

(statearr_33343_33366[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (10))){
var inst_33319 = (state_33327[(2)]);
var state_33327__$1 = (function (){var statearr_33344 = state_33327;
(statearr_33344[(10)] = inst_33319);

return statearr_33344;
})();
var statearr_33345_33367 = state_33327__$1;
(statearr_33345_33367[(2)] = null);

(statearr_33345_33367[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33328 === (8))){
var inst_33298 = (state_33327[(8)]);
var inst_33301 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_33302 = figwheel.client.file_reloading.reload_js_files.call(null,opts,inst_33298);
var inst_33303 = cljs.core.async.timeout.call(null,(1000));
var inst_33304 = [inst_33302,inst_33303];
var inst_33305 = (new cljs.core.PersistentVector(null,2,(5),inst_33301,inst_33304,null));
var state_33327__$1 = state_33327;
return cljs.core.async.ioc_alts_BANG_.call(null,state_33327__$1,(11),inst_33305);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto___33353,ch))
;
return ((function (switch__25937__auto__,c__25999__auto___33353,ch){
return (function() {
var figwheel$client$file_reloader_plugin_$_state_machine__25938__auto__ = null;
var figwheel$client$file_reloader_plugin_$_state_machine__25938__auto____0 = (function (){
var statearr_33349 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33349[(0)] = figwheel$client$file_reloader_plugin_$_state_machine__25938__auto__);

(statearr_33349[(1)] = (1));

return statearr_33349;
});
var figwheel$client$file_reloader_plugin_$_state_machine__25938__auto____1 = (function (state_33327){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_33327);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e33350){if((e33350 instanceof Object)){
var ex__25941__auto__ = e33350;
var statearr_33351_33368 = state_33327;
(statearr_33351_33368[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33327);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33350;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33369 = state_33327;
state_33327 = G__33369;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$file_reloader_plugin_$_state_machine__25938__auto__ = function(state_33327){
switch(arguments.length){
case 0:
return figwheel$client$file_reloader_plugin_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$file_reloader_plugin_$_state_machine__25938__auto____1.call(this,state_33327);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloader_plugin_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloader_plugin_$_state_machine__25938__auto____0;
figwheel$client$file_reloader_plugin_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloader_plugin_$_state_machine__25938__auto____1;
return figwheel$client$file_reloader_plugin_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___33353,ch))
})();
var state__26001__auto__ = (function (){var statearr_33352 = f__26000__auto__.call(null);
(statearr_33352[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___33353);

return statearr_33352;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___33353,ch))
);


return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.truncate_stack_trace = (function figwheel$client$truncate_stack_trace(stack_str){
return cljs.core.take_while.call(null,(function (p1__33370_SHARP_){
return cljs.core.not.call(null,cljs.core.re_matches.call(null,/.*eval_javascript_STAR__STAR_.*/,p1__33370_SHARP_));
}),clojure.string.split_lines.call(null,stack_str));
});
var base_path_33377 = figwheel.client.utils.base_url_path.call(null);
figwheel.client.eval_javascript_STAR__STAR_ = ((function (base_path_33377){
return (function figwheel$client$eval_javascript_STAR__STAR_(code,result_handler){
try{var _STAR_print_fn_STAR_33375 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR_33376 = cljs.core._STAR_print_newline_STAR_;
cljs.core._STAR_print_fn_STAR_ = ((function (_STAR_print_fn_STAR_33375,_STAR_print_newline_STAR_33376,base_path_33377){
return (function() { 
var G__33378__delegate = function (args){
return figwheel.client.figwheel_repl_print.call(null,figwheel.client.console_print.call(null,args));
};
var G__33378 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__33379__i = 0, G__33379__a = new Array(arguments.length -  0);
while (G__33379__i < G__33379__a.length) {G__33379__a[G__33379__i] = arguments[G__33379__i + 0]; ++G__33379__i;}
  args = new cljs.core.IndexedSeq(G__33379__a,0);
} 
return G__33378__delegate.call(this,args);};
G__33378.cljs$lang$maxFixedArity = 0;
G__33378.cljs$lang$applyTo = (function (arglist__33380){
var args = cljs.core.seq(arglist__33380);
return G__33378__delegate(args);
});
G__33378.cljs$core$IFn$_invoke$arity$variadic = G__33378__delegate;
return G__33378;
})()
;})(_STAR_print_fn_STAR_33375,_STAR_print_newline_STAR_33376,base_path_33377))
;

cljs.core._STAR_print_newline_STAR_ = false;

try{return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"value","value",305978217),[cljs.core.str(eval(code))].join('')], null));
}finally {cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR_33376;

cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR_33375;
}}catch (e33374){if((e33374 instanceof Error)){
var e = e33374;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),clojure.string.join.call(null,"\n",figwheel.client.truncate_stack_trace.call(null,e.stack)),new cljs.core.Keyword(null,"base-path","base-path",495760020),base_path_33377], null));
} else {
var e = e33374;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),"No stacktrace available."], null));

}
}});})(base_path_33377))
;
/**
 * The REPL can disconnect and reconnect lets ensure cljs.user exists at least.
 */
figwheel.client.ensure_cljs_user = (function figwheel$client$ensure_cljs_user(){
if(cljs.core.truth_(cljs.user)){
return null;
} else {
return cljs.user = {};
}
});
figwheel.client.repl_plugin = (function figwheel$client$repl_plugin(p__33381){
var map__33386 = p__33381;
var map__33386__$1 = ((cljs.core.seq_QMARK_.call(null,map__33386))?cljs.core.apply.call(null,cljs.core.hash_map,map__33386):map__33386);
var opts = map__33386__$1;
var build_id = cljs.core.get.call(null,map__33386__$1,new cljs.core.Keyword(null,"build-id","build-id",1642831089));
return ((function (map__33386,map__33386__$1,opts,build_id){
return (function (p__33387){
var vec__33388 = p__33387;
var map__33389 = cljs.core.nth.call(null,vec__33388,(0),null);
var map__33389__$1 = ((cljs.core.seq_QMARK_.call(null,map__33389))?cljs.core.apply.call(null,cljs.core.hash_map,map__33389):map__33389);
var msg = map__33389__$1;
var msg_name = cljs.core.get.call(null,map__33389__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__33388,(1));
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"repl-eval","repl-eval",-1784727398),msg_name)){
figwheel.client.ensure_cljs_user.call(null);

return figwheel.client.eval_javascript_STAR__STAR_.call(null,new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(msg),((function (vec__33388,map__33389,map__33389__$1,msg,msg_name,_,map__33386,map__33386__$1,opts,build_id){
return (function (res){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),new cljs.core.Keyword(null,"callback-name","callback-name",336964714).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"content","content",15833224),res], null));
});})(vec__33388,map__33389,map__33389__$1,msg,msg_name,_,map__33386,map__33386__$1,opts,build_id))
);
} else {
return null;
}
});
;})(map__33386,map__33386__$1,opts,build_id))
});
figwheel.client.css_reloader_plugin = (function figwheel$client$css_reloader_plugin(opts){
return (function (p__33393){
var vec__33394 = p__33393;
var map__33395 = cljs.core.nth.call(null,vec__33394,(0),null);
var map__33395__$1 = ((cljs.core.seq_QMARK_.call(null,map__33395))?cljs.core.apply.call(null,cljs.core.hash_map,map__33395):map__33395);
var msg = map__33395__$1;
var msg_name = cljs.core.get.call(null,map__33395__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__33394,(1));
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874))){
return figwheel.client.file_reloading.reload_css_files.call(null,opts,msg);
} else {
return null;
}
});
});
figwheel.client.compile_fail_warning_plugin = (function figwheel$client$compile_fail_warning_plugin(p__33396){
var map__33404 = p__33396;
var map__33404__$1 = ((cljs.core.seq_QMARK_.call(null,map__33404))?cljs.core.apply.call(null,cljs.core.hash_map,map__33404):map__33404);
var on_compile_fail = cljs.core.get.call(null,map__33404__$1,new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036));
var on_compile_warning = cljs.core.get.call(null,map__33404__$1,new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947));
return ((function (map__33404,map__33404__$1,on_compile_fail,on_compile_warning){
return (function (p__33405){
var vec__33406 = p__33405;
var map__33407 = cljs.core.nth.call(null,vec__33406,(0),null);
var map__33407__$1 = ((cljs.core.seq_QMARK_.call(null,map__33407))?cljs.core.apply.call(null,cljs.core.hash_map,map__33407):map__33407);
var msg = map__33407__$1;
var msg_name = cljs.core.get.call(null,map__33407__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = cljs.core.nthnext.call(null,vec__33406,(1));
var pred__33408 = cljs.core._EQ_;
var expr__33409 = msg_name;
if(cljs.core.truth_(pred__33408.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),expr__33409))){
return on_compile_warning.call(null,msg);
} else {
if(cljs.core.truth_(pred__33408.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),expr__33409))){
return on_compile_fail.call(null,msg);
} else {
return null;
}
}
});
;})(map__33404,map__33404__$1,on_compile_fail,on_compile_warning))
});
figwheel.client.heads_up_plugin_msg_handler = (function figwheel$client$heads_up_plugin_msg_handler(opts,msg_hist_SINGLEQUOTE_){
var msg_hist = figwheel.client.focus_msgs.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null], null), null),msg_hist_SINGLEQUOTE_);
var msg_names = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),msg_hist);
var msg = cljs.core.first.call(null,msg_hist);
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__,msg_hist,msg_names,msg){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__,msg_hist,msg_names,msg){
return (function (state_33610){
var state_val_33611 = (state_33610[(1)]);
if((state_val_33611 === (7))){
var inst_33544 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33612_33653 = state_33610__$1;
(statearr_33612_33653[(2)] = inst_33544);

(statearr_33612_33653[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (20))){
var inst_33572 = figwheel.client.rewarning_state_QMARK_.call(null,msg_names);
var state_33610__$1 = state_33610;
if(cljs.core.truth_(inst_33572)){
var statearr_33613_33654 = state_33610__$1;
(statearr_33613_33654[(1)] = (22));

} else {
var statearr_33614_33655 = state_33610__$1;
(statearr_33614_33655[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (27))){
var inst_33584 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33585 = figwheel.client.heads_up.display_warning.call(null,inst_33584);
var state_33610__$1 = state_33610;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(30),inst_33585);
} else {
if((state_val_33611 === (1))){
var inst_33532 = figwheel.client.reload_file_state_QMARK_.call(null,msg_names,opts);
var state_33610__$1 = state_33610;
if(cljs.core.truth_(inst_33532)){
var statearr_33615_33656 = state_33610__$1;
(statearr_33615_33656[(1)] = (2));

} else {
var statearr_33616_33657 = state_33610__$1;
(statearr_33616_33657[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (24))){
var inst_33600 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33617_33658 = state_33610__$1;
(statearr_33617_33658[(2)] = inst_33600);

(statearr_33617_33658[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (4))){
var inst_33608 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33610__$1,inst_33608);
} else {
if((state_val_33611 === (15))){
var inst_33560 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33561 = figwheel.client.format_messages.call(null,inst_33560);
var inst_33562 = new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33563 = figwheel.client.heads_up.display_error.call(null,inst_33561,inst_33562);
var state_33610__$1 = state_33610;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(18),inst_33563);
} else {
if((state_val_33611 === (21))){
var inst_33602 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33618_33659 = state_33610__$1;
(statearr_33618_33659[(2)] = inst_33602);

(statearr_33618_33659[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (31))){
var inst_33591 = figwheel.client.heads_up.flash_loaded.call(null);
var state_33610__$1 = state_33610;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(34),inst_33591);
} else {
if((state_val_33611 === (32))){
var state_33610__$1 = state_33610;
var statearr_33619_33660 = state_33610__$1;
(statearr_33619_33660[(2)] = null);

(statearr_33619_33660[(1)] = (33));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (33))){
var inst_33596 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33620_33661 = state_33610__$1;
(statearr_33620_33661[(2)] = inst_33596);

(statearr_33620_33661[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (13))){
var inst_33550 = (state_33610[(2)]);
var inst_33551 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33552 = figwheel.client.format_messages.call(null,inst_33551);
var inst_33553 = new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33554 = figwheel.client.heads_up.display_error.call(null,inst_33552,inst_33553);
var state_33610__$1 = (function (){var statearr_33621 = state_33610;
(statearr_33621[(7)] = inst_33550);

return statearr_33621;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(14),inst_33554);
} else {
if((state_val_33611 === (22))){
var inst_33574 = figwheel.client.heads_up.clear.call(null);
var state_33610__$1 = state_33610;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(25),inst_33574);
} else {
if((state_val_33611 === (29))){
var inst_33598 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33622_33662 = state_33610__$1;
(statearr_33622_33662[(2)] = inst_33598);

(statearr_33622_33662[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (6))){
var inst_33540 = figwheel.client.heads_up.clear.call(null);
var state_33610__$1 = state_33610;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(9),inst_33540);
} else {
if((state_val_33611 === (28))){
var inst_33589 = figwheel.client.css_loaded_state_QMARK_.call(null,msg_names);
var state_33610__$1 = state_33610;
if(cljs.core.truth_(inst_33589)){
var statearr_33623_33663 = state_33610__$1;
(statearr_33623_33663[(1)] = (31));

} else {
var statearr_33624_33664 = state_33610__$1;
(statearr_33624_33664[(1)] = (32));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (25))){
var inst_33576 = (state_33610[(2)]);
var inst_33577 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33578 = figwheel.client.heads_up.display_warning.call(null,inst_33577);
var state_33610__$1 = (function (){var statearr_33625 = state_33610;
(statearr_33625[(8)] = inst_33576);

return statearr_33625;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(26),inst_33578);
} else {
if((state_val_33611 === (34))){
var inst_33593 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33626_33665 = state_33610__$1;
(statearr_33626_33665[(2)] = inst_33593);

(statearr_33626_33665[(1)] = (33));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (17))){
var inst_33604 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33627_33666 = state_33610__$1;
(statearr_33627_33666[(2)] = inst_33604);

(statearr_33627_33666[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (3))){
var inst_33546 = figwheel.client.compile_refail_state_QMARK_.call(null,msg_names);
var state_33610__$1 = state_33610;
if(cljs.core.truth_(inst_33546)){
var statearr_33628_33667 = state_33610__$1;
(statearr_33628_33667[(1)] = (10));

} else {
var statearr_33629_33668 = state_33610__$1;
(statearr_33629_33668[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (12))){
var inst_33606 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33630_33669 = state_33610__$1;
(statearr_33630_33669[(2)] = inst_33606);

(statearr_33630_33669[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (2))){
var inst_33534 = new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(opts);
var state_33610__$1 = state_33610;
if(cljs.core.truth_(inst_33534)){
var statearr_33631_33670 = state_33610__$1;
(statearr_33631_33670[(1)] = (5));

} else {
var statearr_33632_33671 = state_33610__$1;
(statearr_33632_33671[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (23))){
var inst_33582 = figwheel.client.warning_state_QMARK_.call(null,msg_names);
var state_33610__$1 = state_33610;
if(cljs.core.truth_(inst_33582)){
var statearr_33633_33672 = state_33610__$1;
(statearr_33633_33672[(1)] = (27));

} else {
var statearr_33634_33673 = state_33610__$1;
(statearr_33634_33673[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (19))){
var inst_33569 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_33570 = figwheel.client.heads_up.append_message.call(null,inst_33569);
var state_33610__$1 = state_33610;
var statearr_33635_33674 = state_33610__$1;
(statearr_33635_33674[(2)] = inst_33570);

(statearr_33635_33674[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (11))){
var inst_33558 = figwheel.client.compile_fail_state_QMARK_.call(null,msg_names);
var state_33610__$1 = state_33610;
if(cljs.core.truth_(inst_33558)){
var statearr_33636_33675 = state_33610__$1;
(statearr_33636_33675[(1)] = (15));

} else {
var statearr_33637_33676 = state_33610__$1;
(statearr_33637_33676[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (9))){
var inst_33542 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33638_33677 = state_33610__$1;
(statearr_33638_33677[(2)] = inst_33542);

(statearr_33638_33677[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (5))){
var inst_33536 = figwheel.client.heads_up.flash_loaded.call(null);
var state_33610__$1 = state_33610;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(8),inst_33536);
} else {
if((state_val_33611 === (14))){
var inst_33556 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33639_33678 = state_33610__$1;
(statearr_33639_33678[(2)] = inst_33556);

(statearr_33639_33678[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (26))){
var inst_33580 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33640_33679 = state_33610__$1;
(statearr_33640_33679[(2)] = inst_33580);

(statearr_33640_33679[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (16))){
var inst_33567 = figwheel.client.warning_append_state_QMARK_.call(null,msg_names);
var state_33610__$1 = state_33610;
if(cljs.core.truth_(inst_33567)){
var statearr_33641_33680 = state_33610__$1;
(statearr_33641_33680[(1)] = (19));

} else {
var statearr_33642_33681 = state_33610__$1;
(statearr_33642_33681[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (30))){
var inst_33587 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33643_33682 = state_33610__$1;
(statearr_33643_33682[(2)] = inst_33587);

(statearr_33643_33682[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (10))){
var inst_33548 = figwheel.client.heads_up.clear.call(null);
var state_33610__$1 = state_33610;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33610__$1,(13),inst_33548);
} else {
if((state_val_33611 === (18))){
var inst_33565 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33644_33683 = state_33610__$1;
(statearr_33644_33683[(2)] = inst_33565);

(statearr_33644_33683[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33611 === (8))){
var inst_33538 = (state_33610[(2)]);
var state_33610__$1 = state_33610;
var statearr_33645_33684 = state_33610__$1;
(statearr_33645_33684[(2)] = inst_33538);

(statearr_33645_33684[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__25999__auto__,msg_hist,msg_names,msg))
;
return ((function (switch__25937__auto__,c__25999__auto__,msg_hist,msg_names,msg){
return (function() {
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto__ = null;
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto____0 = (function (){
var statearr_33649 = [null,null,null,null,null,null,null,null,null];
(statearr_33649[(0)] = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto__);

(statearr_33649[(1)] = (1));

return statearr_33649;
});
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto____1 = (function (state_33610){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_33610);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e33650){if((e33650 instanceof Object)){
var ex__25941__auto__ = e33650;
var statearr_33651_33685 = state_33610;
(statearr_33651_33685[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33610);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33650;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33686 = state_33610;
state_33610 = G__33686;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto__ = function(state_33610){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto____1.call(this,state_33610);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto____0;
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto____1;
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__,msg_hist,msg_names,msg))
})();
var state__26001__auto__ = (function (){var statearr_33652 = f__26000__auto__.call(null);
(statearr_33652[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_33652;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__,msg_hist,msg_names,msg))
);

return c__25999__auto__;
});
figwheel.client.heads_up_plugin = (function figwheel$client$heads_up_plugin(opts){
var ch = cljs.core.async.chan.call(null);
figwheel.client.heads_up_config_options_STAR__STAR_ = opts;

var c__25999__auto___33749 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto___33749,ch){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto___33749,ch){
return (function (state_33732){
var state_val_33733 = (state_33732[(1)]);
if((state_val_33733 === (8))){
var inst_33724 = (state_33732[(2)]);
var state_33732__$1 = (function (){var statearr_33734 = state_33732;
(statearr_33734[(7)] = inst_33724);

return statearr_33734;
})();
var statearr_33735_33750 = state_33732__$1;
(statearr_33735_33750[(2)] = null);

(statearr_33735_33750[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33733 === (7))){
var inst_33728 = (state_33732[(2)]);
var state_33732__$1 = state_33732;
var statearr_33736_33751 = state_33732__$1;
(statearr_33736_33751[(2)] = inst_33728);

(statearr_33736_33751[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33733 === (6))){
var state_33732__$1 = state_33732;
var statearr_33737_33752 = state_33732__$1;
(statearr_33737_33752[(2)] = null);

(statearr_33737_33752[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33733 === (5))){
var inst_33720 = (state_33732[(8)]);
var inst_33722 = figwheel.client.heads_up_plugin_msg_handler.call(null,opts,inst_33720);
var state_33732__$1 = state_33732;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33732__$1,(8),inst_33722);
} else {
if((state_val_33733 === (4))){
var inst_33720 = (state_33732[(8)]);
var inst_33720__$1 = (state_33732[(2)]);
var state_33732__$1 = (function (){var statearr_33738 = state_33732;
(statearr_33738[(8)] = inst_33720__$1);

return statearr_33738;
})();
if(cljs.core.truth_(inst_33720__$1)){
var statearr_33739_33753 = state_33732__$1;
(statearr_33739_33753[(1)] = (5));

} else {
var statearr_33740_33754 = state_33732__$1;
(statearr_33740_33754[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33733 === (3))){
var inst_33730 = (state_33732[(2)]);
var state_33732__$1 = state_33732;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33732__$1,inst_33730);
} else {
if((state_val_33733 === (2))){
var state_33732__$1 = state_33732;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33732__$1,(4),ch);
} else {
if((state_val_33733 === (1))){
var state_33732__$1 = state_33732;
var statearr_33741_33755 = state_33732__$1;
(statearr_33741_33755[(2)] = null);

(statearr_33741_33755[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});})(c__25999__auto___33749,ch))
;
return ((function (switch__25937__auto__,c__25999__auto___33749,ch){
return (function() {
var figwheel$client$heads_up_plugin_$_state_machine__25938__auto__ = null;
var figwheel$client$heads_up_plugin_$_state_machine__25938__auto____0 = (function (){
var statearr_33745 = [null,null,null,null,null,null,null,null,null];
(statearr_33745[(0)] = figwheel$client$heads_up_plugin_$_state_machine__25938__auto__);

(statearr_33745[(1)] = (1));

return statearr_33745;
});
var figwheel$client$heads_up_plugin_$_state_machine__25938__auto____1 = (function (state_33732){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_33732);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e33746){if((e33746 instanceof Object)){
var ex__25941__auto__ = e33746;
var statearr_33747_33756 = state_33732;
(statearr_33747_33756[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33732);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33746;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33757 = state_33732;
state_33732 = G__33757;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_$_state_machine__25938__auto__ = function(state_33732){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_$_state_machine__25938__auto____1.call(this,state_33732);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_$_state_machine__25938__auto____0;
figwheel$client$heads_up_plugin_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_$_state_machine__25938__auto____1;
return figwheel$client$heads_up_plugin_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto___33749,ch))
})();
var state__26001__auto__ = (function (){var statearr_33748 = f__26000__auto__.call(null);
(statearr_33748[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto___33749);

return statearr_33748;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto___33749,ch))
);


figwheel.client.heads_up.ensure_container.call(null);

return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.enforce_project_plugin = (function figwheel$client$enforce_project_plugin(opts){
return (function (msg_hist){
if(((1) < cljs.core.count.call(null,cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"project-id","project-id",206449307),cljs.core.take.call(null,(5),msg_hist)))))){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different project. Shutting socket down.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__25999__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__25999__auto__){
return (function (){
var f__26000__auto__ = (function (){var switch__25937__auto__ = ((function (c__25999__auto__){
return (function (state_33778){
var state_val_33779 = (state_33778[(1)]);
if((state_val_33779 === (2))){
var inst_33775 = (state_33778[(2)]);
var inst_33776 = figwheel.client.heads_up.display_system_warning.call(null,"Connection from different project","Shutting connection down!!!!!");
var state_33778__$1 = (function (){var statearr_33780 = state_33778;
(statearr_33780[(7)] = inst_33775);

return statearr_33780;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33778__$1,inst_33776);
} else {
if((state_val_33779 === (1))){
var inst_33773 = cljs.core.async.timeout.call(null,(3000));
var state_33778__$1 = state_33778;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33778__$1,(2),inst_33773);
} else {
return null;
}
}
});})(c__25999__auto__))
;
return ((function (switch__25937__auto__,c__25999__auto__){
return (function() {
var figwheel$client$enforce_project_plugin_$_state_machine__25938__auto__ = null;
var figwheel$client$enforce_project_plugin_$_state_machine__25938__auto____0 = (function (){
var statearr_33784 = [null,null,null,null,null,null,null,null];
(statearr_33784[(0)] = figwheel$client$enforce_project_plugin_$_state_machine__25938__auto__);

(statearr_33784[(1)] = (1));

return statearr_33784;
});
var figwheel$client$enforce_project_plugin_$_state_machine__25938__auto____1 = (function (state_33778){
while(true){
var ret_value__25939__auto__ = (function (){try{while(true){
var result__25940__auto__ = switch__25937__auto__.call(null,state_33778);
if(cljs.core.keyword_identical_QMARK_.call(null,result__25940__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__25940__auto__;
}
break;
}
}catch (e33785){if((e33785 instanceof Object)){
var ex__25941__auto__ = e33785;
var statearr_33786_33788 = state_33778;
(statearr_33786_33788[(5)] = ex__25941__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33778);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33785;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__25939__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33789 = state_33778;
state_33778 = G__33789;
continue;
} else {
return ret_value__25939__auto__;
}
break;
}
});
figwheel$client$enforce_project_plugin_$_state_machine__25938__auto__ = function(state_33778){
switch(arguments.length){
case 0:
return figwheel$client$enforce_project_plugin_$_state_machine__25938__auto____0.call(this);
case 1:
return figwheel$client$enforce_project_plugin_$_state_machine__25938__auto____1.call(this,state_33778);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_project_plugin_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_project_plugin_$_state_machine__25938__auto____0;
figwheel$client$enforce_project_plugin_$_state_machine__25938__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_project_plugin_$_state_machine__25938__auto____1;
return figwheel$client$enforce_project_plugin_$_state_machine__25938__auto__;
})()
;})(switch__25937__auto__,c__25999__auto__))
})();
var state__26001__auto__ = (function (){var statearr_33787 = f__26000__auto__.call(null);
(statearr_33787[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__25999__auto__);

return statearr_33787;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__26001__auto__);
});})(c__25999__auto__))
);

return c__25999__auto__;
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.default_on_jsload = cljs.core.identity;
figwheel.client.default_on_compile_fail = (function figwheel$client$default_on_compile_fail(p__33790){
var map__33796 = p__33790;
var map__33796__$1 = ((cljs.core.seq_QMARK_.call(null,map__33796))?cljs.core.apply.call(null,cljs.core.hash_map,map__33796):map__33796);
var ed = map__33796__$1;
var cause = cljs.core.get.call(null,map__33796__$1,new cljs.core.Keyword(null,"cause","cause",231901252));
var exception_data = cljs.core.get.call(null,map__33796__$1,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886));
var formatted_exception = cljs.core.get.call(null,map__33796__$1,new cljs.core.Keyword(null,"formatted-exception","formatted-exception",-116489026));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: Compile Exception");

var seq__33797_33801 = cljs.core.seq.call(null,figwheel.client.format_messages.call(null,exception_data));
var chunk__33798_33802 = null;
var count__33799_33803 = (0);
var i__33800_33804 = (0);
while(true){
if((i__33800_33804 < count__33799_33803)){
var msg_33805 = cljs.core._nth.call(null,chunk__33798_33802,i__33800_33804);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_33805);

var G__33806 = seq__33797_33801;
var G__33807 = chunk__33798_33802;
var G__33808 = count__33799_33803;
var G__33809 = (i__33800_33804 + (1));
seq__33797_33801 = G__33806;
chunk__33798_33802 = G__33807;
count__33799_33803 = G__33808;
i__33800_33804 = G__33809;
continue;
} else {
var temp__4126__auto___33810 = cljs.core.seq.call(null,seq__33797_33801);
if(temp__4126__auto___33810){
var seq__33797_33811__$1 = temp__4126__auto___33810;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__33797_33811__$1)){
var c__23560__auto___33812 = cljs.core.chunk_first.call(null,seq__33797_33811__$1);
var G__33813 = cljs.core.chunk_rest.call(null,seq__33797_33811__$1);
var G__33814 = c__23560__auto___33812;
var G__33815 = cljs.core.count.call(null,c__23560__auto___33812);
var G__33816 = (0);
seq__33797_33801 = G__33813;
chunk__33798_33802 = G__33814;
count__33799_33803 = G__33815;
i__33800_33804 = G__33816;
continue;
} else {
var msg_33817 = cljs.core.first.call(null,seq__33797_33811__$1);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_33817);

var G__33818 = cljs.core.next.call(null,seq__33797_33811__$1);
var G__33819 = null;
var G__33820 = (0);
var G__33821 = (0);
seq__33797_33801 = G__33818;
chunk__33798_33802 = G__33819;
count__33799_33803 = G__33820;
i__33800_33804 = G__33821;
continue;
}
} else {
}
}
break;
}

if(cljs.core.truth_(cause)){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Error on file "),cljs.core.str(new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", line "),cljs.core.str(new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(cause)),cljs.core.str(", column "),cljs.core.str(new cljs.core.Keyword(null,"column","column",2078222095).cljs$core$IFn$_invoke$arity$1(cause))].join(''));
} else {
}

return ed;
});
figwheel.client.default_on_compile_warning = (function figwheel$client$default_on_compile_warning(p__33822){
var map__33824 = p__33822;
var map__33824__$1 = ((cljs.core.seq_QMARK_.call(null,map__33824))?cljs.core.apply.call(null,cljs.core.hash_map,map__33824):map__33824);
var w = map__33824__$1;
var message = cljs.core.get.call(null,map__33824__$1,new cljs.core.Keyword(null,"message","message",-406056002));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),[cljs.core.str("Figwheel: Compile Warning - "),cljs.core.str(message)].join(''));

return w;
});
figwheel.client.default_before_load = (function figwheel$client$default_before_load(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: notified of file changes");

return files;
});
figwheel.client.default_on_cssload = (function figwheel$client$default_on_cssload(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded CSS files");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),cljs.core.pr_str.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files)));

return files;
});
if(typeof figwheel.client.config_defaults !== 'undefined'){
} else {
figwheel.client.config_defaults = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"load-unchanged-files","load-unchanged-files",-1561468704),new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947),new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036),new cljs.core.Keyword(null,"debug","debug",-1608172596),new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202),new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128),new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223),new cljs.core.Keyword(null,"retry-count","retry-count",1936122875),new cljs.core.Keyword(null,"autoload","autoload",-354122500),new cljs.core.Keyword(null,"url-rewriter","url-rewriter",200543838),new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318)],[true,figwheel.client.default_on_compile_warning,figwheel.client.default_on_jsload,figwheel.client.default_on_compile_fail,false,true,[cljs.core.str("ws://"),cljs.core.str((cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?location.host:"localhost:3449")),cljs.core.str("/figwheel-ws")].join(''),figwheel.client.default_before_load,false,(100),true,false,figwheel.client.default_on_cssload]);
}
figwheel.client.handle_deprecated_jsload_callback = (function figwheel$client$handle_deprecated_jsload_callback(config){
if(cljs.core.truth_(new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config))){
return cljs.core.dissoc.call(null,cljs.core.assoc.call(null,config,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config)),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369));
} else {
return config;
}
});
figwheel.client.base_plugins = (function figwheel$client$base_plugins(system_options){
var base = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"enforce-project-plugin","enforce-project-plugin",959402899),figwheel.client.enforce_project_plugin,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),figwheel.client.file_reloader_plugin,new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),figwheel.client.compile_fail_warning_plugin,new cljs.core.Keyword(null,"css-reloader-plugin","css-reloader-plugin",2002032904),figwheel.client.css_reloader_plugin,new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371),figwheel.client.repl_plugin], null);
var base__$1 = ((cljs.core.not.call(null,figwheel.client.utils.html_env_QMARK_.call(null)))?cljs.core.select_keys.call(null,base,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371)], null)):base);
var base__$2 = ((new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(system_options) === false)?cljs.core.dissoc.call(null,base__$1,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733)):base__$1);
if(cljs.core.truth_((function (){var and__22763__auto__ = new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(system_options);
if(cljs.core.truth_(and__22763__auto__)){
return figwheel.client.utils.html_env_QMARK_.call(null);
} else {
return and__22763__auto__;
}
})())){
return cljs.core.assoc.call(null,base__$2,new cljs.core.Keyword(null,"heads-up-display-plugin","heads-up-display-plugin",1745207501),figwheel.client.heads_up_plugin);
} else {
return base__$2;
}
});
figwheel.client.add_plugins = (function figwheel$client$add_plugins(plugins,system_options){
var seq__33831 = cljs.core.seq.call(null,plugins);
var chunk__33832 = null;
var count__33833 = (0);
var i__33834 = (0);
while(true){
if((i__33834 < count__33833)){
var vec__33835 = cljs.core._nth.call(null,chunk__33832,i__33834);
var k = cljs.core.nth.call(null,vec__33835,(0),null);
var plugin = cljs.core.nth.call(null,vec__33835,(1),null);
if(cljs.core.truth_(plugin)){
var pl_33837 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__33831,chunk__33832,count__33833,i__33834,pl_33837,vec__33835,k,plugin){
return (function (_,___$1,___$2,msg_hist){
return pl_33837.call(null,msg_hist);
});})(seq__33831,chunk__33832,count__33833,i__33834,pl_33837,vec__33835,k,plugin))
);
} else {
}

var G__33838 = seq__33831;
var G__33839 = chunk__33832;
var G__33840 = count__33833;
var G__33841 = (i__33834 + (1));
seq__33831 = G__33838;
chunk__33832 = G__33839;
count__33833 = G__33840;
i__33834 = G__33841;
continue;
} else {
var temp__4126__auto__ = cljs.core.seq.call(null,seq__33831);
if(temp__4126__auto__){
var seq__33831__$1 = temp__4126__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__33831__$1)){
var c__23560__auto__ = cljs.core.chunk_first.call(null,seq__33831__$1);
var G__33842 = cljs.core.chunk_rest.call(null,seq__33831__$1);
var G__33843 = c__23560__auto__;
var G__33844 = cljs.core.count.call(null,c__23560__auto__);
var G__33845 = (0);
seq__33831 = G__33842;
chunk__33832 = G__33843;
count__33833 = G__33844;
i__33834 = G__33845;
continue;
} else {
var vec__33836 = cljs.core.first.call(null,seq__33831__$1);
var k = cljs.core.nth.call(null,vec__33836,(0),null);
var plugin = cljs.core.nth.call(null,vec__33836,(1),null);
if(cljs.core.truth_(plugin)){
var pl_33846 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__33831,chunk__33832,count__33833,i__33834,pl_33846,vec__33836,k,plugin,seq__33831__$1,temp__4126__auto__){
return (function (_,___$1,___$2,msg_hist){
return pl_33846.call(null,msg_hist);
});})(seq__33831,chunk__33832,count__33833,i__33834,pl_33846,vec__33836,k,plugin,seq__33831__$1,temp__4126__auto__))
);
} else {
}

var G__33847 = cljs.core.next.call(null,seq__33831__$1);
var G__33848 = null;
var G__33849 = (0);
var G__33850 = (0);
seq__33831 = G__33847;
chunk__33832 = G__33848;
count__33833 = G__33849;
i__33834 = G__33850;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.start = (function figwheel$client$start(){
var G__33852 = arguments.length;
switch (G__33852) {
case 1:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 0:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$0();

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(arguments.length)].join('')));

}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$1 = (function (opts){
if((goog.dependencies_ == null)){
return null;
} else {
if(typeof figwheel.client.__figwheel_start_once__ !== 'undefined'){
return null;
} else {
figwheel.client.__figwheel_start_once__ = setTimeout((function (){
var plugins_SINGLEQUOTE_ = new cljs.core.Keyword(null,"plugins","plugins",1900073717).cljs$core$IFn$_invoke$arity$1(opts);
var merge_plugins = new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370).cljs$core$IFn$_invoke$arity$1(opts);
var system_options = figwheel.client.handle_deprecated_jsload_callback.call(null,cljs.core.merge.call(null,figwheel.client.config_defaults,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370))));
var plugins = (cljs.core.truth_(plugins_SINGLEQUOTE_)?plugins_SINGLEQUOTE_:cljs.core.merge.call(null,figwheel.client.base_plugins.call(null,system_options),merge_plugins));
figwheel.client.utils._STAR_print_debug_STAR_ = new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(opts);

figwheel.client.add_plugins.call(null,plugins,system_options);

figwheel.client.file_reloading.patch_goog_base.call(null);

return figwheel.client.socket.open.call(null,system_options);
}));
}
}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$0 = (function (){
return figwheel.client.start.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

figwheel.client.start.cljs$lang$maxFixedArity = 1;
figwheel.client.watch_and_reload_with_opts = figwheel.client.start;
figwheel.client.watch_and_reload = (function figwheel$client$watch_and_reload(){
var argseq__23815__auto__ = ((((0) < arguments.length))?(new cljs.core.IndexedSeq(Array.prototype.slice.call(arguments,(0)),(0))):null);
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(argseq__23815__auto__);
});

figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic = (function (p__33855){
var map__33856 = p__33855;
var map__33856__$1 = ((cljs.core.seq_QMARK_.call(null,map__33856))?cljs.core.apply.call(null,cljs.core.hash_map,map__33856):map__33856);
var opts = map__33856__$1;
return figwheel.client.start.call(null,opts);
});

figwheel.client.watch_and_reload.cljs$lang$maxFixedArity = (0);

figwheel.client.watch_and_reload.cljs$lang$applyTo = (function (seq33854){
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33854));
});

//# sourceMappingURL=client.js.map?rel=1446838351218