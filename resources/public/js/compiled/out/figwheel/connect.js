// Compiled by ClojureScript 0.0-3211 {}
goog.provide('figwheel.connect');
goog.require('cljs.core');
goog.require('figwheel.client.utils');
goog.require('figwheel.client');
goog.require('contentful_test.core');
figwheel.client.start.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),"ws://localhost:3449/figwheel-ws",new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),(function() { 
var G__25902__delegate = function (x){
if(cljs.core.truth_(contentful_test.core.mount_root)){
return cljs.core.apply.call(null,contentful_test.core.mount_root,x);
} else {
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: :on-jsload hook 'contentful-test.core/mount-root' is missing");
}
};
var G__25902 = function (var_args){
var x = null;
if (arguments.length > 0) {
var G__25903__i = 0, G__25903__a = new Array(arguments.length -  0);
while (G__25903__i < G__25903__a.length) {G__25903__a[G__25903__i] = arguments[G__25903__i + 0]; ++G__25903__i;}
  x = new cljs.core.IndexedSeq(G__25903__a,0);
} 
return G__25902__delegate.call(this,x);};
G__25902.cljs$lang$maxFixedArity = 0;
G__25902.cljs$lang$applyTo = (function (arglist__25904){
var x = cljs.core.seq(arglist__25904);
return G__25902__delegate(x);
});
G__25902.cljs$core$IFn$_invoke$arity$variadic = G__25902__delegate;
return G__25902;
})()
,new cljs.core.Keyword(null,"build-id","build-id",1642831089),"dev"], null));

//# sourceMappingURL=connect.js.map?rel=1446840775038