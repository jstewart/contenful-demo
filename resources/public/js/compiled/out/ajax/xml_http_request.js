// Compiled by ClojureScript 0.0-3211 {}
goog.provide('ajax.xml_http_request');
goog.require('cljs.core');
goog.require('ajax.protocols');
ajax.xml_http_request.ready_state = (function ajax$xml_http_request$ready_state(e){
return new cljs.core.PersistentArrayMap(null, 5, [(0),new cljs.core.Keyword(null,"not-initialized","not-initialized",-1937378906),(1),new cljs.core.Keyword(null,"connection-established","connection-established",-1403749733),(2),new cljs.core.Keyword(null,"request-received","request-received",2110590540),(3),new cljs.core.Keyword(null,"processing-request","processing-request",-264947221),(4),new cljs.core.Keyword(null,"response-ready","response-ready",245208276)], null).call(null,e.target.readyState);
});
XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_body$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.response;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_status$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.status;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_status_text$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.statusText;
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_get_response_header$arity$2 = (function (this$,header){
var this$__$1 = this;
return this$__$1.getResponseHeader(header);
});

XMLHttpRequest.prototype.ajax$protocols$AjaxResponse$_was_aborted$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core._EQ_.call(null,(0),this$__$1.readyState);
});

XMLHttpRequest.prototype.ajax$protocols$AjaxRequest$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxRequest$_abort$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.abort();
});

XMLHttpRequest.prototype.ajax$protocols$AjaxImpl$ = true;

XMLHttpRequest.prototype.ajax$protocols$AjaxImpl$_js_ajax_request$arity$3 = (function (this$,p__26699,handler){
var map__26700 = p__26699;
var map__26700__$1 = ((cljs.core.seq_QMARK_.call(null,map__26700))?cljs.core.apply.call(null,cljs.core.hash_map,map__26700):map__26700);
var response_format = cljs.core.get.call(null,map__26700__$1,new cljs.core.Keyword(null,"response-format","response-format",1664465322));
var with_credentials = cljs.core.get.call(null,map__26700__$1,new cljs.core.Keyword(null,"with-credentials","with-credentials",-1163127235),false);
var timeout = cljs.core.get.call(null,map__26700__$1,new cljs.core.Keyword(null,"timeout","timeout",-318625318),(0));
var headers = cljs.core.get.call(null,map__26700__$1,new cljs.core.Keyword(null,"headers","headers",-835030129));
var body = cljs.core.get.call(null,map__26700__$1,new cljs.core.Keyword(null,"body","body",-2049205669));
var method = cljs.core.get.call(null,map__26700__$1,new cljs.core.Keyword(null,"method","method",55703592));
var uri = cljs.core.get.call(null,map__26700__$1,new cljs.core.Keyword(null,"uri","uri",-774711847));
var this$__$1 = this;
this$__$1.withCredentials = with_credentials;

this$__$1.onreadystatechange = ((function (this$__$1,map__26700,map__26700__$1,response_format,with_credentials,timeout,headers,body,method,uri){
return (function (p1__26698_SHARP_){
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"response-ready","response-ready",245208276),ajax.xml_http_request.ready_state.call(null,p1__26698_SHARP_))){
return handler.call(null,this$__$1);
} else {
return null;
}
});})(this$__$1,map__26700,map__26700__$1,response_format,with_credentials,timeout,headers,body,method,uri))
;

this$__$1.open(method,uri,true);

this$__$1.timeout = timeout;

var temp__4126__auto___26707 = new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(response_format);
if(cljs.core.truth_(temp__4126__auto___26707)){
var response_type_26708 = temp__4126__auto___26707;
this$__$1.responseType = cljs.core.name.call(null,response_type_26708);
} else {
}

var seq__26701_26709 = cljs.core.seq.call(null,headers);
var chunk__26702_26710 = null;
var count__26703_26711 = (0);
var i__26704_26712 = (0);
while(true){
if((i__26704_26712 < count__26703_26711)){
var vec__26705_26713 = cljs.core._nth.call(null,chunk__26702_26710,i__26704_26712);
var k_26714 = cljs.core.nth.call(null,vec__26705_26713,(0),null);
var v_26715 = cljs.core.nth.call(null,vec__26705_26713,(1),null);
this$__$1.setRequestHeader(k_26714,v_26715);

var G__26716 = seq__26701_26709;
var G__26717 = chunk__26702_26710;
var G__26718 = count__26703_26711;
var G__26719 = (i__26704_26712 + (1));
seq__26701_26709 = G__26716;
chunk__26702_26710 = G__26717;
count__26703_26711 = G__26718;
i__26704_26712 = G__26719;
continue;
} else {
var temp__4126__auto___26720 = cljs.core.seq.call(null,seq__26701_26709);
if(temp__4126__auto___26720){
var seq__26701_26721__$1 = temp__4126__auto___26720;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__26701_26721__$1)){
var c__23560__auto___26722 = cljs.core.chunk_first.call(null,seq__26701_26721__$1);
var G__26723 = cljs.core.chunk_rest.call(null,seq__26701_26721__$1);
var G__26724 = c__23560__auto___26722;
var G__26725 = cljs.core.count.call(null,c__23560__auto___26722);
var G__26726 = (0);
seq__26701_26709 = G__26723;
chunk__26702_26710 = G__26724;
count__26703_26711 = G__26725;
i__26704_26712 = G__26726;
continue;
} else {
var vec__26706_26727 = cljs.core.first.call(null,seq__26701_26721__$1);
var k_26728 = cljs.core.nth.call(null,vec__26706_26727,(0),null);
var v_26729 = cljs.core.nth.call(null,vec__26706_26727,(1),null);
this$__$1.setRequestHeader(k_26728,v_26729);

var G__26730 = cljs.core.next.call(null,seq__26701_26721__$1);
var G__26731 = null;
var G__26732 = (0);
var G__26733 = (0);
seq__26701_26709 = G__26730;
chunk__26702_26710 = G__26731;
count__26703_26711 = G__26732;
i__26704_26712 = G__26733;
continue;
}
} else {
}
}
break;
}

this$__$1.send((function (){var or__22775__auto__ = body;
if(cljs.core.truth_(or__22775__auto__)){
return or__22775__auto__;
} else {
return "";
}
})());

return this$__$1;
});

//# sourceMappingURL=xml_http_request.js.map?rel=1446841697462