// Compiled by ClojureScript 0.0-3211 {}
goog.provide('ajax.protocols');
goog.require('cljs.core');

/**
 * An abstraction for a javascript class that implements
 * Ajax calls.
 */
ajax.protocols.AjaxImpl = (function (){var obj26539 = {};
return obj26539;
})();

/**
 * Makes an actual ajax request.  All parameters except opts
 * are in JS format.  Should return an AjaxRequest.
 */
ajax.protocols._js_ajax_request = (function ajax$protocols$_js_ajax_request(this$,request,handler){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$AjaxImpl$_js_ajax_request$arity$3;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$AjaxImpl$_js_ajax_request$arity$3(this$,request,handler);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._js_ajax_request[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._js_ajax_request["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"AjaxImpl.-js-ajax-request",this$);
}
}
})().call(null,this$,request,handler);
}
});


/**
 * An abstraction for a running ajax request.
 */
ajax.protocols.AjaxRequest = (function (){var obj26541 = {};
return obj26541;
})();

/**
 * Aborts a running ajax request, if possible.
 */
ajax.protocols._abort = (function ajax$protocols$_abort(this$){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$AjaxRequest$_abort$arity$1;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$AjaxRequest$_abort$arity$1(this$);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._abort[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._abort["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"AjaxRequest.-abort",this$);
}
}
})().call(null,this$);
}
});


/**
 * An abstraction for an ajax response.
 */
ajax.protocols.AjaxResponse = (function (){var obj26543 = {};
return obj26543;
})();

/**
 * Returns the HTTP Status of the response as an integer.
 */
ajax.protocols._status = (function ajax$protocols$_status(this$){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$AjaxResponse$_status$arity$1;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$AjaxResponse$_status$arity$1(this$);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._status[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._status["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"AjaxResponse.-status",this$);
}
}
})().call(null,this$);
}
});

/**
 * Returns the HTTP Status Text of the response as a string.
 */
ajax.protocols._status_text = (function ajax$protocols$_status_text(this$){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$AjaxResponse$_status_text$arity$1;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$AjaxResponse$_status_text$arity$1(this$);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._status_text[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._status_text["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"AjaxResponse.-status-text",this$);
}
}
})().call(null,this$);
}
});

/**
 * Returns the response body as a string or as type specified in response-format such as a blob or arraybuffer.
 */
ajax.protocols._body = (function ajax$protocols$_body(this$){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$AjaxResponse$_body$arity$1;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$AjaxResponse$_body$arity$1(this$);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._body[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._body["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"AjaxResponse.-body",this$);
}
}
})().call(null,this$);
}
});

/**
 * Gets the specified response header (specified by a string) as a string.
 */
ajax.protocols._get_response_header = (function ajax$protocols$_get_response_header(this$,header){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$AjaxResponse$_get_response_header$arity$2;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$AjaxResponse$_get_response_header$arity$2(this$,header);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._get_response_header[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._get_response_header["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"AjaxResponse.-get-response-header",this$);
}
}
})().call(null,this$,header);
}
});

/**
 * Was the response aborted.
 */
ajax.protocols._was_aborted = (function ajax$protocols$_was_aborted(this$){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$AjaxResponse$_was_aborted$arity$1;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$AjaxResponse$_was_aborted$arity$1(this$);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._was_aborted[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._was_aborted["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"AjaxResponse.-was-aborted",this$);
}
}
})().call(null,this$);
}
});


/**
 * An abstraction for something that processes requests and responses.
 */
ajax.protocols.Interceptor = (function (){var obj26545 = {};
return obj26545;
})();

/**
 * Transforms the opts
 */
ajax.protocols._process_request = (function ajax$protocols$_process_request(this$,request){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$Interceptor$_process_request$arity$2;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$Interceptor$_process_request$arity$2(this$,request);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._process_request[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._process_request["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Interceptor.-process-request",this$);
}
}
})().call(null,this$,request);
}
});

/**
 * Transforms the raw response (an implementation of AjaxResponse)
 */
ajax.protocols._process_response = (function ajax$protocols$_process_response(this$,response){
if((function (){var and__22763__auto__ = this$;
if(and__22763__auto__){
return this$.ajax$protocols$Interceptor$_process_response$arity$2;
} else {
return and__22763__auto__;
}
})()){
return this$.ajax$protocols$Interceptor$_process_response$arity$2(this$,response);
} else {
var x__23411__auto__ = (((this$ == null))?null:this$);
return (function (){var or__22775__auto__ = (ajax.protocols._process_response[goog.typeOf(x__23411__auto__)]);
if(or__22775__auto__){
return or__22775__auto__;
} else {
var or__22775__auto____$1 = (ajax.protocols._process_response["_"]);
if(or__22775__auto____$1){
return or__22775__auto____$1;
} else {
throw cljs.core.missing_protocol.call(null,"Interceptor.-process-response",this$);
}
}
})().call(null,this$,response);
}
});


/**
* @constructor
* @param {*} status
* @param {*} body
* @param {*} status_text
* @param {*} headers
* @param {*} was_aborted
* @param {*} __meta
* @param {*} __extmap
* @param {*} __hash
* @param {*=} __meta 
* @param {*=} __extmap
* @param {number|null} __hash
*/
ajax.protocols.Response = (function (status,body,status_text,headers,was_aborted,__meta,__extmap,__hash){
this.status = status;
this.body = body;
this.status_text = status_text;
this.headers = headers;
this.was_aborted = was_aborted;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2229667594;
this.cljs$lang$protocol_mask$partition1$ = 8192;
})
ajax.protocols.Response.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__23370__auto__,k__23371__auto__){
var self__ = this;
var this__23370__auto____$1 = this;
return cljs.core._lookup.call(null,this__23370__auto____$1,k__23371__auto__,null);
});

ajax.protocols.Response.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__23372__auto__,k26547,else__23373__auto__){
var self__ = this;
var this__23372__auto____$1 = this;
var G__26549 = (((k26547 instanceof cljs.core.Keyword))?k26547.fqn:null);
switch (G__26549) {
case "was-aborted":
return self__.was_aborted;

break;
case "headers":
return self__.headers;

break;
case "status-text":
return self__.status_text;

break;
case "body":
return self__.body;

break;
case "status":
return self__.status;

break;
default:
return cljs.core.get.call(null,self__.__extmap,k26547,else__23373__auto__);

}
});

ajax.protocols.Response.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__23384__auto__,writer__23385__auto__,opts__23386__auto__){
var self__ = this;
var this__23384__auto____$1 = this;
var pr_pair__23387__auto__ = ((function (this__23384__auto____$1){
return (function (keyval__23388__auto__){
return cljs.core.pr_sequential_writer.call(null,writer__23385__auto__,cljs.core.pr_writer,""," ","",opts__23386__auto__,keyval__23388__auto__);
});})(this__23384__auto____$1))
;
return cljs.core.pr_sequential_writer.call(null,writer__23385__auto__,pr_pair__23387__auto__,"#ajax.protocols.Response{",", ","}",opts__23386__auto__,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"status","status",-1997798413),self__.status],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"body","body",-2049205669),self__.body],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"status-text","status-text",-1834235478),self__.status_text],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"headers","headers",-835030129),self__.headers],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"was-aborted","was-aborted",-2120084828),self__.was_aborted],null))], null),self__.__extmap));
});

ajax.protocols.Response.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__23368__auto__){
var self__ = this;
var this__23368__auto____$1 = this;
return self__.__meta;
});

ajax.protocols.Response.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__23364__auto__){
var self__ = this;
var this__23364__auto____$1 = this;
return (new ajax.protocols.Response(self__.status,self__.body,self__.status_text,self__.headers,self__.was_aborted,self__.__meta,self__.__extmap,self__.__hash));
});

ajax.protocols.Response.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__23374__auto__){
var self__ = this;
var this__23374__auto____$1 = this;
return (5 + cljs.core.count.call(null,self__.__extmap));
});

ajax.protocols.Response.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__23365__auto__){
var self__ = this;
var this__23365__auto____$1 = this;
var h__23191__auto__ = self__.__hash;
if(!((h__23191__auto__ == null))){
return h__23191__auto__;
} else {
var h__23191__auto____$1 = cljs.core.hash_imap.call(null,this__23365__auto____$1);
self__.__hash = h__23191__auto____$1;

return h__23191__auto____$1;
}
});

ajax.protocols.Response.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this__23366__auto__,other__23367__auto__){
var self__ = this;
var this__23366__auto____$1 = this;
if(cljs.core.truth_((function (){var and__22763__auto__ = other__23367__auto__;
if(cljs.core.truth_(and__22763__auto__)){
var and__22763__auto____$1 = (this__23366__auto____$1.constructor === other__23367__auto__.constructor);
if(and__22763__auto____$1){
return cljs.core.equiv_map.call(null,this__23366__auto____$1,other__23367__auto__);
} else {
return and__22763__auto____$1;
}
} else {
return and__22763__auto__;
}
})())){
return true;
} else {
return false;
}
});

ajax.protocols.Response.prototype.ajax$protocols$AjaxResponse$ = true;

ajax.protocols.Response.prototype.ajax$protocols$AjaxResponse$_body$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return new cljs.core.Keyword(null,"body","body",-2049205669).cljs$core$IFn$_invoke$arity$1(this$__$1);
});

ajax.protocols.Response.prototype.ajax$protocols$AjaxResponse$_status$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return new cljs.core.Keyword(null,"status","status",-1997798413).cljs$core$IFn$_invoke$arity$1(this$__$1);
});

ajax.protocols.Response.prototype.ajax$protocols$AjaxResponse$_status_text$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return new cljs.core.Keyword(null,"status-text","status-text",-1834235478).cljs$core$IFn$_invoke$arity$1(this$__$1);
});

ajax.protocols.Response.prototype.ajax$protocols$AjaxResponse$_get_response_header$arity$2 = (function (this$,header){
var self__ = this;
var this$__$1 = this;
return cljs.core.get.call(null,new cljs.core.Keyword(null,"headers","headers",-835030129).cljs$core$IFn$_invoke$arity$1(this$__$1),header);
});

ajax.protocols.Response.prototype.ajax$protocols$AjaxResponse$_was_aborted$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return new cljs.core.Keyword(null,"was-aborted","was-aborted",-2120084828).cljs$core$IFn$_invoke$arity$1(this$__$1);
});

ajax.protocols.Response.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__23379__auto__,k__23380__auto__){
var self__ = this;
var this__23379__auto____$1 = this;
if(cljs.core.contains_QMARK_.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"was-aborted","was-aborted",-2120084828),null,new cljs.core.Keyword(null,"status-text","status-text",-1834235478),null,new cljs.core.Keyword(null,"headers","headers",-835030129),null,new cljs.core.Keyword(null,"status","status",-1997798413),null,new cljs.core.Keyword(null,"body","body",-2049205669),null], null), null),k__23380__auto__)){
return cljs.core.dissoc.call(null,cljs.core.with_meta.call(null,cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,this__23379__auto____$1),self__.__meta),k__23380__auto__);
} else {
return (new ajax.protocols.Response(self__.status,self__.body,self__.status_text,self__.headers,self__.was_aborted,self__.__meta,cljs.core.not_empty.call(null,cljs.core.dissoc.call(null,self__.__extmap,k__23380__auto__)),null));
}
});

ajax.protocols.Response.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__23377__auto__,k__23378__auto__,G__26546){
var self__ = this;
var this__23377__auto____$1 = this;
var pred__26550 = cljs.core.keyword_identical_QMARK_;
var expr__26551 = k__23378__auto__;
if(cljs.core.truth_(pred__26550.call(null,new cljs.core.Keyword(null,"status","status",-1997798413),expr__26551))){
return (new ajax.protocols.Response(G__26546,self__.body,self__.status_text,self__.headers,self__.was_aborted,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_(pred__26550.call(null,new cljs.core.Keyword(null,"body","body",-2049205669),expr__26551))){
return (new ajax.protocols.Response(self__.status,G__26546,self__.status_text,self__.headers,self__.was_aborted,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_(pred__26550.call(null,new cljs.core.Keyword(null,"status-text","status-text",-1834235478),expr__26551))){
return (new ajax.protocols.Response(self__.status,self__.body,G__26546,self__.headers,self__.was_aborted,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_(pred__26550.call(null,new cljs.core.Keyword(null,"headers","headers",-835030129),expr__26551))){
return (new ajax.protocols.Response(self__.status,self__.body,self__.status_text,G__26546,self__.was_aborted,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_(pred__26550.call(null,new cljs.core.Keyword(null,"was-aborted","was-aborted",-2120084828),expr__26551))){
return (new ajax.protocols.Response(self__.status,self__.body,self__.status_text,self__.headers,G__26546,self__.__meta,self__.__extmap,null));
} else {
return (new ajax.protocols.Response(self__.status,self__.body,self__.status_text,self__.headers,self__.was_aborted,self__.__meta,cljs.core.assoc.call(null,self__.__extmap,k__23378__auto__,G__26546),null));
}
}
}
}
}
});

ajax.protocols.Response.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__23382__auto__){
var self__ = this;
var this__23382__auto____$1 = this;
return cljs.core.seq.call(null,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"status","status",-1997798413),self__.status],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"body","body",-2049205669),self__.body],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"status-text","status-text",-1834235478),self__.status_text],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"headers","headers",-835030129),self__.headers],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"was-aborted","was-aborted",-2120084828),self__.was_aborted],null))], null),self__.__extmap));
});

ajax.protocols.Response.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__23369__auto__,G__26546){
var self__ = this;
var this__23369__auto____$1 = this;
return (new ajax.protocols.Response(self__.status,self__.body,self__.status_text,self__.headers,self__.was_aborted,G__26546,self__.__extmap,self__.__hash));
});

ajax.protocols.Response.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__23375__auto__,entry__23376__auto__){
var self__ = this;
var this__23375__auto____$1 = this;
if(cljs.core.vector_QMARK_.call(null,entry__23376__auto__)){
return cljs.core._assoc.call(null,this__23375__auto____$1,cljs.core._nth.call(null,entry__23376__auto__,(0)),cljs.core._nth.call(null,entry__23376__auto__,(1)));
} else {
return cljs.core.reduce.call(null,cljs.core._conj,this__23375__auto____$1,entry__23376__auto__);
}
});

ajax.protocols.Response.cljs$lang$type = true;

ajax.protocols.Response.cljs$lang$ctorPrSeq = (function (this__23404__auto__){
return cljs.core._conj.call(null,cljs.core.List.EMPTY,"ajax.protocols/Response");
});

ajax.protocols.Response.cljs$lang$ctorPrWriter = (function (this__23404__auto__,writer__23405__auto__){
return cljs.core._write.call(null,writer__23405__auto__,"ajax.protocols/Response");
});

ajax.protocols.__GT_Response = (function ajax$protocols$__GT_Response(status,body,status_text,headers,was_aborted){
return (new ajax.protocols.Response(status,body,status_text,headers,was_aborted,null,null,null));
});

ajax.protocols.map__GT_Response = (function ajax$protocols$map__GT_Response(G__26548){
return (new ajax.protocols.Response(new cljs.core.Keyword(null,"status","status",-1997798413).cljs$core$IFn$_invoke$arity$1(G__26548),new cljs.core.Keyword(null,"body","body",-2049205669).cljs$core$IFn$_invoke$arity$1(G__26548),new cljs.core.Keyword(null,"status-text","status-text",-1834235478).cljs$core$IFn$_invoke$arity$1(G__26548),new cljs.core.Keyword(null,"headers","headers",-835030129).cljs$core$IFn$_invoke$arity$1(G__26548),new cljs.core.Keyword(null,"was-aborted","was-aborted",-2120084828).cljs$core$IFn$_invoke$arity$1(G__26548),null,cljs.core.dissoc.call(null,G__26548,new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"body","body",-2049205669),new cljs.core.Keyword(null,"status-text","status-text",-1834235478),new cljs.core.Keyword(null,"headers","headers",-835030129),new cljs.core.Keyword(null,"was-aborted","was-aborted",-2120084828)),null));
});


//# sourceMappingURL=protocols.js.map?rel=1446841697062