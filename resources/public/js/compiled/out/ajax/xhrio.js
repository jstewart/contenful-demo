// Compiled by ClojureScript 0.0-3211 {}
goog.provide('ajax.xhrio');
goog.require('cljs.core');
goog.require('goog.Uri');
goog.require('goog.net.XhrIo');
goog.require('goog.net.XhrManager');
goog.require('goog.json');
goog.require('goog.net.EventType');
goog.require('goog.events');
goog.require('ajax.protocols');
goog.require('goog.net.ErrorCode');
goog.net.XhrIo.prototype.ajax$protocols$AjaxResponse$ = true;

goog.net.XhrIo.prototype.ajax$protocols$AjaxResponse$_body$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.getResponseText();
});

goog.net.XhrIo.prototype.ajax$protocols$AjaxResponse$_status$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.getStatus();
});

goog.net.XhrIo.prototype.ajax$protocols$AjaxResponse$_status_text$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.getStatusText();
});

goog.net.XhrIo.prototype.ajax$protocols$AjaxResponse$_get_response_header$arity$2 = (function (this$,header){
var this$__$1 = this;
return this$__$1.getResponseHeader(header);
});

goog.net.XhrIo.prototype.ajax$protocols$AjaxResponse$_was_aborted$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core._EQ_.call(null,this$__$1.getLastErrorCode(),goog.net.ErrorCode.ABORT);
});

goog.net.XhrIo.prototype.ajax$protocols$AjaxRequest$ = true;

goog.net.XhrIo.prototype.ajax$protocols$AjaxRequest$_abort$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1.abort(goog.net.ErrorCode.ABORT);
});

goog.net.XhrIo.prototype.ajax$protocols$AjaxImpl$ = true;

goog.net.XhrIo.prototype.ajax$protocols$AjaxImpl$_js_ajax_request$arity$3 = (function (this$,p__26531,handler){
var map__26532 = p__26531;
var map__26532__$1 = ((cljs.core.seq_QMARK_.call(null,map__26532))?cljs.core.apply.call(null,cljs.core.hash_map,map__26532):map__26532);
var with_credentials = cljs.core.get.call(null,map__26532__$1,new cljs.core.Keyword(null,"with-credentials","with-credentials",-1163127235),false);
var timeout = cljs.core.get.call(null,map__26532__$1,new cljs.core.Keyword(null,"timeout","timeout",-318625318),(0));
var headers = cljs.core.get.call(null,map__26532__$1,new cljs.core.Keyword(null,"headers","headers",-835030129));
var body = cljs.core.get.call(null,map__26532__$1,new cljs.core.Keyword(null,"body","body",-2049205669));
var method = cljs.core.get.call(null,map__26532__$1,new cljs.core.Keyword(null,"method","method",55703592));
var uri = cljs.core.get.call(null,map__26532__$1,new cljs.core.Keyword(null,"uri","uri",-774711847));
var this$__$1 = this;
var G__26533 = this$__$1;
goog.events.listen(G__26533,goog.net.EventType.COMPLETE,((function (G__26533,this$__$1,map__26532,map__26532__$1,with_credentials,timeout,headers,body,method,uri){
return (function (p1__26530_SHARP_){
return handler.call(null,p1__26530_SHARP_.target);
});})(G__26533,this$__$1,map__26532,map__26532__$1,with_credentials,timeout,headers,body,method,uri))
);

G__26533.setTimeoutInterval(timeout);

G__26533.setWithCredentials(with_credentials);

G__26533.send(uri,method,body,cljs.core.clj__GT_js.call(null,headers));

return G__26533;
});
goog.net.XhrManager.prototype.ajax$protocols$AjaxImpl$ = true;

goog.net.XhrManager.prototype.ajax$protocols$AjaxImpl$_js_ajax_request$arity$3 = (function (this$,p__26534,handler){
var map__26535 = p__26534;
var map__26535__$1 = ((cljs.core.seq_QMARK_.call(null,map__26535))?cljs.core.apply.call(null,cljs.core.hash_map,map__26535):map__26535);
var max_retries = cljs.core.get.call(null,map__26535__$1,new cljs.core.Keyword(null,"max-retries","max-retries",-1933762121));
var priority = cljs.core.get.call(null,map__26535__$1,new cljs.core.Keyword(null,"priority","priority",1431093715));
var timeout = cljs.core.get.call(null,map__26535__$1,new cljs.core.Keyword(null,"timeout","timeout",-318625318),(0));
var id = cljs.core.get.call(null,map__26535__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var headers = cljs.core.get.call(null,map__26535__$1,new cljs.core.Keyword(null,"headers","headers",-835030129));
var body = cljs.core.get.call(null,map__26535__$1,new cljs.core.Keyword(null,"body","body",-2049205669));
var method = cljs.core.get.call(null,map__26535__$1,new cljs.core.Keyword(null,"method","method",55703592));
var uri = cljs.core.get.call(null,map__26535__$1,new cljs.core.Keyword(null,"uri","uri",-774711847));
var this$__$1 = this;
return this$__$1.send(id,uri,method,body,cljs.core.clj__GT_js.call(null,headers),priority,handler,max_retries);
});

//# sourceMappingURL=xhrio.js.map?rel=1446841696898