# contentful-test

A simple demo of Contentful API access to reactjs components.

A content list is available, spaces list isn't implemented (you'll get a 404 error)
Content entries are simply dumped to the screen after clicking on ID.

This was fairly easy to set up with clojurescript due to the excellent support for
reactjs and the routing library that's in the application.

## Development Mode

### Run application:
Get API token from contentful

```
https://app.contentful.com/spaces/4j8ylvu0dirz/api/keys/
It's the "Website" API token
```

Add token to `CHANGEME` placeholder in `resources/public/index.html`


Install leiningen (jvm required):

```
brew install leiningen
```

Run it!

```
lein clean
lein figwheel dev
```

Figwheel will automatically push cljs changes to the browser.

Wait a bit, then browse to [http://localhost:3449](http://localhost:3449).

## Production Build

```
lein clean
lein cljsbuild once min
```
