(ns contentful-test.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :as re-frame]))

(re-frame/register-sub
 :entries
 (fn [db]
   (reaction (:entries @db))))

(re-frame/register-sub
 :current-entry
 (fn [db]
   (reaction (:current-entry @db))))


(re-frame/register-sub
 :spaces
 (fn [db]
   (reaction (:spaces @db))))

(re-frame/register-sub
 :active-panel
 (fn [db _]
   (reaction (:active-panel @db))))

(re-frame/register-sub
 :api-error
 (fn [db _]
   (reaction (:api-error @db))))
