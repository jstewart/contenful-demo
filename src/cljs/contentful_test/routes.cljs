(ns contentful-test.routes
    (:require-macros [secretary.core :refer [defroute]])
    (:import goog.History)
    (:require [secretary.core :as secretary]
              [goog.events :as events]
              [goog.history.EventType :as EventType]
              [re-frame.core :as re-frame]))

(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes []
  (secretary/set-config! :prefix "#")
  ;; --------------------
  ;; define routes here
  (defroute "/" []
    (re-frame/dispatch [:set-active-panel :home-panel]))

  (defroute "/entries" []
    (re-frame/dispatch [:load-entries])
    (re-frame/dispatch [:set-active-panel :entries-panel]))

  (defroute "/entries/:id" [id]
    (re-frame/dispatch [:load-entry id])
    (re-frame/dispatch [:set-active-panel :entry-panel]))


  (defroute "/spaces" []
    (re-frame/dispatch [:load-spaces])
    (re-frame/dispatch [:set-active-panel :spaces-panel]))

  ;; --------------------
  (hook-browser-navigation!))
