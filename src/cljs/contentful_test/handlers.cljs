(ns contentful-test.handlers
  (:require [re-frame.core :as re-frame]
            [ajax.core :refer [GET POST]]
            [contentful-test.db :as db]))

(re-frame/register-handler
 :initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/register-handler
 :set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(re-frame/register-handler
 :load-entries
 (fn [db _]
   (GET
    (str "https://cdn.contentful.com/spaces/"
         js/space
         "/entries?access_token="
         js/access_token)
    {:handler #(re-frame/dispatch [:process-response [:entries %1]])
     :error-handler #(re-frame/dispatch [:api-error %1])
     :response-format :json
     :keywords? true})
   (assoc db :loading? true)))

(re-frame/register-handler
 :load-spaces
 (fn [db _]
   (GET
    (str "https://cdn.contentful.com/spaces?access_token="
         js/access_token)
    {:handler #(re-frame/dispatch [:process-response [:spaces %1]])
     :error-handler #(re-frame/dispatch [:api-error %1])
     :response-format :json
     :keywords? true})
   (assoc db :loading? true)))

(re-frame/register-handler
 :load-entry
 (fn [db [_ id]]
   (GET
    (str "https://cdn.contentful.com/spaces/"
         js/space
         "/entries/"
         id
         "?access_token="
         js/access_token)
    {:handler #(re-frame/dispatch [:process-response [:entry %1]])
     :error-handler #(re-frame/dispatch [:api-error %1])
     :response-format :json
     :keywords? true})
   (assoc db :loading? true)))

(re-frame/register-handler
 :process-response
 (fn [db [_ [type response]]]
   (let [thisdb (assoc db :loading? false :api-error nil)]
     (condp = type
       :entries (assoc thisdb :entries (:items response))
       :spaces (assoc thisdb :spaces   (:items response))
       :entry (assoc thisdb :current-entry response)
       :default thisdb))))

(re-frame/register-handler
 :api-error
 (fn [db [_ error]]
   (assoc db :api-error error)))
